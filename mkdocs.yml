# Project information
site_name: Kubernetes
site_url: "https://sdldocs.gitlab.io/server-techs/kubernetes/" 
site_author: YoonJoon Lee
site_description: >-
 Kebernetes
# Repository
repo_name: sdldocs/server-techs/kubernetes/
repo_url: https://github.com/sdldocs/server-techs/kubernetes/

# Configuration
theme:
  name: material
  # custom_dir: !ENV [THEME_DIR, "material"]

  # Static files
  static_templates:
    - 404.html

  # Don't include MkDocs' JavaScript
  #include_search_page: false
  #search_index_only: true

  # Default values, taken from mkdocs_theme.yml
  language: en
  #features:
  #  - content.code.annotate
    # - content.tabs.link
    # - header.autohide
    # - navigation.expand
  #  - navigation.indexes
    # - navigation.instant
  #  - navigation.sections
  #  - navigation.tabs
    # - navigation.tabs.sticky
  #  - navigation.top
  #  - navigation.tracking
  #  - search.highlight
  #  - search.share
  #  - search.suggest
  #  - toc.follow
    # - toc.integrate
  palette:
    - scheme: amber
      primary: amber
      accent: orange
      #toggle:
        #icon: material/toggle-switch
        #name: Switch to dark mode
    #- scheme: slate
      #primary: red
      #accent: red
      #toggle:
        #icon: material/toggle-switch-off-outline
        #name: Switch to light mode
  font:
    text: Roboto
    code: Roboto Mono
  #favicon: assets/favicon.png
  #icon:
    #logo: logo


nav:
  - 목차: index.md
  - 개발자를 위한 Kubernetes 시리즈:
    - Part I, 왜 Kubernetes를 배워야 할까?: kubernetes4developers/why-learn-kubernetes.md
    - Part II, Docker 기초: kubernetes4developers/docker-basics.md
    - Part III, Kubernetes 101 - Pods & 컨트롤러: kubernetes4developers/pods-controllers.md
    - Part IV, Kubernetes 101 - 구조 & 네트워킹: kubernetes4developers/architecture-networking.md
    - Part V, Kubernetes 101 - 리소스 관리 & 스케쥴링: kubernetes4developers/resourcemanagement-scheduling.md
    - Part VI, Kubernetes 도구: kubernetes4developers/kubernetes-tools.md
    - Part VII, Kubernetes 2023 현황: kubernetes4developers/state-2023.md
    - Part VIII, 고급 Kubernetes 주제: kubernetes4developers/advanced-topics.md
  - Documentation:
    - Home: documentation/home.md
    - 시작하기: documentation/getting-started.md
    - 개념:
      - 개요: documentation/concepts/index.md
      - Overview: 
        - 개요: documentation/concepts/overview/index.md
        - Kubernetes 객체: 
          - Kubernetes의 객체: documentation/concepts/overview/working-with-objects/index.md
          - Kubernetes 객체 관리: documentation/concepts/overview/working-with-objects/object-management.md
          - 객체 이름과 ID: documentation/concepts/overview/working-with-objects/names.md
          - 레이블과 선택자(Selector): documentation/concepts/overview/working-with-objects/labels.md
          - 네임스페이스(Namespaces): documentation/concepts/overview/working-with-objects/namespaces.md
          - 어노테이션: documentation/concepts/overview/working-with-objects/annotations.md
          - 필드 선택자: documentation/concepts/overview/working-with-objects/field-selectors.md
          - 파이널라이저: documentation/concepts/overview/working-with-objects/finalizers.md
          - 소유자와 디펜던트: documentation/concepts/overview/working-with-objects/owners-dependents.md
          - 추천 레이블: documentation/concepts/overview/working-with-objects/common-labels.md
        - Kubernetes 구성 요소: documentation/concepts/overview/kubernetes-components.md
        - Kubernetes API: documentation/concepts/overview/kubernetes-api.md
  #     - 클러스터 구조: documentation/concpts/cluster-architecture.md
  #     # - 컨테이너:
  #     # - 워크로드:
  #     # - 서비스, 로드 밸런싱과 네트워킹:
  #     # - 스토리지:
  #     # - 구성:
  #     # - 보안:
  #     # - 정책:
  #     # - 스케쥴링, Preemption과 Eviction:
  #     # - 클러스터 관리:
  #     # - Kubernetes 확장:
      - 워크로드:
        - 워크로드: documentation/concepts/workloads/index.md
        - Pods:
          - Pods: documentation/concepts/workloads/pods/index.md
          - Pods 수명주기: documentation/concepts/workloads/pods/pod-lifecycle.md
        - 워크로드 리소스:
          - 워크로그 리소스: documentation/concepts/workloads/controllers/index.md
  #   - 작업:
  #     - 도구 설치: documentation/tasks/install-tools.md
  #     - 클러스터 관리: documentation/tasks/administer-cluster.md
    - 튜토리얼:
      - Hello Minikube: documentation/tutorials/hello-minikube.md
      - Kubernetes 기초: 
        - Kubernetes 기본 사항 알아보기: documentation/tutorials/kubernetes-basics/index.md
        - 클러스터 생성: documentation/tutorials/kubernetes-basics/create-cluster.md
        - 앱 Deployment: documentation/tutorials/kubernetes-basics/deploy-app.md
        - 앱 탐색: documentation/tutorials/kubernetes-basics//explore.md
        - 앱 노출: documentation/tutorials/kubernetes-basics/expose.md
        - 앱 확장: documentation/tutorials/kubernetes-basics/scale.md
        - 앱 업데이트: documentation/tutorials/kubernetes-basics/update.md
      - 상태를 저장하지 않는(Statelee) 어플리케이션:
        - Stateless 어플리케이션: documentation/tutorials/stateless-applications/index.md
        - 클러스터의 어플리케이션 액세스를 위한 외부 IP 주소 노출: documentation/tutorials/stateless-applications/expose-external-ip-address.md
        - Redis를 사용한 PHP 방명록 배포: documentation/tutorials/stateless-applications/guestbook.md
      - 상태를 유지하는(Stateful) 어플리케이션:
        - Stateful 어플리케이션: documentation/tutorials/stateful-applications/index.md
        - StatefulSet 기초: documentation/tutorials/stateful-applications/statefulset-basics.md
        - 퍼시스턴트 볼륨이 있는 워드프레스와 MySQL 배포: documentation/tutorials/stateful-applications/mysql-wordpress-persistent-volume.md
  #   - Reference:
  #     - 용어: documentation/reference/glossary.md
  #     - API 개요: documentation/reference/api-overview.md
