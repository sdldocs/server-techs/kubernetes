"개발자를 위한 Kubernetes" 시리즈의 Part I에 오신 것을 환영합니다!

Kubernetes의 개념에 대해 알아보기 전에 한 걸음 물러나서 Kubernetes를 배우는 것이 왜 필요한지 먼저 정리해보는 것이 유용할 것이다. 항상 동기를 파악하는 것은 배움에 도움을 줄 수 있다. 이미 이 시리즈를 읽기 시작하였기 때문에 잔소리일 가능성이 높지만, 그럼에도 불구하고 몇 가지 흥미로운 이점을 발견하실 수 있을 것이다.

그럼 시작해 보자!

[DevZero](http://devzero.io/)이 이 시리즈를 제공했다.

![](./images/why-learn-kubernetes/fig-devzero.webp)

DevZero의 플랫폼은 클라우드 호스팅 개발 환경을 제공한다. DevZero를 사용하면 기존 인프라 구성을 재사용하여 개발과 테스트를 위한 프로덕션과 유사한 환경을 구성할 수 있다.

지금 바로 [devzero.io](https://www.devzero.io/)에서 DevZero를 시작할 수 있다.

## Kubernetes is everywhere
우선, Kubernetes는 전 세계를 먹어 치우고 있는 중이다.

![](images/why-learn-kubernetes/fig-everywhere.webp)

2018년부터 Cloud Native Computing Foundation(CNCF)은 매년 설문조사를 실시하고 있다. 2022년에 실시된 최신 설문조사에서는 560만 명 이상의 개발자가 Kubernetes를 사용한다고 답했다. 설문조사 결과에 따르면 더 많은 개발자와 조직이 Kubernetes를 채택하고 있을 뿐만 아니라 사용 중인 클러스터의 수도 증가하고 있는 중이다.

![](./images/why-learn-kubernetes/fig-3.webp)

이 설문조사에 선택 편향이 있다고 말할 수도 있지만 결국, Kubernetes가 속한 클라우드 네이티브 컴퓨팅 재단의 설문조사에 응답한 사람들은 대부분 Kubernetes를 사용 중이거나 적어도 관심이 있을 가능성이 높기 때문이다. 그러나 다른 설문조사([Splunk](https://www.splunk.com/en_us/blog/learn/state-of-kubernetes.html), [RedHat](https://www.redhat.com/en/resources/kubernetes-adoption-security-market-trends-overview), [D2IQ](https://d2iq.com/blog/data-kubernetes-2022-survey), [Dynatrace](https://www.dynatrace.com/news/blog/kubernetes-in-the-wild-2023/), [Datadog](https://www.datadoghq.com/container-report/))의 데이터를 통해 Kubernetes 사용의 증가 추세를 확인할 수 있다.

일화적으로, 역사적으로 느리게 변화하는 산업에서도 Kubernetes를 채택하는 것이 나타나고 있다. 정부와 군용 어플리케이션은 물론 금융 부문에서도 Kubernetes가 사용되는 사례가 생기고 있다.

Dilbert에서 조차도 언급하고 있다.

![](./images/why-learn-kubernetes/fig-4.webp)

## 왜 개발자 여러분이 신경 써야 할까?
점점 더 많은 사람들이 Kubernetes를 사용하고 있지만, 개발자로서 왜 관심을 가져야 할까?

실용적인 측면과 추상적인 측면을 모두 생각해 보도록 하자.

### 실용적인 측면
가장 구체적인 예로, 구인 공고의 핵심 용어로 Kubernetes가 자주 등장하는 것을 들 수 있다. 예전에는 구인 공고의 '보너스 점수' 또는 '있으면 좋은 점' 섹션에 Kubernetes 경험이 나열되는 경우가 많았다. 이제는 특히 스타트업에서 선임 백엔드 엔지니어링 직책의 '자격 요건' 섹션에 해당 항목이 포함되는 것을 어렵지 않게 볼 수 있다.

![](./images/why-learn-kubernetes/fig-5.webp)

최소한 Kubernetes에 대한 경험이나 이해만 있어도 이력서 심사를 통과하는 데 도움이 될 수 있다.

## 기타 이점
위의 이유가 진부하게 느껴진다면 저자가 경험했던 다른 이점들을 소개한다.

- **표준화된 인프라(Standardized infrastructure)**: 이에 대해서는 이 시리즈의 개발자 경험 부분에서 자세히 설명하겠지만, 많은 조직에서 다양한 수준의 Kubernetes 채택으로 어려움을 겪고 있다. 대부분의 경우, 프로덕션과 스테이징 환경에는 Kubernetes 클러스터가 있지만 개발, QA와 테스팅 단계에서는 여전히 Docker 또는 Docker Compose를 사용하고 있을 수 있다. 이는 두 세트의 인프라를 유지 관리해야 한다는 것을 의미하지만, 모든 환경에 대해 단일 Kubernetes 기반 인프라가 있다면 개발자와 인프라 팀(예: SRE, 플랫폼 엔지니어링, DevOps 등) 모두 많은 시간을 절약할 수 있다.
- **Kubernetes를 염두에 둔 설계(Design for Kubernetes in mind)**: 회사에서 Kubernetes를 어떻게 사용하여 설계하였는 지를 이해하면 Kubernetes 기본 제공 구성 요소를 직접 다시 작성하는 일을 피할 수 있다. 여기에는 상태 확인과 서비스 검색같은 Kubernetes 기본 개념부터 mTLS를 Istio로 오프로드하는 것과 같은 관련 도구로 잠금 해제된 기능까지를 포함한다.
- **더 "agile하게"**: 새로운 제품이나 통합을 대규모로 테스트하는 경우, 이제 제공된 Helm 차트 또는 Kubernetes 매니페스트 파일을 사용하여 Kubernetes에 배포하는 것이 매우 간단해졌다. Kubernetes에 익숙할수록 인프라 담당자가 배포 또는 디버깅을 도와줄 때까지 기다릴 필요 없이 더 빠르게 반복하고 PoC를 구축할 수 있다.
- **도구에 대한 이해**: 대부분의 회사에는 사용자가 사용할 수 있는 Kubernetes 관련 도구가 있다. 때로는 쿠버네티스 CLI 도구에 대한 간단한 래퍼일 수도 있다. 다른 경우에는 기본 개념을 추상화한 Kubernetes 또는 타사 플랫폼에 구축된 내부 도구일 수도 있다. 정확한 질문을 하기 위해 Kubernetes 개념과 도구의 작업 방식을 구분하는 것이 도움이 되는 경우가 빈번하다. ChatGPT를 사용하더라도 먼저 무엇을 질문해야 할 것인지 알아야 한다.

이 중 어느 것도 마음에 들지 않는다면 단순히 "더 나은 엔지니어가 되기 위해" Kubernetes를 배우고 싶을 수도 있다. (저요!)

![](./images/why-learn-kubernetes/fig-6.webp)

## 다음 단계
지금쯤이면 Kubernetes에 대해 배우거나 적어도 이 시리즈를 읽는 것이 시간을 투자할 만한 가치가 있다는 확신이 들었기를 바란다. 다음 파트에서 Kubernetes가 왜 필요한지, 그리고 K8의 개념이 Docker의 개념과 어떻게 유사한지 이해하는 데 중요한 Docker 개념에 대해 자세히 알아보겠다.
