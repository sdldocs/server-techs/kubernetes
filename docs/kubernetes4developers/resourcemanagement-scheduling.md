[Part IV, Kubernetes 101 - 구조 & 네트워킹](architecture-networking.md)에서 Kubernetes 구조와 네트워킹에 대해 자세히 살펴보았다. 이제 pod와 컨트롤러에 대해 알아보고 pod 안팎에서 트래픽을 활성화하는 방법을 알아보았다. 리소스 관리와 스케줄링으로 Kubernetes 기본 개념에 대한 개요를 마무리해 보겠다. 즉, 어플리케이션에 필요한 적절한 리소스를 제공하는 방법에 대해 살펴보고, pod를 스케줄링할 때 Kubernetes를 어떻게 유연하게 사용할 수 있는지 살펴보겠다.

이 주제는 개발이나 인프라 관련 주제라고 생각할 수도 있지만, 이러한 주제에 익숙해지는 것만으로도 스케줄링할 수 없거나 제거된 pod를 디버깅하는 데 시간을 절약하는 데 도움이 될 수 있다. 또한, Kubernetes 네이티브 기능으로 고가용성과 내결함성을 지원할 수 있는 방법에 대해 생각해보는 것도 도움이 된다.

회의적이시더라도 인내심을 갖고 이 주제를 훑어보시기 바란다.

## 리소스 관리
Kubernetes는 **리소스** 섹션에서 어플리케이션이 각 컨테이너에 필요한 리소스를 지정할 수 있다. 가장 일반적으로 CPU와 메모리(RAM)에 대한 요청과 제한을 설정한다.

이름은 상당히 자명하지만 세부 사항은 어플리케이션에 미묘한 영향을 미친다.

- **Requests**: `kube-scheduler`가 pod를 스케줄링할 node를 결정하는 데 요청(request)을 사용한다. CPU 2개와 512MB의 메모리를 요청하면, `kube-scheduler`는 그 정도의 여유 용량을 가진 노드를 찾는다. 노드에 초과 용량이 있는 경우, 어플리케이션은 요청된 리소스보다 더 많은 리소스를 사용할 수 있다.
- **Limits**: 리소스 제한을 적용하기 위해 kubelet과 컨테이너 런타임이 제한(limit)을 사용한다. 어플리케이션이 제한보다 더 많은 CPU를 사용하는 경우, 조절하기 시작한다. 마찬가지로, 컨테이너가 제한보다 더 많은 메모리를 소비하면 시스템 커널에 의해 메모리 부족(OOM, out of memory) 오류와 함께 종료된다.

아래 예에서 각 컨테이너에 대한 CPU와 메모리 요청과 제한을 확인할 수 있다. CPU는 CPU 단위로 측정되며, "1" 단위는 물리적 또는 가상 코어와 동일하다. 0.5 CPU와 같이 소수점 단위를 사용하거나 250m와 같이 *milicpu*를 사용할 수 있다. 반면 메모리는 일반적인 수량 접미사를 사용하여 바이트 단위로 측정한다(예: Mi는 Mebibytes, G는 Gigabytes).

```yml
apiVersion: v1
kind: Pod
metadata:
  name: example
spec:
  constainers:
    name: my-app
    ...
    resources:
      requests:
        memory: "512Mi"
        cpu: "2"
      limits:
        memory: "512Mi"
        cpu: "4"
    name: side-car
    ...
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```

리소스와 관련하여 이해해야 할 몇 가지 미묘하지만 중요한 사항이 있다.

1. 용량 계획을 수행할 때, 기본 node의 모든 CPU와 메모리를 어플리케이션이 사용할 수 있는 것은 아니라는 점을 이해하는 것이 중요하다. 각 node는 운영 체제, Kubernetes 구성 요소, 모니터링 또는 보안 에이전트와 같은 기타 DaemonSets을 실행하기 위해 약간의 예약된 용량이 필요하기 때문이다.
2. 요청과 제한을 결합하여 어플리케이션의 서비스 품질(QoS)을 설정할 수 있다. 예를 들어 보장된 QoS가 필요한 경우 요청을 제한과 동일하게 설정할 수 있다. 반면에 터지기 쉬운(burstable) 어플리케이션의 경우 평균 부하를 기준으로 요청 리소스를 설정하고 더 높은 제한을 최대 부하로 설정할 수 있다.
3. "이상적인" 요청과 한도를 찾으려면 약간의 실험이 필요하다. 너무 많은 리소스를 요청하여 어플리케이션을 예약하기 어렵게 만들거나 리소스를 낭비하고 싶지는 않을 것이다. 반면에 한도를 너무 낮게 설정하면 어플리케이션이 억압되거나 종료될 위험이 있다.

Kubernetes 리소스 계획은 어플리케이션을 프로덕션으로 푸시하기 전에 해야 하는 중요한 작업으로, [궁극적인 Kubernetes 리소스 계획 가이드](https://blog.devgenius.io/ultimate-kubernetes-resource-planning-guide-449a4fddd1d6)에서 다루고 있다. 하지만 지금 여기서 중요한 것은 1) 리소스를 설정하면 어플리케이션 스케줄링에 대한 효휼적인 결정을 내리는 데 도움이 되고, 2) 이 값을 검사하고 수정하여 어플리케이션 성능 또는 서비스 품질을 개선할 수 있다.

## 스케쥴링
pod 스케줄링에 대해 설명하자면, Kubernetes에는 다른 node의 pod를 스케줄링하는 데 도움이 되는 더 세분화된 옵션이 있다. 이것이 왜 중요한지 궁금할 수 있다. Kubernetes는 단순히 사용 가능한 node에 pod를 스케줄하면 되지 않을까?

다음 사용 사례를 고려해보자.

- 특정 하드웨어(예: GPU가 있는 인스턴스, ARM 또는 Windows node, 다른 인스턴스 타입, 스팟 또는 프리퍼티블과 같은 할인 노드)를 특정 pod가 활용하도록 하려는 경우
- 고가용성을 위해 여러 영역 또는 토폴로지에 있는 여러 node에 node를 분산시킬 수 있다. 또는 네트워크 지연 시간을 최소화하기 위해 특정 pod를 공동 배치할 수도 있다.
- 규정 준수 또는 보안상의 이유로 특정 pod가 동일한 하드웨어에 스케줄링되지 않도록 해야 한다.
- 리소스가 제한되어 있을 때 더 중요한 작업을 수행하기 위해 특정 pod를 다른 pod들 보다 우선적으로 스케줄링할 수 있다.

Kubernetes를 사용하면 사용 사례에 맞게 원하는 동작을 지정할 수 있다. 스케줄링 결정에 영향을 미치는 몇 가지 방법을 살펴보자.

1. 가장 간단한 방법은 `nodeSelector`를 사용하는 것이다. node 레이블을 사용하여 해당 레이블이 있는 node에만 pod를 스케줄링하도록 Kubernetes에 지시할 수 있다.
2. node 선택 동작을 보다 세밀하게 제어해야 한다면, [`nodeAffinity`](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity) 필드를 사용할 수 있다. `preferredDuringSchedulingIgnoredDuringExecution`을 통해 소프트 규칙을 설정하거나 `requiredDuringSchedulingIgnoredDuringExecution`을 통해 하드 규칙을 설정하여 표현식(expression)을 계산할 수 있다. 일반적인 예로 가용성 영역 또는 레이블을 선택하는 규칙이 있다.
3. 또는, pod 레이블을 기반으로 스케줄링 동작을 설정할 수도 있다. 이는 특정 pod를 공동 배치하거나 pod가 동일한 node에 스케줄링되지 않도록 하는 데 유용할 수 있다. 구문은 `nodeAffinity`와 유사하게 동작한다.
4. 특정 노드에 pod가 스케줄되지 않도록 하려면 `taint`를 사용할 수 있다. taint 레이블과 일치하고 허용성을 가진 파드만 해당 노드에 스케줄링될 수 있다. 이는 특정 하드웨어가 있는 node를 액세스하기 위해 중요한 워크로드를 격리하는 데 유용할 수 있다. 예를 들어, GPU를 지원하는 노드에 taint가 있어서 머신 러닝 워크로드만 스케줄링되고 다른 pod는 리소스를 차지하지 않도록 할 수 있다.
5. 또한 토폴로지 확산 제약 조건을 정의하여 최대 스큐(즉, pod가 얼마나 고르지 않게 분산되는지) 및 토폴로지 도메인같은 고가용성 동작과 리소스 활용도를 미세 조정할 수 있다.
6. 마지막으로, `PriorityClass`를 정의하여 스케줄링에서 우선순위를 지정해야 하는 pod를 `kube-scheduler`에 알려줄 수 있다. pod를 스케줄링할 수 없는 경우, 더 중요한 pod를 위한 공간을 확보하기 위해 우선순위가 낮은 pod를 퇴거시킬 수도 있다.

Kubernetes 문서 웹사이트에는 위의 모든 동작에 대한 광범위한 예가 있으므로, 이 동작이 필요한 경우 [nodeSelector](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector), [nodeAffinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity), [podAffinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity), [taints](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/), [토폴로지](https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/) 및 [pod 우선순위](https://kubernetes.io/docs/concepts/scheduling-eviction/pod-priority-preemption/)를 확인해 보기를 적극 권장한다.

## 마무리
이것으로 가장 중요한 Kubernetes 개념에 대한 간략한 개요를 마무리했다. 물론 Kubernetes를 관리하고 확장하려면 아직 익혀 할 다른 많은 주제가 있지만, Kubernetes 사용을 배우는 개발자로서 이제 탄탄한 기초를 갖추셨을 것이다. 더 고급 주제나 스토리지 또는 구성같이 이 글에서 다루지 않은 주제를 접하게 된다면, Kubernetes 웹사이트로 이동하여 자세히 알아볼 수 있다.

지금쯤이면 컨테이너 오케스트레이터를 위한 기능을 제공하기 위해 Kubernetes 내부에서 많은 일이 진행되고 있다는 것을 알 수 있을 것이다. 여러분이 일상적으로 사용하는 대부분의 Kubernetes 클러스터에는 살펴본 기본 Kubernetes 구성 요소 외에도 훨씬 더 많은 애드온과 헬퍼 도구가 설치되어 있을 것이다. 다음에는 이러한 인기 있는 도구 중 몇 가지를 살펴봄으로써 동일한 작업을 반복하지 않고도 어떤 도구를 활용할 수 있는지 알아볼 수 있도록 하겠다.
