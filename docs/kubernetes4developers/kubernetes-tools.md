지금까지 pod에서 리소스 관리에 이르기까지 몇 가지 기본적인 Docker와 Kubernetes 개념을 다루었다. 이 파트에서는 이론에서 좀 더 실용적인 문제로 넘어가서 몇 가지 인기 있는 Kubernetes 도구를 살펴보겠다. 예리한 독자라면 이전 글에서 Kubernetes 매니페스트나 구성 요소 배포에 대해 자세히 다루지 않았다는 것을 눈치채셨을 것이다. 이는 의도적인 것이었으며, 대부분의 경우 이러한 도구와 직접 상호 작용하지 않을 것이기 때문이다. 대부분의 경우 인프라 팀이 이미 설치한 오픈 소스 또는 자체 개발 도구를 사용하게 될 것이다.

이 시리즈의 목표는 Kubernetes를 보다 편안하고 생산적으로 사용할 수 있도록 돕는 것이다. 몇 가지 인기 있는 Kubernetes 도구를 살펴봄으로써 다음과 같은 목표를 달성하고자 한다.

1. 네이티브 Kubernetes 개념과 그 위에 구축된 추상화/래퍼/툴링 레이어를 식별하는 데 도움을 준다.
2. 기존 프로젝트에 대한 안내를 제공하므로 처음부터 다시 시작할 필요가 없다.
3. 작업을 더 빠르게 완료하는 데 도움이 되는 애드온/플러그인/도구를 알려주거나 어떤 질문을 해야 하는지 알려준다.

그럼 시작해 보자!

## Helm
[Helm](https://helm.sh/)은 배포를 위한 템플릿과 버전 관리 기능을 제공하는 Kubernetes 어플리케이션용 패키지 매니저이다. Helm은 Kubernetes에서 가장 널리 사용되는 패키지 관리자이며 다른 배포 도구에서 강력한 통합 기능을 제공하는 경우가 많기 때문에 알아두는 것이 중요하다.

개발자의 경우 두 가지중 한 방법으로 Helm과 상호작용할 가능성이 높다.

1. 자체 Helm 차트를 생성하여 배포를 위해 어플리케이션을 패키징해야 한다.
2. 통합 또는 엔드투엔드 테스트를 위해 다른 서비스나 종속성(예: 데이터베이스, 캐시, 메시지 큐)을 배포하기 위해 기존 Helm 차트(오픈소스 또는 비공개, 내부)를 사용한다.

어플리케이션에서 어떻게 Helm을 사용하는 지에 대한 구체적인 내용은 조직마다 다를 수 있다. 일부 팀에는 Helm 차트를 부트스트랩하는 데 도움이 되는 CLI 도구가 있을 수 있다. 다른 팀에는 일부 값을 재정의하고 사용할 수 있는 "표준" 템플릿이 있을 수 있다. 다른 팀에서는 더 완전한 기능을 갖춘 대시보드나 포털을 통해 사용 환경을 안내할 수도 있다.

하지만 결국 Helm은 이전 단원에서 배운 Kubernetes 매니페스트(예: 배포, 스테이트풀셋, 서비스 등)를 차트라는 논리적 패키지로 감싸는 래퍼일 뿐이다. Kubernetes 네이티브 개념과 Helm 또는 그 위에 있는 관련 도구에서 제공하는 템플릿 엔진을 구분할 수 있다면 무슨 일이 일어나고 있는지 빠르게 파악할 수 있을 것이다.

Helm에 대해 자세히 알아보려면 [Helm 101 for Developers](https://levelup.gitconnected.com/helm-101-for-developers-1c28e734937e)을 살펴보자.

기타 인기 도구: [kustomize](https://kustomize.io/), [cdk8s](https://cdk8s.io/)

## 로컬 개발을 위한 Kubernetes 배포
원격 개발 환경이 설정된 대규모 조직에서 일하지 않는 한, 로컬 개발용 Kubernetes 배포를 접하게 될 것이다. 이 분야에는 [Docker Desktop](https://docs.docker.com/desktop/kubernetes/), [minikube](https://minikube.sigs.k8s.io/docs/start/), [kind](https://kind.sigs.k8s.io/), [k3s](https://k3s.io/) 등 다양한 옵션이 있다. 개발과 테스트를 위해 여전히 로컬에서 Docker 또는 Docker Compose를 사용하고 있더라도 CI에서 이러한 옵션이 실행되는 것을 볼 수 있다.

이러한 다양한 Kubernetes 배포에 대해 알아야 할 중요한 점은 이러한 배포는 주로 로컬(즉, 보다 제약이 많고 프로덕션과 유사하지 않은 환경) 또는 테스트용 CI에서 실행하도록 설계되었기 때문에 Kubernetes 기능이 약간 다르게 동작할 수 있다는 점이다. 예를 들어, ingress 또는 로드 밸런서 기능을 지원하기 위해 일부 해결 방법이 구현될 수 있다.

이러한 옵션 중 일부에 대해 궁금한 점이 있다면 [Kubernetes for Local Developemnt](https://blog.devgenius.io/kubernetes-for-local-development-a6ac19f1d1b2)를 읽어 보자.

## 공통 클러스터 구성 요소
다음으로, 클러스터에 이미 설치되어 있는 일반적인 클러스터 구성 요소들이 있다. 인프라 팀과 협력하여 이러한 구성 요소(또는 유사한 구성 요소)를 설치하면 더욱 편리하게 사용할 수 있다.

- **클러스터 오토스케일러**: Kubernetes는 pod에 대한 자동 확장 기능을 제공하지만, 기본 node를 확장하려면 별도의 클러스터 오토스케일러를 설치해야 한다. GKE 같은 일부 관리형 Kubernetes 제공자는 이 기능을 패키지로 제공한다. 다른 경우에는 node 또는 pod(수직 확장을 원하는 경우)에 대한 [Kubernetes 오토스케일러](https://github.com/kubernetes/autoscaler)를 설치하고 구성해야 한다. EKS에서 실행하는 경우, [Karpenter](https://itnext.io/karpenter-open-source-high-performance-kubernetes-cluster-autoscaler-d56e3ab06aae)를 확인해 보자.
- **Cert-manager 및 external-dns**: 외부 액세스가 필요한 웹 어플리케이션의 경우, 1) 서비스를 가리키도록 DNS를 구성하고 2) 엔드포인트를 암호화하기 위해 TLS 인증서를 구성하는 것은 시간이 많이 걸리고 사소한 작업 중 하나이다. 이 모든 작업은 Terraform 또는 SDK를 통해 Kubernetes 외부에서 제어할 수 있지만, [cert-manager](https://cert-manager.io/)와 [external-dns](https://github.com/kubernetes-sigs/external-dns)를 사용하여 Kubernetes 서비스에 주석을 추가함으로써 이 프로세스를 자동화할 수도 있다.
- **secret 관리**: Kubernetes에서 어플리케이션을 실행하는 데 있어 또 다른 실질적인 문제는 어플리케이션이 사용할 시크릿(secret)을 가져오는 것이다. Kubernetes는 기본적으로 시크릿 관리를 지원하지만, 기본적으로 base64로 인코딩된 일반 텍스트로 저장된다. 클러스터를 구성하여 미사용 시크릿을 암호화할 수 있지만, [Vault]()나 클라우드 제공업체의 제품 같은 정교한 제어와 감사 메커니즘을 제공하지 않는다. 시작 또는 초기 컨테이너에서 서비스에서 이러한 시크릿을 가져오는 대신, [Secrets Store CSI Driver](https://secrets-store-csi-driver.sigs.k8s.io/introduction.html) 같은 도구를 사용하여 이를 자동화할 수 있다.
- **백업 및 마이그레이션**: 마지막으로, 퍼시스턴트(persistent) 볼륨이 있는 stateful 어플리케이션을 실행하는 경우 재해 복구 또는 보다 안전한 마이그레이션을 위해 주기적으로 데이터를 백업해야 한다. 이 작업을 자동화하는 데는 [velero](https://velero.io/)만 있으면 된다. 자세히 알아보려면 [Disaster Recovery on Kubernetes](https://blog.devgenius.io/disaster-recovery-on-kubernetes-98c5c78382bb)에 대한 가이드를 읽어보세요.

## 기타
마지막으로, Kubernetes와 더 쉽게 상호 작용할 수 있는 유용한 도구 모음으로 마무리하겠다.

- **Command line tools**: kubectl 프롬프트 문자열을 위한 [kube-ps1](https://github.com/jonmosco/kube-ps1), 클러스터와 네임스페이스 간 전환을 위한 [kubectx](https://github.com/ahmetb/kubectx).
- **Port forwarding**: 포트 포워딩할 엔드포인트를 쉽게 관리할 수 있는 [kube-forwarder](https://github.com/pixel-point/kube-forwarder).
- **IDE**: Kubernetes 클러스터를 관리하고 상호 작용할 수 있는 멋진 콘솔 환경을 위한 [OpenLens](https://github.com/lensapp/lens).

## 마무리
Kubernetes 에코시스템은 매일 새로운 도구와 프로젝트가 추가되는 방대한 규모이다. Google에서 "awesome-kubernetes"를 검색하면 현재 진행 중인 모든 작업을 파악할 수 있다. 그러나 대부분의 경우, 여러분은 인프라 팀이 설정한 것을 기반으로 어느 정도 큐레이팅된 Kubernetes와의 상호 작용을 하게 될 것이다. 일부 사용자에게는 몇 가지 도우미 도구가 포함된 최소한의 Kubernetes 클러스터에 가까울 수 있다. 다른 경우에는 조직의 프레임워크나 도구 뒤에 추상화되어 있을 수도 있다. 어떤 경우든 Kubernetes 기본 개념만 알고 있다면 일반 Kubernetes 위키에서 답을 찾을지, 아니면 도구 리포지토리에서 답을 찾을지 구분할 수 있다. LLM이 발전함에 따라 조만간 이러한 질문을 한 곳에서 할 수 있게 되겠지만, 그때까지는 Kubernetes 스택을 풀어나가기 위한 출발점이 필요하다.

다음에는 Kubernetes가 어디에 사용되고 있는지, 어떻게 사용되고 있는지 등 Kubernetes의 현황을 살펴보겠다. 
