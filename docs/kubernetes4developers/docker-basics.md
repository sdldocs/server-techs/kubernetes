"개발자를 위한 Kubernetes" 시리즈의 Part II에 오신 것을 환영합니다!

Kubernetes는 컨테이너 오케스트레이션 플랫폼 또는 시스템으로 정의되는 경우가 많다. 저자가 Kubernetes에 대해 처음 배울 때만 해도 그 의미가 정확히 무엇인지 명확하지 않았다. 지금은 Kubernetes 문서에서 "컨테이너화된 어플리케이션의 배포, 확장과 관리를 자동화하는 오픈 소스 시스템"이라고 더 잘 설명되어 있지만, 정의를 아는 것 외에 애초에 Kubernetes가 왜 필요한지에 대해서는 여전히 불분명했다.

당시 풀스택 개발자였던 저자는 Docker와 Docker Compose만으로도 충분하다고 생각했다. 이제 코드를 작성하면 클라우드에서도 동일하게 작동할 것이라고 합리적으로 기대할 수 있었다. 결국, "하지만 내 컴퓨터에서는 작동한다"는 수수께끼에 대한 확실한 해결책이었다.

![](./images/docker-basics/fig-01.png)

하지만 어플리케이션이 확장되기 시작하고 DevOps/[SRE](https://aws.amazon.com/ko/what-is/sre/) 유형의 작업을 더 많이 맡게 되면서 Docker의 한계와 Kubernetes가 탄생한 이유를 깨닫기 시작했다. 대부분의 어플리케이션 개발자에게 "왜 Kubernetes를 사용해야 하는가/필요한가"라는 맥락이 종종 빠져 있다. 그래서 Kubernetes 개념으로 바로 넘어가기 전에, 관련 Kubernetes 유사점을 설명하는 데 유용할 뿐만 아니라 "왜"에 대한 근거를 마련할 수 있는 기본 Docker 개념에 대해 설명하고자 한다.

> **Notes**: 예리한 독자들은 Docker != 컨테이너라고 지적할 수도 있다. 예, 엄밀히 말하면 Docker는 컨테이너를 개발하고 실행하기 위한 플랫폼이다. 그러나 컨테이너 시장에서 Docker가 지배적이기 때문에 구어체적으로 Docker와 컨테이너는 종종 같은 의미로 사용된다. 이 글에서는 기술적인 구분은 그다지 중요하지 않으므로 이 용어를 느슨하게 사용할 것이다.

## 컨테이너 101
컨테이너의 핵심은 [커널 네임스페이스와 crgoup](https://medium.com/@saschagrunert/demystifying-containers-part-i-kernel-space-2c53d6979504)을 사용하여 호스트 컴퓨터의 다른 프로세스(가장 일반적으로는 랩톱 또는 클라우드의 VM)로부터 자신을 격리하는 독립된 프로세스이다. 이는 크게 두 가지 의미를 갖고 있다.
- 각 컨테이너에는 코드 자체부터 종속성, 구성 파일, 환경 변수 등 어플리케이션에 필요한 모든 것이 포함되어야 한다. 즉, 컨테이너는 이식성이 있어야 한다.
- 컨테이너는 서로 격리되어 있으므로 동일한 컴퓨터에서 여러 컨테이너를 실행할 수 있다.

컨테이너는 가상 머신에서 어플리케이션을 직접 실행하는 것과 비교하여 컨테이너는 OS를 가상화하므로 대부분의 마이크로서비스 아키텍처에서 더 가볍고 리소스 효율적이다.

![](./images/docker-basics/fig-02.webp)

Docker는 이러한 컨테이너를 빌드하고 실행할 수 있는 플랫폼을 제공한다. 예를 들어, Docker파일에 정의된 컨테이너를 패키징하는 CLI 도구를 제공한다. 또한 환경 변수를 설정하여 구성을 재정의하고 지속성을 위해 스토리지를 연결할 수 있는 방법도 제공한다.

많은 세부 사항을 간략하게 설명했지만 자세한 내용은 훌륭한 튜토리얼이 많이 있으므로 필요하면 읽어 보도록 하자.
- [Docker’s getting started guide](https://docs.docker.com/get-started/)
- [Docker for beginners](https://docker-curriculum.com/)

## Docker 네트워킹과 다중 컨테이너
Docker의 가장 중요한(그리고 흔히 오해하는) 측면 중 하나는 네트워킹이다. 컨테이너가 격리된 프로세스라고 해서 다른 컨테이너와 통신하지 않는다는 의미는 아니다. 실제로 대부분의 최신 어플리케이션의 경우, 데이터베이스든 다른 구성 요소든 적어도 하나 이상의 다른 서비스와 통신할 가능성이 크다. 예를 들어, 간단한 웹 어플리케이션은 프론트엔드, 백엔드, 데이터베이스 구성 요소와 확장에 따라 캐시 또는 메시지 큐같은 또 다른 구성 요소를 가질 수 있다.

Docker는 컨테이너 간의 통신을 할 수 있도록 여러 네트워크를 편리하게 제공한다. `docker network ls`를 실행하면 다음과 같이 결과를 출력한다.

```bash
$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
91659235fd82   bridge    bridge    local
bfddd8fa529d   host      host      local
8b86e52edd57   none      null      local
```


## 브릿지 네트워크(Bridge Network)
기본적으로 컨테이너는 '브릿지' 네트워크에서 실행된다. 각 컨테이너에는 IP 주소가 할당된다. 즉, 컨테이너는 IP 주소를 지정하여 서로 통신할 수 있다.

네트워킹을 테스트하기 위해 두 개의 알파인 이미지를 실행해 보자.

```bash
$ docker run -dit --name container1 alpine sh
$ docker run -dit --name container2 alpine sh
```

그러면 대화형 TTY가 활성화된 백그라운드에서 container1 및 container2라는 이름의 알파인 컨테이너 두 개가 나타난다. `docker ps`를 실행한다.

```bash
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED          STATUS          PORTS     NAMES
d60077d34d22   alpine    "sh"      7 minutes ago    Up 7 minutes              container2
a09342ce6ce5   alpine    "sh"      10 minutes ago   Up 10 minutes             container1
```

IP 주소를 파악하기 위하여 `docker inspect bridge` 명령으로 네트워크를 검사하여 보자.

```bash
$ docker inspect bridge
[
    {
        "Name": "bridge",
        "Id": "91659235fd8217013acfc94aadb2274e187385b4c86c86ca7e1a4d9eb86a007f",
        "Created": "2023-07-21T01:49:26.466861416Z",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "a09342ce6ce5c5d47da2fc6a016a7e64c590bac2933f0a7a0da247f7304c2d09": {
                "Name": "container1",
                "EndpointID": "550fe53423c3d23e8c667ea53e6bbab5f16d4dee052f1c16e4f71f44596a82d8",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "d60077d34d22f790723c00c2c870157dfd217f909c3fe99f7af4f86c4008f875": {
                "Name": "container2",
                "EndpointID": "2f7199a3f237010b2daa7a51e7b97908ffd33b6187a856cab72b6b92024a0f0e",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "65535"
        },
        "Labels": {}
    }
]
```

`container1`에는 `172.17.0.2`가 할당되고 `container2`는 `172.17.0.3`에서 실행되고 있음을 알 수 있다.

이제 `container1`에 연결하여 `container2`를 핑할 수 있다.

```bash
docker attach container1 
/ # ping -c l 172.17.0.3 
ping: invalid number 'l'
/ # ping -c 1 172.17.0.3 
PING 172.17.0.3 (172.17.0.3): 56 data bytes
64 bytes from 172.17.0.3: seq=0 ttl=64 time=1.117 ms

--- 172.17.0.3 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 1.117/1.117/1.117 ms
```

안타깝게도 DNS resolution 기능은 제공되지 않는다. 따라서 컨테이너 이름으로 핑을 시도하면 실패한다.

```bash
/ # ping -c 1 container2
ping: bad address 'container2'
```

이제 이 동작은 그다지 유용하지 않다. 컨테이너는 본질적으로 임시적이므로 컨테이너가 다시 시작되면 새 IP 주소가 할당될 수 있다. 주기적으로 네트워크를 검사할 수는 있지만 작업하기가 쉽지 않다.

반면에 사용자 정의 네트워크는 자동 서비스 검색 기능을 제공한다. 이를 테스트하기 위해 네트워크를 생성하고 핑 테스트를 다시 실행해 보겠다.

```bash
$ docker network create test
$ docker run -dit --name container3 --network test alpine sh
$ docker run -dit --name container4 --network test alpine sh
```

이제 `container3`에 연결하여 이름으로 `container4`를 핑할 수 있다.

```bash
$ docker attach container3
/ # ping -c 1 container4
PING container4 (172.18.0.3): 56 data bytes
64 bytes from 172.18.0.3: seq=0 ttl=64 time=0.389 ms

--- container4 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.389/0.389/0.389 ms
```

실제로, 이는 동일한 네트워크에서 여러 서비스가 실행되는 Docker Compose에서 가장 자주 사용된다. 웹 어플리케이션 예제로 돌아가서, 이제 컨테이너는 IP 주소 대신 이름으로 서로 대화할 수 있다.

## 호스트 네트워크
다른 유용한 방법은 호스트 네트워크에 바인딩하는 것이다. 이를 통해 컨테이너는 프로세스 수준 격리를 유지하면서도 해당 포트를 Docker의 호스트 네트워크에 노출할 수 있다.

예시를 위해 `docker run -d — network host nginx` 명령으로 컨테이너를 실행해 보겠다. 

`http://localhost:80/` 로 이동하면 일반적인 "Welcome to nginx!" 메시지가 나타난다.

![](./images/docker-basics/fig-09.webp)

이 글을 따라가면서 Mac을 사용 중이라면 아마도 이 방법이 작동하지 않는다는 것을 눈치챘을 것이다. 이는 Docker 데몬이 실제로 Mac의 가상 머신에서 실행되기 때문이다. 그래서 다른 방법을 사용하여야 한다. Mac에서는 `--publish`, `-p` 명령을 사용하여 트래픽을 호스트 네트워크로 전달한다.

```bash
$ docker run -d -p 8080:80 nginx
```

이제 `localhost:8080`으로 이동하여 nginx 페이지를 볼 수 있어야 한다. Docker Compose에서 이 필드는 `ports` 아래에 노출된다. "port-forwarding"에 대한 이 주제는 Kubernetes 네트워킹을 이해하는 데 중요하므로 기억해두자.

## 확장과 축소
실제로 개발자는 다음과 같은 방식으로 Docker와 상호작용할 가능성이 높다.
- 일부 레지스트리에 게시되고 클라우드에서 실행되는 Docker 이미지 만들기
- 컨테이너화된 구성 요소를 사용하여 로컬에서 통합 테스트를 실행한다. 단위 테스트에 [testcontainer](https://testcontainers.com/)를 사용할 수도 있다.
- Docker Compose를 사용하여 CI에서 테스트 실행하기

이 모든 시나리오에서 Docker는 그 역할을 잘 수행한다. 코드를 패키징하여 프로덕션에서 실행할 수 있으며, 여러 서비스와 관련된 테스트를 작성할 수도 있다.

그러나 이제 솔루션을 수십, 수백, 수천 개의 컨테이너로 확장해 보자. 이때 모든 컨테이너가 동일한 호스트 머신에 들어갈 수 없다. 컨테이너가 서로 다른 머신에서 실행될 때 어떻게 컨테이너가 서로 대화할 수 있을까? 또는 자동 확장, 자동 업그레이드/롤백, 구성 관리 등의 문제를 어떻게 처리할까? 그리고 로컬 컴퓨터와 클라우드 환경 간의 서로 다른 Docker 환경에서 발생하는 문제를 어떻게 처리할까?

바로 여기에 **컨테이너 오케스트레이터(orchestrator)**이 필요하다.

다음 파트부터는 Kubernetes가 무엇인지, 그리고 Docker 지식을 바탕으로 어떻게 구축할 수 있는지 살펴보도록 하자.

![](./images/docker-basics/fig-10.webp)
