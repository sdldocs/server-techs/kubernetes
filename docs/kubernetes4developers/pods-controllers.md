"개발자를 위한 쿠버네티스" 시리즈의 Part III에 오신 것을 환영합니다!

이제 아주 기초적인 것부터 시작하여 Kubernetes의 개념에 대해 자세히 살펴보겠다. Kubernetes는 종종 학습 곡선이 가파른 것으로 알려져 있다. 어느 정도는 사실이지만, 개발자로서 Kubernetes의 생산적인 사용자가 되기 위해 모든 것을 배울 필요는 없다. 사실, 어플리케이션을 살펴보고 빌드, 실행과 디버깅을 시작하려면 빙산의 일각에 해당하는 지식만 있으면 된다. 대부분의 조직에서 "어려운" 작업은 관리 및 프로비저닝을 담당하는 인프라 팀에 소속된 Kubernetes 전문가가 처리할 것이기 때문이다.

그러니 더 이상 고민할 필요 없이 바로 시작하겠다!

## Pods와 Nodes
**pod**는 Kubernetes에서 배포 가능한 가장 작은 단위이다. 좀 더 실용적인 용어로, Kubernetes가 배포하고 관리하는 공유 리소스(예: 스토리지, 네트워크)가 있는 하나 이상의 컨테이너 그룹이다. pod는 필요한 컨테이너와 리소스를 실행하기 위한 조건과 함께 정의한다. 그런 다음, Kubernetes 컨트롤러는 이러한 사양을 갖고 노드들에서 pod를 스케줄링한다.

**node**는 Kubernetes가 관리하는 가상 또는 물리적 머신이다. 클라우드 기반 Kubernetes 클러스터와 상호 작용하는 경우, 이들은 단순히 Kubernetes 구성 요소를 실행하는 EC2 머신과 같은 가상 머신이다(다음에 다룰 예정). 이러한 node에 사용 가능한 리소스가 있고 일부 조건(예: [taints와 tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/))에 구속되지 않는 한, pod가 해당 node에서 실행되도록 스케줄링할 수 있다. Docker에서와 마찬가지로, 단일 node에서 여러 개의 pod를 실행할 수 있다.

![](./images/pods-controllers/fig_01.webp)

이론적으로 pod의 컨테이너 개수에 제한이 없지만, 실제로는 가장 일반적인 다음과 같은 방식으로 사용된다.

- **pod당 한 컨테이너**: 대부분의 경우, pod는 컨테이너를 감싸는 래퍼일 뿐이다.
- **pod당 컨테이너 + "사이드카"**: 일부 어플리케이션은 헬퍼 기능을 제공하는 사이드카 컨테이너와 함께 실행된다. 가장 일반적으로 로그 포워딩, 네트워킹 프록시 또는 구성 관리가 여기에 포함된다.
- **긴밀하게 결합된 어플리케이션**: 스토리지 리소스를 직접 공유해야 하는 긴밀하게 결합된 어플리케이션들을 단일 pod에 배포하기도 한다.

직관적인 이름의 두가지 특별한 유형의 컨테이너가 있다.

- **Init 컨테이너**: Init 컨테이너는 어플리케이션 컨테이너가 시작되기 전에 항상 실행된다. 이는 설정(setuo) 스크립트(예: 데이터베이스 마이그레이션, 구성 재로드 (configuration reload) 등)를 실행하는 데 유용하다.

- **임시(Ephemeral) 컨테이너**: 쿠버네티스 v1.25에 도입된 임시 컨테이너는 문제 해결을 위해 기존 pod 내에서 일시적으로 실행된다. 이는 pod로 실행할 수 없는 경우(예: pod가 충돌하거나 컨테이너가 쉘없이 배포되지 않은 이미지를 실행하는 경우)에 유용할 수 있다.

## Pod 생명주기
Docker의 컨테이너와 마찬가지로 pod는 본질적으로 임시적이라는 점은 중요하다. pod가 생성되면 고유 ID가 부여되고 노드내에서 스케줄된다. 그런 다음 작업 완료, 오류 또는 축출로 인해 중지될 때까지 해당 노드에서 실행된다. 해당 노드에 장애가 발생하면, pod는 스케줄이 변경되지 않고 삭제되고 새 ID를 가진 새 pod가 스케줄된다.

pod는 또한 간단하고 설명이 필요없는 단계를 따른다.

1. **Pending**: pod가 스케줄링을 기다리거나 실행에 필요한 아티팩트를 다운로드하는 중(예: 도커 이미지 다운로드).
2. **Running**: pod의 컨테이너 중 하나 이상이 실행 중이거나 재시작 중임.
3. **Succeeded**: pod의 모든 컨테이너가 성공적으로 종료되었다. 이는 초기화 컨테이너 또는 크론 작업과 같이 정의된 종료 상태를 가진 컨테이너와 관련이 있다.
4. **Failed**: 하나 이상의 컨테이너가 실패로 종료되었다.
5. **Unknown**: 일반적으로 Kubernetes가 pod와 통신하여 상태를 확인할 수 없음을 나타낸다.

Kubernetes는 개별 컨테이너의 상태 Waiting, Running 또는 Terinated를 노출한다. Docker와 마찬가지로, 각 컨테이너는 pod 상태를 나타내는 프로브(Docker healthcheck와 유사)뿐만 아니라 restartPolicy를 가질 수 있다.

## Pod 컨트롤러
디버깅 목적으로 임시 컨테이너를 실행하는 경우를 제외하고, pod는 거의 직접 배포되지 않는다. 대신, [컨트롤러](https://kubernetes.io/docs/concepts/architecture/controller/)를 통해 배포와 관리된다. Kubernetes는 컨트롤러가 시스템의 상태를 조절하는 로봇 공학에서의 컨트롤러 개념을 차용한다. pod와 관련하여 컨트롤러는 복제, 자동 스케일링, 스케줄링 기본 설정, 자가 복구 등 사양에 정의된 대로 pod 상태를 추적한다.

알아야 할 중요한 컨트롤러 유형은 다음과 같다.

- [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
- [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)
- [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)

stateless 어플리케이션의 경우, Deployments를 다루게 된다. Deployment는 컨테이너 이미지에서 레플리카 수에 이르기까지 pod의 상태를 정의한다. 새 버전을 배포하려는 경우, Deployment는 새 ReplicaSet을 생성하고 새 버전으로 롤링(rolling) deployment를 수행한다. 이렇게 하면 원하는 경우 쿠버네티스를 이전 버전으로 롤백할 수 있다.

![](images/pods-controllers/fig_02.png)

이름에서 알 수 있듯이 StatefulSet은 상태를 보존해야 하는 어플리케이션을 위한 것이다. Kubernetes 내에서 데이터베이스를 배포하는 경우 가장 많이 접하게 된다. StatefulSet은 pod의 보장된 순서 또는 안정적인 고유 식별자가 필요한 경우에도 유용하다. 합의(consensus) 알고리즘(예: 블록체인)을 다룰 때 이러한 시나리오에 직면할 수 있다.

마지막으로, DaemonSet은 각 node에서 pod가 실행되도록 해야 할 때 유용하다. Kubernetes 관리자가 fluentbit와 같은 log forwarder나 일부 보안 플러그인을 모든 노드에 설치하는 데 주로 사용된다.

## 주요 내용
Pod는 쿠버네티스의 작동 방식을 이해하는 데 기본이다. 다행히도 Docker에 대한 지식이 있다면 파드 == 하나 이상의 컨테이너인 것을 쉽게 파악할 수 있다. "Kubernetes" 계층은 컨트롤러 수준에서 제공됩니다. 배포(Deployment), 스테이트풀셋(StatefulSets)과 데몬셋(DaemonSets)은 이러한 동작을 코드화하기 위해 복제와 자가 복구 속성 같은 오케스트레이션 세부 사항을 정의하는 방법이다.

이제 기본 Kubernetes 객체에 대해 이해했으니 다음에는 Kubernetes 구조와 흔히 오해하는 몇 가지 네트워킹 개념을 살펴보겠다.
