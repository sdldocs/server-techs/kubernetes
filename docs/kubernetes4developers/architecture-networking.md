[이전 포스팅](pods-controllers.md)에서는 *pod*, *node*와 *pod 컨트롤러*가 무엇인지 살펴보며 Kubernetes의 기본 사항에 대해 배우기 시작했다. 이제 Kubernetes에서 무엇이 실행되는지(즉, pod)를 알았으니 이제 Kubernetes의 상위 수준의 구조와 pod가 서로 통신하는 방법에 대해 알아보겠다. 저자의 경험에 비추어 볼 때, Kubernetes 네트워킹은 항상 개발자들을 혼란스럽게 하는 경향이 있다. 하지만 일단 Docker 네트워킹이 어떻게 작동하는지 이해했다면 유사점을 발견하고 이러한 개념을 더 쉽게 이해할 수 있을 것이다([Part II Docker 기초](docker-basics.md)를 아직 읽지 않았다면 지금이 복습하기에 좋은 시기이다).

더 이상 설명 없이 바로 시작한다!

## Kubernetes 구조
[이전 포스팅](pods-controllers.md)에서 pod가 Kubernetes에서 배포 가능한 가장 작은 단위라는 것을 배웠다. 다른 측면에서 보면, Kubernetes 클러스터는 하나 이상의 node에서 실행되는 모든 구성 요소를 포함한다.

높은 수준에서 Kubernetes는 구성 요소들의 세 세트로 구성되어 있다.

- **control plane 구성 요소**: API 서버, etcd, kube-scheduler, kube-controller-manager
- **node 구성 요소**: kubelet, kube-proxy, container runtime
- **add-on**: DNS, 네트워킹 플러그인 등

Kubernetes 문서 웹사이트에는 이러한 모든 구성 요소에 대한 훌륭한 다이어그램(아래 그림)과 설명이 있다. 각 구성 요소를 자세히 설명하지는 않겠지만, 핵심 개념과 요점을 나열해 보겠다.

![](images/architecture-networking/kubernetes_cluster.png)

1. Kubernetes는 control plane에서 실행되는 API 서버가 모든 요청을 수신하는 "hub-and-spoke" API 패턴을 사용한다.
2. pod 컨트롤러(예: Deployment, StatefulSet, DaemonSet)의 컨트롤러 패턴을 기억하는가? control plane 컴포넌트는 컨트롤러를 실행하여 node와 기타 작업 같은 다른 Kubernetes 컴포넌트를 관리하기도 한다.
3. kube-scheduler는 리소스 요청과 선호도 규칙 또는 taint같은 기타 제약 조건 등 다른 요소들을 고려하여 node에 pod를 할당한다.
4. 각 node에는 pod의 모든 컨테이너가 지정된 대로 실행되고 있는지 확인하는 *kubelet*이라는 에이전트가 있다.
5. 통신은 *kube-proxy*를 통해 설정된다. 이 구성 요소는 클러스터에서 네트워킹을 활성화하기 위해 pod의 트래픽 요청을 전달한다.

[Part II Docker 기초](docker-basics.md)에서 컨테이너 오케스트레이터가 필요한 이유에 대해 설명한 것을 기억해보자. 그리고 위에서 언급한 이러한 모든 구성 요소들은 함께 작동하여 서비스 검색(kube-proxy, DNS), 자가 복구(kubelet. Kube-controllers, kube-scheduler) 등의 이점을 제공한다.

마지막으로, 한 가지만 기억한다면, **컨트롤러 패턴은 Kubernetes 구조와 설계의 핵심 원칙을 뒷받침한다는 점을 기억하세요**. Kubernetes는 모든 수준에서 많은 컨트롤러를 사용하여 원하는 상태를 취하고 해당 상태를 유지하기 위해 지속적으로 모니터링한다. 따라서 "원하는" 상태를 더 많이 정의할수록 Kubernetes는 그 결과 더 많은 작업을 수행할 수 있다.

## Kebernetes 네트워킹
이제 많은 엔지니어가 고민하는 주제인 네트워킹에 대해 알아보자.

하지만 자세히 설명하기 전에 한 걸음 물러나서 배경을 정리해 보겠다. [Part II Docker 기초](docker-basics.md)에서는 `bridge` 네트워크와 `host` 네트워크를 통해 Docker가 네트워킹을 처리하는 방법을 살펴보았다. 자체 브리지 네트워크를 생성했을 때 이름으로 다른 컨테이너에 연결할 수 있다는 것을 확인했다.

```bash
$ docker attach container3
/ # ping -c 1 container4
PING container4 (172.18.0.3): 56 data bytes
64 bytes from 172.18.0.3: seq=0 ttl=64 time=0.389 ms

--- container4 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.389/0.389/0.389 ms
```

Kubernetes를 사용하면 이제 동일한 node에 있을 수도 있고 없을 수도 있는 pod로 작업하고 있다. 또한, 우리는 일반적으로 Docker보다 더 큰 규모로 작업하고 있으므로 동일한 클러스터에서 사용 가능한 모든 엔드포인트에 대한 서비스 검색이 있으면 좋을 것이다. Kubernetes 구조에 대한 개요를 통해 Kubernetes에는 토대를 마련하는 몇 가지 구성 요소(예: kube-proxy, DNS 서비스)가 있다는 것을 알고 있지만 실제로 어떻게 작동할까?

## 서비스
Kubernetes *services*를 입력한다.

Kubernetes 서비스는 pod를 액세스하기 위한 논리적 엔드포인트 집합을 정의하는 추상화 계층이다. pod 앞에 있는 클러스터 내 로드 밸런서라고 생각할 수 있다.

예를 들어 설명해 보겠다. 여기에는 5개 복제본이 있는 `nginx`에 대한 간단한 배포가 있다. `http`라는 이름으로 포트 80을 열었음을 알 수 있다.

```yml
apiVersion: app/v1
kind: Deployment
metadata:
  name: nginx
  labels:
    app: ngnix
spec:
  replicas: 5
  selector: 
    matchLabels:
      app: ngnix
  template:
    metadata:
      labels:
        app: ngnix
    spec:
      containers:
        name: nginx
        image: nginx:1.24
        ports:
          containerPort: 80
          name: http
```

nginx는 상태를 저장하지 않기 때문에 네트워크 호출을 5개의 nginx 파드에 균등하게 분산시키고, 이를 위해 서비스를 생성한다.

여기서 주의해야 할 중요한 부분은 다음과 같다.

- **name**: nginx-service는 서비스 이름입니다.
- **selector**: `app: nginx`의 레이블 키-값 쌍을 일치시켜 pod를 선택했다.
- **targetPort**: nginx pod의 포트 80에 매핑되는 `http`에 명명된 포트를 타겟팅한다.
- **port**: 서비스를 호출하는 다른 사용자에게 노출하려는 포트이다.

이제 다른 pod가 `nginx-service:8080`을 호출하여 nginx pod와 통신할 수 있다.

> **Note**: 상태를 저장해야 하는 어플리케이션의 경우, *headless service*를 사용하여 대화하려는 특정 pod를 구분해야 한다. 이것은 고급 주제이므로 자세한 내용은 [문서](https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/)를 참고하세요. 그러나 기본 개념을 여전히 적용할 수 있다.

## 서비스 타입
Kybernetes에는 실제로 네 가지 타입의 서비스가 있다. nginx 예에서는 타입을 지정하지 않았기 때문에 암시적으로 **ClusterIP** 타입의 서비스를 생성했다. 이렇게 하면 어플리케이션이 클러스터의 다른 사용자에게는 노출되지만, 클러스터 외부의 서비스(예: 외부 대면 API 서비스, 프론트엔드 등)가 클러스터에 도달하도록 하려면 어떻게 해야 할까?

이 경우 다른 세 가지 타입의 서비스가 필요하다.

- **NodePort**: 클러스터의 각 노드에 정적 포트를 할당한다. 서비스 정의에서 이 포트 번호를 지정하거나 아직 사용되지 않은 포트를 Kubernetes가 할당하도록 할 수 있다.
- **LoadBlancer**: 외부 로드밸런서 뒤에 서비스를 노출한다. Kubernetes는 로드밸런서 컴포넌트를 기본적으로 제공하지 않으므로 클라우드 제품이나 MetalLB와 같은 외부 서비스를 사용해야 한다.
- **ExternalName**: 클러스터 DNS 서비스의 CNAME 레코드를 구성하여 서비스를 외부 호스트 이름 값에 매핑한다(예: nginx 서비스를 nginx.example.com에 매핑).

실제로는 ClusterIP와 NodePort만 사용할 가능성이 높다. 그 이유는 클라우드 제공자나 인프라 팀이 프로비저닝하고 관리하는 클러스터는 일반적으로 [ingress 컨트롤러](https://medium.com/swlh/kubernetes-ingress-controller-overview-81abbaca19ec)를 사용하여 클러스터에 대한 역방향 프록시를 설정하기 때문이다. AWS Ingress 컨트롤러(예: ALB, NLB)와 같은 클라우드 제공업체의 Ingress 제품을 사용하려면 기본 서비스 유형이 NodePort여야 합니다. NGINX, Traefik 또는 Kong과 같은 타사 Ingress를 사용하는 경우, ingress 뒤에 ClusterIP를 노출할 수 있다.

> **Note**: Kubernetes Ingress 컨트롤러에 대한 심층적인 가이드는 [여기](https://medium.com/swlh/kubernetes-ingress-controller-overview-81abbaca19ec)를 읽어볼 수 있다.

## DNS 리솔루션
네트워킹 측면에서 알아야 할 몇 가지 다른 뉘앙스가 있다. nginx 예로 돌아가서, 다른 파드가 `nginx-service:8080`을 호출하여 어플리케이션과 대화할 수 있다고 하였다. 하지만 더 정확하게 말하면, 우리의 nginx 배포와 동일한 네임스페이스에 있는 pod만 `nginx-service:8080`을 호출할 수 있다.

Kubernetes의 네임스페이스는 Kubernetes 리소스를 격리하기 위한 소프트웨어 정의 메커니즘이다. 클러스터 내에서 리소스를 구성하거나 소프트 멀티테넌시를 구현하는 방법이라고 생각하면 된다(예: 팀별, 엔지니어별 또는 CI 실행별로 네임스페이스를 가질 수 있음).

배포와 서비스와 같이 네임스페이스에 따라 범위가 지정된 Kubernetes 객체가 있는 반면, 스토리지 또는 node와 관련된 객체처럼 클러스터 전체에 걸쳐 있는 객체도 있다. 기본적으로 Kubernetes는 네임스페이스 간 통신을 허용한다. 하지만 네임스페이스 간 연결하려면 `<service-name>.<namespace-name>.svc.cluster.local` 형식의 정규화된 도메인 이름(FQDN, fully qualified domain name)을 사용해야 한다.

원래 nginx 배포가 `test-ns-1`에서 실행 중이라고 가정해 보자. `test-ns-1` 내에서 호출하는 경우, `nginx-service:8080` 또는 `nginx-service.test-ns-1.svc.cluster.local:8080`같은 FQDN을 사용할 수 있다. 이제 다른 네임스페이스에서 호출하는 경우, FQDN: `nginx-service.test-ns-1.svc.cluster.local:8080`을 사용해야 한다. Kubernetes DNS는 부분적으로도 확인할 수 있으므로 `nginx-service.test-ns-1:8080`으로 호출해도 작동한다.

## 로컬에서 실행
마지막으로, 개발자들이 Kubernetes 네트워킹을 혼동하는 또 다른 경우는 미니큐브(minikube)나 kind 같은 소규모 배포를 통해 로컬에서 실행하는 경우이다. 도커의 호스트 네트워크에 익숙한 개발자의 경우, 모든 것을 로컬호스트에 매핑하기 위해 `-p` 플래그를 전달할 수 없다는 사실에 종종 어려워 한다.

이 문제를 해결할 수 있는 몇 가지 방법이 있다.

- 시작 시 특정 포트 또는 포트 범위를 로컬호스트에 매핑하도록 Kubernetes 배포를 구성할 수 있다(MacOS에서는 작동하지 않을 수 있음).
- `minikube ingress`와 같은 add-on 서비스를 사용하거나 `minikube tunnel` 같은 [helper 명령](https://minikube.sigs.k8s.io/docs/start/)을 사용하여 서비스에서 여는 특정 포트로 매핑한다. 이는 Kubernetes 배포에 따라 좌우된다.
- 또는 `-p` 플래그를 통해 Docker 호스트 네트워크 매핑 동작을 모방하기 위해 [Kubernetes 포트 포워딩](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)을 사용하세요.

## 마무리
이 Part에서 많은 내용을 다루었다. 하지만 Docker의 개념이 어떻게 Kubernetes로 변환되는지 확인할 수 있었기를 바란다. 기억해야 할 두 가지 주요 사항은 1) 많은 Kubernetes 컴포넌트가 컨트롤러 패턴을 사용한다는 것과 2) 서비스는 pod와 통신할 수 있도록 Kubernetes가 제공하는 추상화라는 것이다.

보다 실용적인 문제로 넘어가기 전에 리소스 관리와 스케줄링이라는 주제를 한 가지 더 다룰 것이다. 개발자는 이러한 값과 동작을 직접 설정하지 않을 수도 있지만, 이러한 값과 동작을 알면 장애를 훨씬 더 빠르게 이해하고 디버깅하는 데 도움이 된다.
