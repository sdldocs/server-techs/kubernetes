Kubernetes는 컨테이너화된 어플리케이션의 배포, 확장과 관리를 자동화하기 위한 오픈 소스 컨테이너 오케스트레이션 엔진이다. 이 오픈 소스 프로젝트는 클라우드 네이티브 컴퓨팅 재단([CNCF](https://www.cncf.io/about))에서 호스팅하고 있다.

## Kubernetes 이해
Kubernetes와 그 기본 개념에 대해 알아본다.

- [개요](./concepts/overview/index.md)
- [Kubernetes 구성요소](./concepts/overview/kubernetes-components.md)
- [Kubernetes API](./concepts/overview/kubernetes-api.md)
- [Kubernetes 객체](./concepts/overview/working-with-objects/index.md)
- [Pods](./concepts/workloads/pods/index.md)

## Kubernetes 체험
- [Hello Minikube](./tutorials/hello-minikube.md)
- [Kubernetes 기초](./tutorials/kubernetes-basics/index.md)
- [Stateless 예: Redis를 사용한 PHP 방명록](./tutorials/stateless-applications/guestbook.md)
- [Stateful 예: 퍼시스턴트 볼륨이 있는 워드프레스와 MySQL 배포](./tutorials/stateful-applications/mysql-wordpress-persistent-volume.md)
