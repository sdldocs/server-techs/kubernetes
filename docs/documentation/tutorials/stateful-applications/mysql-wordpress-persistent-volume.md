이 튜토리얼에서는 Minikube를 사용하여 WordPress 사이트와 MySQL 데이터베이스를 배포하는 방법을 보인다. 두 어플리케이션 모두 PersistentVolumes과 PersistentVolumeClaims을 사용하여 데이터를 저장한다.

[PersistentVolumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)(PV)은 관리자가 수동으로 프로비저닝하거나 [StorageClass](https://kubernetes.io/docs/concepts/storage/storage-classes)를 사용하여 Kubernetes가 동적으로 프로비저닝한 클러스터의 스토리지 조각이다. [PersistentVolumeClaims](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)(PVC)은 PV가 처리할 수 있는 사용자의 스토리지 요청이다. PersistentVolumes과 PersistentVolumeClaims은 Pod 수명주기와 독립적이며, 재시작, 스케줄 재조정, 심지어 Pod 삭제를 통해 데이터를 보존한다.

> **Warnong**: 이 배포는 단일 인스턴스 워드프레스와 MySQL Pod를 사용하므로 프로덕션 사용 사례에는 적합하지 않다. 프로덕션 환경에서 워드프레스를 배포하려면 [워드프레스 헬름 차트](https://github.com/bitnami/charts/tree/master/bitnami/wordpress)를 사용하는 것을 고려하여야 한다.

> **Note**: 이 튜토리얼에서 제공하는 파일은 GA 배포 API를 사용하며 Kubernetes 버전 1.9 이상에만 해당된다. 이 튜토리얼를 이전 버전의 Kubernetes에서 사용하려면 API 버전을 적절히 업데이트하거나 이 튜토리얼의 이전 버전을 참조하세요.

## 목표
- PersistentVolumeClaims과 PersistentVolumes 만들기
- 다음을 사용하여 `kustomization.yaml`을 만들기
    - Secret 생성기
    - MySQL 리소스 구성
    - 워드프레스 리소스 구성
- `kubectl apply -k ./`로 kustomization 디렉터리를 적용한다.
- 정리

## 시작을 위하여
Kubernetes 클러스터가 있어야 하며, 클러스터와 통신하도록 kubectl 명령 도구가 구성되어 있어야 한다. 컨트롤 플레인 호스트로 작동하지 않는 노드가 2개 이상 있는 클러스터에서 이 튜토리얼을 실행하는 것이 좋다. 클러스터가 아직 없는 경우, [minikube](https://minikube.sigs.k8s.io/docs/tutorials/multi_node/)를 사용하여 클러스터를 생성하거나 다음 Kubernetes 플레이그라운드 중 하나를 사용할 수 있다.

- [Killercoda](https://killercoda.com/playgrounds/scenario/kubernetes)
- [Play with Kubernetes](https://labs.play-with-k8s.com/)

버전을 확인하려면 `kubectl version`을 입력한다.

이 페이지에 표시된 예는 `kubectl` 1.27 이상에서 작동한다.

다음 구성 파일을 다운로드한다.

- [mysql-deployment.yaml](https://kubernetes.io/examples/application/wordpress/mysql-deployment.yaml)
- [wordpress-deployment.yaml](https://kubernetes.io/examples/application/wordpress/wordpress-deployment.yaml)

## PersistentVolumes과 PersistentVolumeClaims 생성
MySQL과 Wordpress는 각각 데이터를 저장하기 위해 PersistentVolumes이 필요하다. 해당 PersistentVolumeClaims은 배포 단계에서 생성된다.

많은 클러스터 환경에는 기본 StorageClass가 설치되어 있다. PersistentVolumeClaims에 StorageClass가 지정되지 않은 경우, 클러스터의 기본 StorageClass가 대신 사용된다.

PersistentVolumeClaims이 생성되면, PersistentVolumes은 StorageClass 구성을 기반으로 동적으로 프로비저닝된다.

> **Warning**: 로컬 클러스터에서 기본 StorageClass는 `hostPath` 프로비저너를 사용한다. `hostPath` 볼륨은 개발과 테스트에만 적합하다. `hostPath` 볼륨을 사용하면, 데이터는 Pod가 스케줄된 node의 `/tmp`에 저장되며 node 간에 이동하지 않는다. Pod가 죽어서 클러스터의 다른 node로 스케줄되거나 node가 재부팅되면 데이터가 손실된다.

> **Note**: `hostPath` 프로비저너를 사용해야 하는 클러스터를 불러오는 경우, `controller-manaer` 구성 요소에서 `--enable-hostpath-provisioner` 플래그를 설정해야 한다.

> **Note**: Google Kubernetes 엔진에서 실행 중인 Kubernetes 클러스터가 있는 경우 [이 가이드](https://cloud.google.com/kubernetes-engine/docs/tutorials/persistent-disk)를 따르세요.

## `kustomizarion.yaml` 생성

### Secret 생성기 추가
Secret은 비밀번호나 키와 같은 민감한 데이터를 저장하는 객체이다. 1.14부터 `kubectl`은 커스터마이제이션 파일을 사용하여 쿠버네티스 객체를 관리할 수 있도록 지원한다. Secret은 `kustomization.yaml`의 생성기를 사용하여 생성할 수 있다.

다음 명령으로 `kustomization.yaml`에 Secret 생성기를 추가한다. `YOUR_PASSWORD`를 사용하려는 비밀번호로 대체해야 한다.

```bash
$ cat <<EOF >./kustomization.yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=YOUR_PASSWORD
EOF
```

## MySQL 및 WordPress에 대한 리소스 구성 추가
다음 매니페스트는 단일 인스턴스 MySQL Deployment에 대해 설명한다. MySQL 컨테이너는 PersistentVolumes을 `/var/lib/mysql`에 마운트한다. `MYSQL_ROOT_PASSWORD` 환경 변수는 Secret에서 데이터베이스 비밀번호를 설정한다.

```yml
# application/wordpress/mysql-deployment.yaml
apiVersion: v1
kind: Service
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  ports:
    - port: 3306
  selector:
    app: wordpress
    tier: mysql
  clusterIP: None
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
  labels:
    app: wordpress
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  selector:
    matchLabels:
      app: wordpress
      tier: mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: mysql
    spec:
      containers:
      - image: mysql:8.0
        name: mysql
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password
        - name: MYSQL_DATABASE
          value: wordpress
        - name: MYSQL_USER
          value: wordpress
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-pv-claim
```

다음 매니페스트는 단일 인스턴스 WordPress Deployment에 대해 설명한다. Wordpress 컨테이너는 웹사이트 데이터 파일을 위해 `/var/www/html`에 PersistentVolumes을 마운트한다. `WORDPRESS_DB_HOST` 환경 변수는 위에 정의된 MySQL 서비스의 이름을 설정하며, WordPress는 서비스별로 데이터베이스를 액세스한다. `WORDPRESS_DB_PASSWORD` 환경 변수는 커스텀으로 생성된 Secret 데이터베이스 비밀번호를 설정한다.

```yml
# application/wordpress/wordpress-deployment.yaml
apiVersion: v1
kind: Service
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  ports:
    - port: 80
  selector:
    app: wordpress
    tier: frontend
  type: LoadBalancer
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: wp-pv-claim
  labels:
    app: wordpress
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  selector:
    matchLabels:
      app: wordpress
      tier: frontend
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: frontend
    spec:
      containers:
      - image: wordpress:6.2.1-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: wordpress-mysql
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password
        - name: WORDPRESS_DB_USER
          value: wordpress
        ports:
        - containerPort: 80
          name: wordpress
        volumeMounts:
        - name: wordpress-persistent-storage
          mountPath: /var/www/html
      volumes:
      - name: wordpress-persistent-storage
        persistentVolumeClaim:
          claimName: wp-pv-claim
```

<span>1.</span> MySQL 배포 구성 파일을 다운로드한다.

```bash
$ curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
```

<span>2.</span> Wordpress 배포 구성 파일을 다은로드 한다.

```bash
$ curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
```

<span>3.</span> `kustomization.yaml` 파일에 이들을 추가한다.

```bash
$ cat <<EOF >>./kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
EOF
```

## 적용과 확인
`kustomization.yaml`에는 WordPress 사이트와 MySQL 데이터베이스를 배포하기 위한 모든 리소스가 포함되어 있다. 디렉터리는 다음과 같이 적용할 수 있다.

```bash
$ kubectl apply -k ./
```

이제 모든 객체가 존재하는지 확인할 수 있다.

<span>1.</span> 다음 명령을 실행하여 Secret이 존재하는지 확인한다.

```bash
$ kubectl get secrets
```

출력은 다음과 같을 것이다.

```
NAME                    TYPE                                  DATA   AGE
mysql-pass-c57bb4t7mf   Opaque                                1      9s
```

<span>2.</span> PersistentVolumes이 동적으로 프로비저닝되었는지 확인한다.

```bash
$ kubectl get pvc
```

> **Note**: PV를 프로비저닝하고 바인딩하는 데 최대 몇 분이 소요될 수도 있다.

출력은 다음과 같을 것이다.

```
NAME             STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
mysql-pv-claim   Bound     pvc-8cbd7b2e-4044-11e9-b2bb-42010a800002   20Gi       RWO            standard           77s
wp-pv-claim      Bound     pvc-8cd0df54-4044-11e9-b2bb-42010a800002   20Gi       RWO            standard           77s
```

<span>3.</span> 다음 명령을 실행하여 Pod가 실행 중인지 확인한다.

```bash
$ kubectl get pods
```

> **Note**: `파드`의 상태가 RUNNING이 되려면 최대 몇 분 정도 걸릴 수 있다.

출력은 다음과 같을 것이다.

```
NAME                               READY     STATUS    RESTARTS   AGE
wordpress-mysql-1894417608-x5dzt   1/1       Running   0          40s
```

<span>4.</span> 다음 명령을 실행하여 서비스가 실행 중인지 확인한다.

```bash
$ kubectl get services wordpress
```

출력은 다음과 같을 것이다.

```
NAME        TYPE            CLUSTER-IP   EXTERNAL-IP   PORT(S)        AGE
wordpress   LoadBalancer    10.0.0.89    <pending>     80:32406/TCP   4m
```

> **Note**: Minikube는 `NodePort`를 통해서만 서비스를 노출할 수 있다. `EXTERNAL-IP`는 항상 pending이다.

<span>5.</span> 다음 명령을 실행하여 WordPress 서비스의 IP 주소를 얻을 수 있다.

```bash
$ minicube service wordpress --url
```

출력은 다음과 같을 것이다.

```
http://1.2.3.4:32406
```

<span>6.</span> IP 주소를 복사하고 브라우저에서 페이지를 로드하여 사이트를 확인한다.

다음 스크린샷과 유사한 WordPress 설정 페이지가 디스플레이된다.

![](../../images/tutorials/WordPress.png)

> **Warning**: 이 페이지의 WordPress 설치를 그대로 두지 마세요. 다른 사용자가 이 페이지를 발견하면 인스턴스에서 웹사이트를 설정하고 악성 콘텐츠를 제공하는 데 사용할 수 있다.

사용자 아이디와 비밀번호를 생성하여 WordPress를 설치하거나 인스턴스를 삭제하세요.

## 정리
<span>1.</span> 다음 명령을 실행하여 Secret, Deployments, 서비스와 PersistentVolumeClaims을 삭제한다.

```bash
$ kubectl delete -k ./
```

## 다음

- [Introspection and Debugging](https://kubernetes.io/docs/tasks/debug/debug-application/debug-running-pod/)에 대해 자세히 알아보기
- [Jobs](https://kubernetes.io/docs/concepts/workloads/controllers/job/)에 대해 자세히 알아보기
- [Port Forwarding](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)에 대해 자세히 알아보기
- [Get a Shell to a Container](https://kubernetes.io/docs/tasks/debug/debug-application/get-shell-running-container/) 방법 알아보기
