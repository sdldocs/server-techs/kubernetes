이 튜토리얼은 StatefulSet으로 어플리케이션을 관리하는 방법을 소개한다. SyayefulSet의 Pod를 생성, 삭제, 확장 및 업데이트하는 방법을 설명한다.

## 시작을 위하여
이 튜토리얼을 시작하기 전에 다음 Kubernetes 개념을 숙지해야 한다.

- [Pod](../../concepts/workloads/pods/index.md)
- [클러스터 DNS](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/)
- [Headless 서비스](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services)
- [PersistentVolumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)
- [PersistentVolumes 프로비저닝](https://github.com/kubernetes/examples/tree/master/staging/persistent-volume-provisioning/)
- [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/kubectl/) 명령 도구

> **Note**: 이 튜토리얼에서는 클러스터가 PersistentVolume을 동적으로 프로비저닝하도록 구성되어 있다고 가정한다. 클러스터가 그렇게 구성되지 않은 경우, 이 튜토리얼을 시작하기 전에 1 GB 볼륨 두 개를 수동으로 프로비저닝해야 한다.

## 목표
StatefulSet은 Stateful 어플리케이션과 분산 시스템을 함께 사용하기 위한 것이다. 그러나 Kubernetes에서 Stateful 어플리케이션과 분산 시스템을 관리하는 것은 광범위하고 복잡한 주제이다. StatefulSet의 기본 기능을 시연하고 전자의 주제를 후자의 주제와 혼동하지 않기 위해 StatefulSet을 사용하여 간단한 웹 어플리케이션을 배포할 것이다.

이 튜토리얼을 마치면 다음 내용을 익히게 될 것이다.

- StatefulSet을 생성하는 방법
- StatefulSet이 파드를 관리하는 방법
- StatefulSet을 삭제하는 방법
- StatefulSet을 확장하는 방법
- StatefulSet의 Pod를 업데이트하는 방법

## StatefulSet 생성
아래 예를 사용하여 StstefulSet을 생성하는 것으로 시작한다. [StstefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/) 개념에 제시된 예와 유사하다. StstefulSet, `web`에 파드의 IP 주소를 게시하기 위해 [headless 서비스](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services)인 `nginx`를 생성한다.

```yml
# application/web/web.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: web
  clusterIP: None
  selector:
    app: nginx
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
spec:
  serviceName: "nginx"
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: registry.k8s.io/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: web
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

위의 예를 다운로드하여 `web.yaml`이라는 파일에 저장한다.

터미널 창 두 개를 사용해야 한다. 첫 번째 터미널에서, StatefulSet의 Pod가 생성되는 것을 보기 위해 `kubectl get`을 사용한다.

```bash
$ kubectl get pods -w -l app=nginx
```

두 번째 터미널에서 [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands/#apply)를 사용하여 `web.yaml`에 정의된 headless 서비스와 StatefulSet을 생성한다.

```bash
$ kubectl apply -f web.yaml
service/nginx created
statefulset.apps/web created
```

위의 명령은 각각 [NGINX](https://www.nginx.com/) 웹서버를 실행하는 두 개의 Pod를 생성한다. `nginx` 서비스 받기...

```bash
$ kubectl get service nginx

NAME      TYPE         CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
nginx     ClusterIP    None         <none>        80/TCP    12s
```

...그런 다음 웹 StatefulSet을 가져와서 둘 다 성공적으로 생성되었는지 확인한다.

```bash
$ kubectl get statefulset web

NAME      DESIRED   CURRENT   AGE
web       2         1         20s
```

### 순서가 있는 파드 생성
*n*개의 복제본이 있는 StatefulSet의 경우, Pod가 배포될 때 *{0..n-1}*으로 순서대로 순차적으로 생성된다. 첫 번째 터미널에서 `kubectl get` 명령의 출력을 살펴본다. 결국 출력은 아래와 같이 보일 것이다.

```bash
$ kubectl get pods -w -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-0     0/1       Pending   0          0s
web-0     0/1       Pending   0         0s
web-0     0/1       ContainerCreating   0         0s
web-0     1/1       Running   0         19s
web-1     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-1     0/1       ContainerCreating   0         0s
web-1     1/1       Running   0         18s
```

`web-1` Pod는 `web-0` Pod가 *`Running`* 중([Pod 단계](../../concepts/workloads/pods/pod-lifecycle.md/#pod-단계) 참조)과 *`Ready`*([Pod 조건](../../concepts/workloads/pods/pod-lifecycle.md/#pod-조건)의 타입 참조)가 될 때까지 시작되지 않는다는 점에 유의한다.

> **Note**: StatefulSet 각 Pod에 할당된 정수로 차례 서수를 구성하려면, 시작 서수를 참조한다.

## StatefulSet의 Pod
StatefulSet의 Pod는 고유한 서수 인덱스와 안정적인 네트워크 아이덴티티를 갖고 있다.

### 파드의 서수 인덱스 살펴보기
StatefulSet의 Pod를 가져온다.

```bash
$ kubectl get pods -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          1m
web-1     1/1       Running   0          1m
```

[StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/) 개념에서 언급했듯이, StatefulSet의 Pod는 고정적이고 고유한 아이덴티티를 갖는다. 이 아이덴티티는 StatefulSet 컨트롤러가 각 Pod에 할당하는 고유한 서수 인덱스를 기반으로 한다.

Pod의 이름은 `<statefulset name>-<ordinal index>` 형식을 취한다. `web` StatefulSet에는 두 개의 복제본이 있기 때문에, `web-0`와 `web-1`이라는 두 개의 Pod를 생성한다.

### 안정적인 네트워크 ID 사용
각 Pod는 서수 인덱스에 기반한 안정적인 호스트명을 갖는다. 각 Pod에서 `hostname` 명령을 실행하려면 [`kubectl exec`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands/#exec)을 사용한다.

```bash
$ for i in 0 1; do kubectl exec "web-$i" -- sh -c 'hostname'; done

web-0
web-1
```

`kubectl run`을 사용하여 `dnsutils` 패키지에서 `nslookup` 명령을 제공하는 컨테이너를 실행한다. Pod의 hostname에 `nslookup`을 사용하여 클러스터 내 DNS 주소를 검사할 수 있다.

```bash
$ kubectl run -i --tty --image busybox:1.28 dns-test --restart=Never --rm
```

을 실행하면 새 쉘이 시작된다. 새 쉘에서 실행한다.

```bash
$ # Run this in the dns-test container shell
nslookup web-0.nginx
```

다음과 유사한 결과를 출력한다.

```
Server:    10.0.0.10
Address 1: 10.0.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-0.nginx
Address 1: 10.244.1.6

nslookup web-1.nginx
Server:    10.0.0.10
Address 1: 10.0.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-1.nginx
Address 1: 10.244.2.6
```

(그리고 이제 컨테이너 쉘을 종료한다: `exit`).

헤드리스 서비스의 CNAME은 SRV 레코드(Running이고 Ready인 각 Pod에 대해 하나씩)를 가리킨다. SRV 레코드는 Pod의 IP 주소가 포함된 A 레코드 항목을 가리킨다.

한 터미널에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pod -w -l app=nginx
```

두 번째 터미널에서 `kubectl delete`를 사용하여 StatefulSet의 모든 Pod를 삭제한다.

```bash
$ kubectl delete pod -l app=nginx

pod "web-0" deleted
pod "web-1" deleted
```

StatefulSet이 다시 시작되고 두 Pod가 모두 Running과 Ready됨으로 전환될 때까지 기다린다.

```bash
$ kubectl get pod -w -l app=nginx

NAME      READY     STATUS              RESTARTS   AGE
web-0     0/1       ContainerCreating   0          0s
NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          2s
web-1     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-1     0/1       ContainerCreating   0         0s
web-1     1/1       Running   0         34s
```

`kubectl exec`와 `kubectl run`을 사용하여 Pod의 hostname과 클러스터 내 DNS 항목을 확인한다. 먼저, Pod의 hostname을 확인한다.

```bash
$ for i in 0 1; do kubectl exec web-$i -- sh -c 'hostname'; done

web-0
web-1
```

그리고,다음을 실행한다.

```bash
$ kubectl run -i --tty --image busybox:1.28 dns-test --restart=Never --rm
```

을 실행하면 새 쉘이 시작된다.

새 쉘에서 실행한다.

```bash
$ # Run this in the dns-test container shell
nslookup web-0.nginx
```

다음과 유사한 결과를 출력한다.

```
Server:    10.0.0.10
Address 1: 10.0.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-0.nginx
Address 1: 10.244.1.7

nslookup web-1.nginx
Server:    10.0.0.10
Address 1: 10.0.0.10 kube-dns.kube-system.svc.cluster.local

Name:      web-1.nginx
Address 1: 10.244.2.8
```

(그리고 이제 컨테이너 쉘을 종료한다: `exit`).

Pod의 서수, hostname, SRV 레코드 및 A 레코드 이름은 변경되지 않았지만, Pod와 연결된 IP 주소는 변경되었을 수 있다. 이 튜토리얼에 사용된 클러스터에서는 변경되었다. 그렇기 때문에 StatefuleSet의 Pod에 IP 주소로 연결하도록 다른 어플리케이션을 구성하지 않는 것이 중요하다.

StatefuleSet의 활성 멤버를 찾아서 연결해야 하는 경우, headless 서비스(`nginx.default.svc.cluster.local`)의 CNAME을 쿼리해야 한다. CNAME과 연결된 SRV 레코드에는 StatefuleSet에서 Running이고 Ready인 Pod만 포함된다.

어플리케이션이 이미 활성 상태와 준비 상태를 테스트하는 연결 로직을 구현한 경우, 안정적이므로 파드(`web-0.nginx.default.svc.cluster.local`, `web-1.nginx.default.svc.cluster.local`)의 SRV 레코드를 사용할 수 있으며, 어플리케이션은 Running과 Ready 상태로 전환될 때 Pod의 주소를 검색할 수 있다.

### 안정적인 스토리지에 쓰기
`web-0`와 `web-1`에 대한 PersistentVolumeClaims을 가져온다.

```bash
$ kubectl get pvc -l app=nginx
```

다음과 유사한 결과를 출력한다.

```
NAME        STATUS    VOLUME                                     CAPACITY   ACCESSMODES   AGE
www-web-0   Bound     pvc-15c268c7-b507-11e6-932f-42010a800002   1Gi        RWO           48s
www-web-1   Bound     pvc-15c79307-b507-11e6-932f-42010a800002   1Gi        RWO           48s
```

StatefulSet 컨트롤러는 PersistentVolumes 두 개에 바인딩된 PersistentVolumeClaims 두 개를 생성했다.

이 튜토리얼에서 사용된 클러스터는 PersistentVolumes을 동적으로 프로비저닝하도록 구성되었으므로 PersistentVolumes이 자동으로 생성되고 바인딩되었다.

NGINX 웹서버는 기본적으로 `/usr/share/nginx/html/index.html`으로 인덱스 파일을 제공한다. StatefulSet `spec`의 `volumeMount` 필드는 `/usr/share/nginx/html` 디렉터리가 PersistentVolumes에 의해 백업되도록 보장한다.

Pod의 hostname을 `index.html` 파일에 쓰고 NGINX 웹서버가 해당 hostbane을 제공하는지 확인한다.

```bash
$ for i in 0 1; do kubectl exec "web-$i" -- sh -c 'echo "$(hostname)" > /usr/share/nginx/html/index.html'; done

$ for i in 0 1; do kubectl exec -i -t "web-$i" -- curl http://localhost/; done

web-0
web-1
```

> **Note**: 위의 curl 명령에 대해 **403 Forbidden** 응답이 대신 표시되는 경우, ([hostPath 볼륨을 사용할 때의 버그](https://github.com/kubernetes/kubernetes/issues/2630)로 인해) `volumeMounts`를 실행하여 마운트된 디렉터리의 권한을 수정해야 한다.
> 
> ```bash
> $ for i in 0 1; do kubectl exec web-$i -- chmod 755 /usr/share/nginx/html; done
> ```
> 
> 를 실행한 후 위의 curl 명령을 다시 시도한다.

한 터미널에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pod -w -l app=nginx
```

두 번째 터미널에서 StatefulSet의 모든 Pod를 삭제한다.

```bash
$ kubectl delete pod -l app=nginx

pod "web-0" deleted
pod "web-1" deleted
```

첫 번째 터미널에서 `kubectl get` 명령의 출력을 검사하고 모든 Pod가 Running과 Ready로 전환될 때까지 기다린다.

```bash
$ kubectl get pod -w -l app=nginx

NAME      READY     STATUS              RESTARTS   AGE
web-0     0/1       ContainerCreating   0          0s
NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          2s
web-1     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-1     0/1       ContainerCreating   0         0s
web-1     1/1       Running   0         34s
```

웹 서버가 해당 hostname을 계속 제공하는지 확인한다.

```bash
$ for i in 0 1; do kubectl exec -i -t "web-$i" -- curl http://localhost/; done

web-0
web-1
```

`web-0`와 `web-1`의 스케줄이 변경되었지만, PersistentVolumeClaims과 연결된 PersistentVolumes이 `volumeMount`에 다시 마운트되기 때문에 해당 hostname을 계속 서비스한다. `web-0`와 `web-1`이 어떤 node에 예약되었는지에 관계없이 해당 PersistentVolumes은 적절한 마운트 지점에 마운트된다.

## 스테이트풀셋 확장
StatefulSet를 확장한다는 것은 복제본의 수를 늘리거나 줄이는 것을 의미한다. 이는 `replicas` 필드를 업데이트하여 수행한다. StatefulSet를 스케일링하려면 [`kubectl scale`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands/#scale) 또는 [`kubectl patch`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands/#patch)를 사용할 수 있다.

### 스케일업
한 터미널 창에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pods -w -l app=nginx
```

다른 터미널 창에서 `kubectl scale`을 사용하여 복제본 수를 5로 확장한다.

```bash
$ kubectl scale sts web --replicas=5

statefulset.apps/web scaled
```

첫 번째 터미널에서 `kubectl get` 명령의 출력을 검사하고, 세 개의 추가 Pod가 Running과 Ready으로 전환될 때까지 기다린다.

```bash
$ kubectl get pods -w -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          2h
web-1     1/1       Running   0          2h
NAME      READY     STATUS    RESTARTS   AGE
web-2     0/1       Pending   0          0s
web-2     0/1       Pending   0         0s
web-2     0/1       ContainerCreating   0         0s
web-2     1/1       Running   0         19s
web-3     0/1       Pending   0         0s
web-3     0/1       Pending   0         0s
web-3     0/1       ContainerCreating   0         0s
web-3     1/1       Running   0         18s
web-4     0/1       Pending   0         0s
web-4     0/1       Pending   0         0s
web-4     0/1       ContainerCreating   0         0s
web-4     1/1       Running   0         19s
```

StatefulSet 컨트롤러는 복제본의 수를 확장했다. StatefulSet 생성 시와 마찬가지로, StatefulSet 컨트롤러는 서수 인덱스에 따라 각 Pod를 순차적으로 생성했으며, 각 Pod의 이전 Pod가 Running과 Ready 상태가 될 때까지 기다린 후 후속 Pod를 실행했다.

### 스케일다운
한 터미널 창에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pods -w -l app=nginx
```

다른 터미널 창에서 `kubectl patch`을 사용하여 복제본 수를 3으로 되돌린다.

```bash
$ kubectl patch sts web -p '{"spec":{"replicas":3}}'

statefulset.apps/web patched
```

`web-4`와 `web-3`가 Terminating으로 전환될 때까지 기다린다.

```bash
$ kubectl get pods -w -l app=nginx

NAME      READY     STATUS              RESTARTS   AGE
web-0     1/1       Running             0          3h
web-1     1/1       Running             0          3h
web-2     1/1       Running             0          55s
web-3     1/1       Running             0          36s
web-4     0/1       ContainerCreating   0          18s
NAME      READY     STATUS    RESTARTS   AGE
web-4     1/1       Running   0          19s
web-4     1/1       Terminating   0         24s
web-4     1/1       Terminating   0         24s
web-3     1/1       Terminating   0         42s
web-3     1/1       Terminating   0         42s
```

### 순차적 파드 종료
컨트롤러는 서수 인덱스와 관련하여 역순으로 한 번에 하나의 Pod를 삭제하고, 다음 Pod를 삭제하기 전에 각 Pod가 완전히 종료될 때까지 기다렸다.

StatefulSet의 PersistentVolumeClaims을 가져온다.

```bash
$ kubectl get pvc -l app=nginx

NAME        STATUS    VOLUME                                     CAPACITY   ACCESSMODES   AGE
www-web-0   Bound     pvc-15c268c7-b507-11e6-932f-42010a800002   1Gi        RWO           13h
www-web-1   Bound     pvc-15c79307-b507-11e6-932f-42010a800002   1Gi        RWO           13h
www-web-2   Bound     pvc-e1125b27-b508-11e6-932f-42010a800002   1Gi        RWO           13h
www-web-3   Bound     pvc-e1176df6-b508-11e6-932f-42010a800002   1Gi        RWO           13h
www-web-4   Bound     pvc-e11bb5f8-b508-11e6-932f-42010a800002   1Gi        RWO           13h
```

여전히 PersistentVolumeClaims 5개와 PersistentVolumes 5개가 있다. Pod의 안정적인 스토리지를 탐색할 때, StatefulSet의 Pod가 삭제될 때 StatefulSet의 Pod에 마운트된 PersistentVolumes은 삭제되지 않는 것을 확인했다. 이는 StetefulSet의 스케일링으로 인해 Pod가 삭제된 경우에도 마찬가지이다.

## StatefulSet 업데이트
Kubernetes 1.7 이상에서 StatefulSet 컨트롤러는 자동 업데이트를 지원한다. 사용되는 전략은 StatefulSet API 객체의 `spec.updateStrategy`전략 필드에 의해 결정된다. 이 기능은 StatefulSet에서 컨테이너 이미지, 리소스 요청 및/또는 제한, 레이블 및 Pod의 어노테이션을 업그레이드하는 데 사용할 수 있다. 유효한 업데이트 전략은 `RollingUpdate`와 `OnDelete` 두 종류가 있다.

`RollingUpdate` 업데이트 전략은 StatefulSet의 기본값이다.

### 롤링 업데이트(Rolling Update)
`RollingUpdate` 업데이트 전략은 StatefulSet의 모든 Pod를 StatefulSet의 보증(gurantees)을 준수하면서 역순으로 업데이트한다.

`RollingUpdate` 업데이트 전략을 적용하기 위해 `web` StatefulSet을 패치한다.

```bash
$ kubectl patch statefulset web -p '{"spec":{"updateStrategy":{"type":"RollingUpdate"}}}'

statefulset.apps/web patched
```

한 터미널 창에서 `web` StatefulSet을 패치하여 컨테이너 이미지를 다시 변경한다.

```bash
$ kubectl patch statefulset web --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/image", "value":"gcr.io/google_containers/nginx-slim:0.8"}]'

statefulset.apps/web patched
```

다른 터미널에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pod -l app=nginx -w
```

다음과 유사한 결과를 출력한다.

```
NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          7m
web-1     1/1       Running   0          7m
web-2     1/1       Running   0          8m
web-2     1/1       Terminating   0         8m
web-2     1/1       Terminating   0         8m
web-2     0/1       Terminating   0         8m
web-2     0/1       Terminating   0         8m
web-2     0/1       Terminating   0         8m
web-2     0/1       Terminating   0         8m
web-2     0/1       Pending   0         0s
web-2     0/1       Pending   0         0s
web-2     0/1       ContainerCreating   0         0s
web-2     1/1       Running   0         19s
web-1     1/1       Terminating   0         8m
web-1     0/1       Terminating   0         8m
web-1     0/1       Terminating   0         8m
web-1     0/1       Terminating   0         8m
web-1     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-1     0/1       ContainerCreating   0         0s
web-1     1/1       Running   0         6s
web-0     1/1       Terminating   0         7m
web-0     1/1       Terminating   0         7m
web-0     0/1       Terminating   0         7m
web-0     0/1       Terminating   0         7m
web-0     0/1       Terminating   0         7m
web-0     0/1       Terminating   0         7m
web-0     0/1       Pending   0         0s
web-0     0/1       Pending   0         0s
web-0     0/1       ContainerCreating   0         0s
web-0     1/1       Running   0         10s
```

StatefulSet의 Pod는 역순으로 업데이트된다. StetefulSet 컨트롤러는 각 파드를 종료하고, 다음 파드를 업데이트하기 전에 실행 및 준비 상태로 전환될 때까지 기다린다. StetefulSet 컨트롤러는 다음 Pod의 서수 후속 Pod가 Running과 Ready상태가 될 때까지 다음 Pod 업데이트를 진행하지 않지만, 업데이트 중에 실패한 Pod는 현재 버전으로 복원한다는 점에 유의한다.

이미 업데이트를 받은 Pod는 업데이트된 버전으로 복원되고, 아직 업데이트를 받지 않은 파드는 이전 버전으로 복원된다. 이러한 방식으로, 컨트롤러는 간헐적인 장애가 발생하더라도 어플리케이션을 계속 건강하게 유지하고 업데이트를 일관되게 유지하려고 시도한다.

컨테이너 이미지를 볼 수 있도록 Pod를 가져온다.

```bash
$ for p in 0 1 2; do kubectl get pod "web-$p" --template '{{range $i, $c := .spec.containers}}{{$c.image}}{{end}}'; echo; done

registry.k8s.io/nginx-slim:0.8
registry.k8s.io/nginx-slim:0.8
registry.k8s.io/nginx-slim:0.8
```

이제 StatefulSet의 모든 Pod가 이전 컨테이너 이미지를 실행하고 있다.

> **Note**: 또한, StatefulSet에 대한 롤링 업데이트의 상태를 보기 위해 `kubectl rollout status sts/<name>`을 사용할 수도 있다.

#### 업데이트 준비(staging)
`RollingUpdate` 업데이트 전략의 `partion` 파라미터를 사용하여 StatefulSet에 대한 업데이트를 스테이징할 수 있다. 스테이지드 업데이트는 StatefulSet의 모든 Pod를 현재 버전으로 유지하면서 StatefulSet의 `.spec.template`에 대한 변형을 허용한다.

`web` StatefulSet을 패치하여 `update` 필드에 파티션을 추가한다.

```bash
$ kubectl patch statefulset web -p '{"spec":{"updateStrategy":{"type":"RollingUpdate","rollingUpdate":{"partition":3}}}}'

statefulset.apps/web patched
```

StatefulSet을 다시 패치하여 컨테이너의 이미지를 변경한다.

```bash
$ kubectl patch statefulset web --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/image", "value":"registry.k8s.io/nginx-slim:0.7"}]'

statefulset.apps/web patched
```

StatefulSet에서 Pod를 삭제한다.

```bash
$ kubectl delete pod web-2

pod "web-2" deleted
```

Pod가 Running and Ready 상태가 되도록 기다린다.

```bash
$ kubectl get pod -l app=nginx -w

NAME      READY     STATUS              RESTARTS   AGE
web-0     1/1       Running             0          4m
web-1     1/1       Running             0          4m
web-2     0/1       ContainerCreating   0          11s
web-2     1/1       Running   0         18s
```

Pod의 컨테이너 이미지를 가져온다.

```bash
$ kubectl get pod web-2 --template '{{range $i, $c := .spec.containers}}{{$c.image}}{{end}}'

registry.k8s.io/nginx-slim:0.8
```

업데이트 전략이 `RollingUpdate`라 하더라도 StatefulSet은 Pod를 원래 컨테이너로 복원했다는 것을 알 수 있다. 이는 Pod의 서수가 `updateStrategy`가 지정한 `partition`보다 작기 때문이다.

#### 카나리아 출시(Rolling out Canary)
위에서 지정한 파티션을 줄임으로써 카나리아를 롤아웃하여 수정 사항을 테스트할 수 있다.

파티션을 줄이도록 StatefulSet을 패치한다.

```bash
$ kubectl patch statefulset web -p '{"spec":{"updateStrategy":{"type":"RollingUpdate","rollingUpdate":{"partition":2}}}}'

statefulset.apps/web patched
```

`web-2`가 Ruung and Ready 상태가 되도록 기다린다.

```bash
$ kubectl get pod -l app=nginx -w

NAME      READY     STATUS              RESTARTS   AGE
web-0     1/1       Running             0          4m
web-1     1/1       Running             0          4m
web-2     0/1       ContainerCreating   0          11s
web-2     1/1       Running   0         18s
```

Pod의 컨테이너를 가져온다.

```bash
$ kubectl get pod web-2 --template '{{range $i, $c := .spec.containers}}{{$c.image}}{{end}}'

registry.k8s.io/nginx-slim:0.7
```

파티션을 변경했을 때, Pod의 서수가 파티션보다 크거나 같기 때문에 StatefulSet 컨트롤러가 자동으로 `web-2` Pod를 업데이트했다.

`web-1` Pod를 삭제한다.

```bash
$ kubectl delete pod web-1

pod "web-1" deleted
```

`web-1` Pod가 Ruung and Ready 상태가 되도록 기다린다.

```bash
$ kubectl get pod -l app=nginx -w
```

다음과 유사한 결과를 출력한다.

```
NAME      READY     STATUS        RESTARTS   AGE
web-0     1/1       Running       0          6m
web-1     0/1       Terminating   0          6m
web-2     1/1       Running       0          2m
web-1     0/1       Terminating   0         6m
web-1     0/1       Terminating   0         6m
web-1     0/1       Terminating   0         6m
web-1     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-1     0/1       ContainerCreating   0         0s
web-1     1/1       Running   0         18s
```

`web-1` Pod의 컨테이너 이미지를 가져온다.

```bash
$ kubectl get pod web-1 --template '{{range $i, $c := .spec.containers}}{{$c.image}}{{end}}'

registry.k8s.io/nginx-slim:0.8
```

Pod의 서수가 파티션보다 작기 때문에 `web-1`이 원래 구성으로 복원되었다. 파티션을 지정하면, StatefulSet의 `.spec.template`이 업데이트될 때 서수가 파티션보다 크거나 같은 모든 Pod가 업데이트된다. 파티션보다 작은 서수를 가진 파드가 삭제되거나 다른 방식으로 종료되면, 원래 구성으로 복원된다.

#### 단계적 롤아웃
카나리아를 롤아웃하는 방법과 유사한 방식으로 파티션 롤링 업데이트를 사용하여 단계적 롤아웃(예: 선형, 기하학적 또는 지수 롤아웃)을 수행할 수 있습니다. 단계적 롤아웃을 수행하려면 컨트롤러가 업데이트를 일시 중지할 서수로 파티션을 설정합니다.

파티션은 현재 2로 설정되어 있습니다. 파티션을 0으로 설정합니다:

```bash
$ kubectl patch statefulset web -p '{"spec":{"updateStrategy":{"type":"RollingUpdate","rollingUpdate":{"partition":0}}}}'

statefulset.apps/web patched
```

StatefulSet의 모든 Pod가 Running and Ready 상태가 될 때까지 기다린다.

```bash
$ kubectl get pod -l app=nginx -w
```

다음과 유사한 결과를 출력한다.

```
NAME      READY     STATUS              RESTARTS   AGE
web-0     1/1       Running             0          3m
web-1     0/1       ContainerCreating   0          11s
web-2     1/1       Running             0          2m
web-1     1/1       Running   0         18s
web-0     1/1       Terminating   0         3m
web-0     1/1       Terminating   0         3m
web-0     0/1       Terminating   0         3m
web-0     0/1       Terminating   0         3m
web-0     0/1       Terminating   0         3m
web-0     0/1       Terminating   0         3m
web-0     0/1       Pending   0         0s
web-0     0/1       Pending   0         0s
web-0     0/1       ContainerCreating   0         0s
web-0     1/1       Running   0         3s
```

StatefulSet의 Pod에 대한 컨테이너 이미지 세부 정보를 가져온다.

```bash
$ for p in 0 1 2; do kubectl get pod "web-$p" --template '{{range $i, $c := .spec.containers}}{{$c.image}}{{end}}'; echo; done

registry.k8s.io/nginx-slim:0.7
registry.k8s.io/nginx-slim:0.7
registry.k8s.io/nginx-slim:0.7
```

`partition`을 `0`으로 이동하면 StatefulSet이 업데이트 프로세스를 계속할 수 있다.

### On Delete
`OnDelete` 업데이트 전략은 레거시(1.6 및 이전) 동작을 구현하며, 이 업데이트 전략을 선택하면 StatefullSet 컨트롤러는 StatefulSet의 `.spec.template` 필드를 수정할 때 Pod를 자동으로 업데이트하지 않는다. 이 전략은 `.spec.template.updateStrategy.type`을 `OnDelete`로 설정하여 선택할 수 있다.

## StatefulSet 삭제
StatefulSet은 Non-Cascading과 Cascading 삭제를 모두 지원한다. Non-Cascading Delete에서는 StatefulSet이 삭제될 때 StatefulSet의 Pod는 삭제되지 않는다. Cascading Delete에서는 StatefulSet와 해당 Pod 모두 삭제된다.

### Non-Cascading 삭제
하나의 터미널 창에서 StatefulSet의 Pod를 확인한다.

```bash
kubectl get pods -w -l app=nginx
```

StatefulSet을 삭제하려면 [`kubectl delete`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands/#delete)를 사용한다. 명령에 `--cascade=orphan` 파라미터를 제공해야 한다. 이 파라미터는 Kubernetes가 StatefulSet만 삭제하고 그 어떤 Pod도 삭제하지 않도록 지시한다.

```bash
$ kubectl delete statefulset web --cascade=orphan

statefulset.apps "web" deleted
```

`Pod`를 가져와서 상태를 확인한다.

```bash
$ kubectl get pods -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          6m
web-1     1/1       Running   0          7m
web-2     1/1       Running   0          5m
```

`web`이 삭제되었지만 모든 Pod는 여전히 Running and Ready 상태이다. `web-0`를 삭제한다.

```bash
$ kubectl delete pod web-0

pod "web-0" deleted
```

StatefulSet의 Pod를 가져온다.

```bash
$ kubectl get pods -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-1     1/1       Running   0          10m
web-2     1/1       Running   0          7m
```

`web` StatefulSet이 삭제되었으므로, `web-0`는 다시 시작되지 않았다.

한 터미널에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pods -w -l app=nginx
```

두 번째 터미널에서 StatefulSet을 다시 생성한다. `nginx` 서비스를 삭제하지 않았다면(삭제해서는 안 된다), 서비스가 이미 존재한다는 오류 메시지가 표시된다.

```bash
$ kubectl apply -f web.yaml

statefulset.apps/web created
service/nginx unchanged
```

오류를 무시하세요. 이 오류는 해당 서비스가 이미 존재함에도 불구하고 *nginx* headless 서비스를 생성하려고 시도했음을 나타낼 뿐이다.

첫 번째 터미널에서 실행 중인 `kubectl get` 명령의 출력을 살펴본다.

```bash
$ kubectl get pods -w -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-1     1/1       Running   0          16m
web-2     1/1       Running   0          2m
NAME      READY     STATUS    RESTARTS   AGE
web-0     0/1       Pending   0          0s
web-0     0/1       Pending   0         0s
web-0     0/1       ContainerCreating   0         0s
web-0     1/1       Running   0         18s
web-2     1/1       Terminating   0         3m
web-2     0/1       Terminating   0         3m
web-2     0/1       Terminating   0         3m
web-2     0/1       Terminating   0         3m
```

`web` StatefulSet이 다시 생성되면, 먼저 `web-0`을 재시작했다. `web-1`은 이미 Running and Reay 상태가 되었기 때문에, `web-0`가  Running으로 전환되면, 이 Pod를 채택했다. 2와 동일한 복제본으로 StatefulSet을 다시 생성했기 때문에, `web-0`가 다시 생성되고 `web-1`이 이미 Running and Ready로 확인되면서, `web-2`가 종료되었다.

Pod의 웹서버가 제공하는 `index.html` 파일의 내용을 다시 한 번 살펴보자.

```bash
$ for i in 0 1; do kubectl exec -i -t "web-$i" -- curl http://localhost/; done

web-0
web-1
```

StatefulSet과 `web-0` Pod를 모두 삭제했더라도, StatefulSet은 원래 `index.html` 파일에 입력된 호스트명을 계속 제공한다. StatefulSet은 Pod와 연결된 PersistentVolumes을 삭제하지 않기 때문이다. StatefulSet을 다시 생성하고 `web-0`을 재시작하면 원래 PersistentVolumes이 다시 마운트된다.

### Cascading 삭제
한 터미널 창에서 StatefulSet의 Pod를 확인한다.

```bash
$ kubectl get pods -w -l app=nginx
```

다른 터미널에서 StatefulSet을 다시 삭제합니다. 이번에는 `--cascade=orphan` 매개변수를 생략한다.

```bash
$ kubectl delete statefulset web
```

첫 번째 터미널에서 `kubectl get` 명령을 실행하고 그 출력을 검사하여 모든 Pod가 Terminating으로 전환될 때까지 기다린다.

```bash
$ kubectl get pods -w -l app=nginx

NAME      READY     STATUS    RESTARTS   AGE
web-0     1/1       Running   0          11m
web-1     1/1       Running   0          27m
NAME      READY     STATUS        RESTARTS   AGE
web-0     1/1       Terminating   0          12m
web-1     1/1       Terminating   0         29m
web-0     0/1       Terminating   0         12m
web-0     0/1       Terminating   0         12m
web-0     0/1       Terminating   0         12m
web-1     0/1       Terminating   0         29m
web-1     0/1       Terminating   0         29m
web-1     0/1       Terminating   0         29m
```

[스케일다운](#스케일다운)에서 보았듯이, Pod는 서수 인덱스의 역순에 따라 한 번에 하나씩 종료된다. Pod를 종료하기 전에, StatefulSet 컨트롤러는 Pod의 후속 Pod가 완전히 종료될 때까지 기다린다.

> **Note**: 캐스케이딩(cascading) 삭제는 StatefulSet과 해당 Pod를 함께 제거하지만, SatefulSet과 연결된 headless 서비스는 삭제되지 않는다. `nginx` 서비스를 수동으로 삭제해야 한다.

```bash
$ kubectl delete service nginx

service "nginx" deleted
```

StatefulSet과 headless 서비스를 다시 한번 재생성한다.

```bash
$ kubectl apply -f web.yaml

service/nginx created
statefulset.apps/web created
```

StatefulSet의 모든 Pod가 Running and Ready 상태로 전환되면, `index.html` 파일의 내용을 검색한다.

```bash
$ for i in 0 1; do kubectl exec -i -t "web-$i" -- curl http://localhost/; done

web-0
web-1
```

StatefulSet의 모든 Pod를 완전히 삭제했지만, Pod는 PersistentVolumes이 마운트된 상태로 다시 생성되며, `web-0`과 `web-1`은 hostname을 계속 서비스한다.

마지막으로, `nginx` 서비스 삭제한다.

```bash
$ kubectl delete service nginx
service "nginx" deleted
```

그리고, `web`  StatefulSet도 함께

```bash
kubectl delete statefulset web
statefulset "web" deleted
```

## Pod 관리 정책
일부 분산 시스템의 경우, StatefulSet 순서 보장은 불필요하거나 바람직하지 않다. 이러한 시스템에는 고유성과 아덴티티만 필요하다. 이 문제를 해결하기 위해, Kubernetes 1.7에서는 StatefulSet API 객체에 `.spec.podManagementPolicy`를 도입했다.

### OrderedReady Pod 관리
`OrderedReady` Pod 관리는 StatefulSet의 기본값이다. 이것은 StatefulSet 컨트롤러가 위에서 설명한 순서 보장을 준수하도록 지시한다.

### 병렬 Pod 관리
병렬 Pod 관리는 StatefulSet 컨트롤러가 모든 Pod를 병렬로 시작하거나 종료하고, 다른 Pod를 시작하거나 종료하기 전에 Pod가 Running and Ready 상태가 되거나 완전히 종료될 때까지 기다리지 않도록 지시한다. 이 옵션은 스케일링 작업의 동작에만 영향을 미친다. 업데이트는 영향을 받지 않는다.

```yml
# application/web/web-parallel.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  ports:
  - port: 80
    name: web
  clusterIP: None
  selector:
    app: nginx
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: web
spec:
  serviceName: "nginx"
  podManagementPolicy: "Parallel"
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: registry.k8s.io/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: web
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

위의 예를 다운로드하고 `web-parallel.yaml`이라는 파일에 저장한다.

이 매니페스트는 `web` StatefulSet `.spec.podManagementPolicy`가 `Parallel`로 설정되어 있다는 점을 제외하면 위에서 다운로드한 것과 동일하다.

한 터미널에서 StatefulSet의 Pod를 감시한다.

```bash
$ kubectl get pod -l app=nginx -w
```

다른 터미널에서 매니페스트에 StatefulSet과 서비스를 생성한다.

```bash
$ kubectl apply -f web-parallel.yaml
service/nginx created
statefulset.apps/web created
```

첫 번째 터미널에서 `kubectl get` 명령을 실행하고 그 출력을 살펴본다.

```bash
$ kubectl get pod -l app=nginx -w
NAME      READY     STATUS    RESTARTS   AGE
web-0     0/1       Pending   0          0s
web-0     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-1     0/1       Pending   0         0s
web-0     0/1       ContainerCreating   0         0s
web-1     0/1       ContainerCreating   0         0s
web-0     1/1       Running   0         10s
web-1     1/1       Running   0         10s
```

StatefulSet 컨트롤러는 `web-0`과 `web-1`을 동시에 시작했다.

두 번째 터미널을 열어두고 다른 터미널 창에서 StatefulSet의 크기를 조정한다.

```bash
$ kubectl scale statefulset/web --replicas=4
statefulset.apps/web scaled
```

`kubectl get` 명령이 실행중인 터미널의 출력을 살펴본다.

```bash
web-3     0/1       Pending   0         0s
web-3     0/1       Pending   0         0s
web-3     0/1       Pending   0         7s
web-3     0/1       ContainerCreating   0         7s
web-2     1/1       Running   0         10s
web-3     1/1       Running   0         26s
```

StatefulSet은 새로운 Pod 두 개를 출시했으며, 첫 번째 Pod가 Running and Ready 상태가 될 때까지 기다리지 않고 두 번째 Pod를 출시했다.

## 정리
정리 작업의 일부로 `kubectl` 명령을 실행할 수 있도록 두 개의 터미널이 열려 있어야 한다.

```bash
$ kubectl delete sts web
# sts is an abbreviation for statefulset
```

`kubectl get` 명령으로 해당 Pod가 삭제되는 것을 볼 수 있다.

```bash
$ kubectl get pod -l app=nginx -w
web-3     1/1       Terminating   0         9m
web-2     1/1       Terminating   0         9m
web-3     1/1       Terminating   0         9m
web-2     1/1       Terminating   0         9m
web-1     1/1       Terminating   0         44m
web-0     1/1       Terminating   0         44m
web-0     0/1       Terminating   0         44m
web-3     0/1       Terminating   0         9m
web-2     0/1       Terminating   0         9m
web-1     0/1       Terminating   0         44m
web-0     0/1       Terminating   0         44m
web-2     0/1       Terminating   0         9m
web-2     0/1       Terminating   0         9m
web-2     0/1       Terminating   0         9m
web-1     0/1       Terminating   0         44m
web-1     0/1       Terminating   0         44m
web-1     0/1       Terminating   0         44m
web-0     0/1       Terminating   0         44m
web-0     0/1       Terminating   0         44m
web-0     0/1       Terminating   0         44m
web-3     0/1       Terminating   0         9m
web-3     0/1       Terminating   0         9m
web-3     0/1       Terminating   0         9m
```

삭제하는 동안 스테이트풀셋은 모든 Pod를 동시에 제거하며, 해당 Pod를 삭제하기 전에 Pod의 서수 후계자가 종료될 때까지 기다리지 않는다.

`kubectl get` 명령이 실행 중인 터미널을 닫고 `nginx` 서비스를 삭제한다.

```bash
$ kubectl delete svc nginx
```

이 튜토리얼에서 사용된 PersistentVolumes의 퍼시스턴트 스토리지 미디어를 삭제한다.

```bash
$ kubectl get pvc
NAME        STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
www-web-0   Bound    pvc-2bf00408-d366-4a12-bad0-1869c65d0bee   1Gi        RWO            standard       25m
www-web-1   Bound    pvc-ba3bfe9c-413e-4b95-a2c0-3ea8a54dbab4   1Gi        RWO            standard       24m
www-web-2   Bound    pvc-cba6cfa6-3a47-486b-a138-db5930207eaf   1Gi        RWO            standard       15m
www-web-3   Bound    pvc-0c04d7f0-787a-4977-8da3-d9d3a6d8d752   1Gi        RWO            standard       15m
www-web-4   Bound    pvc-b2c73489-e70b-4a4e-9ec1-9eab439aa43e   1Gi        RWO            standard       14m
```

```bash
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM               STORAGECLASS   REASON   AGE
pvc-0c04d7f0-787a-4977-8da3-d9d3a6d8d752   1Gi        RWO            Delete           Bound    default/www-web-3   standard                15m
pvc-2bf00408-d366-4a12-bad0-1869c65d0bee   1Gi        RWO            Delete           Bound    default/www-web-0   standard                25m
pvc-b2c73489-e70b-4a4e-9ec1-9eab439aa43e   1Gi        RWO            Delete           Bound    default/www-web-4   standard                14m
pvc-ba3bfe9c-413e-4b95-a2c0-3ea8a54dbab4   1Gi        RWO            Delete           Bound    default/www-web-1   standard                24m
pvc-cba6cfa6-3a47-486b-a138-db5930207eaf   1Gi        RWO            Delete           Bound    default/www-web-2   standard                15m
```

```bash
$ kubectl delete pvc www-web-0 www-web-1 www-web-2 www-web-3 www-web-4
persistentvolumeclaim "www-web-0" deleted
persistentvolumeclaim "www-web-1" deleted
persistentvolumeclaim "www-web-2" deleted
persistentvolumeclaim "www-web-3" deleted
persistentvolumeclaim "www-web-4" deleted
```

```bash
$ kubectl get pvc
No resources found in default namespace.
```

> **Note**: 또한 이 튜토리얼에서 사용된 PersistentVolumes의 퍼시스턴트 스토리지 미디어를 삭제해야 한다. 환경, 스토리지 구성과 프로비저닝 방법에 따라 필요한 단계를 수행하여 모든 스토리지가 회수되도록 하십시오.
