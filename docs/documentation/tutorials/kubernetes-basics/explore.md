`kubectl get`, `kubectl describe`, `kubectl logs` 및 `kubectl exec`을 사용하여 Kubernetes 어플리케이션 문제를 해결하는 방법을 알아보자.

## Pod와 Node 보기

#### 목표
- Kubernetes Pod에 대해 알아본다.
- Kubernetes Node에 대해 알아본다.
- 배포된 애플리케이션의 문제 해결하기

### Kubernetes Pod
모듈 [2](./deploy-app.md/#kubectl을-사용하여-deployment-생성하기)에서 Deployment를 생성할 때, Kubernetes는 어플리케이션 인스턴스를 호스팅할 **Pod**를 생성했다. Pod는 하나 이상의 어플리케이션 컨테이너(예: Docker)의 그룹과 해당 컨테이너에 대한 일부 공유 리소스를 나타내는 Kubernetes 추상화이다. 이러한 리소스에는 다음을 포함한다.

- 볼륨으로 공유 스토리지
- 고유 클러스터 IP 주소을 갖는 네트워킹
- 컨테이너 이미지 버전 또는 사용할 특정 포트 등 각 컨테이너를 실행하는 방법에 대한 정보

Pod는 어플리케이션별 "논리적 호스트"를 모델링하며, 비교적 긴밀하게 결합된 다양한 어플리케이션 컨테이너를 포함할 수 있다. 예를 들어, Pod에는 Node.js 앱이 포함된 컨테이너와 Node.js 웹서버에서 게시할 데이터를 공급하는 다른 컨테이너가 모두 포함될 수 있다. Pod의 컨테이너는 IP 주소와 포트 공간을 공유하며, 항상 공동으로 위치와 스케줄링되고, 동일한 node에서 공유 컨텍스트에서 실행된다.

pod는 Kubernetes 플랫폼의 원자 단위이다. Kubernetes에서 Deployment를 생성하면, 해당 Deployment는 컨테이너를 직접 생성하는 것과는 반대로 그 안에 컨테이너가 포함된 Pod를 생성한다. 각 Pod는 예약된 node에 연결되며, 종료(재시작 정책에 따라) 또는 삭제될 때까지 해당 node에 남아 있는다. node에 장애가 발생하면 클러스터의 다른 사용 가능한 node에서 동일한 Pod가 스케줄링된다.

### Pod 개요

![](../../images/tutorials/module_03_pods.svg)

### Node
Pod는 항상 **Node**에서 실행된다. node는 Kubernetes의 워커 머신이며 클러스터에 따라 가상 머신이거나 물리적 머신일 수 있다. 각 node는 컨트롤 플레인에 의해 관리된다. node에는 여러 개의 Pod가 있을 수 있으며, Kubernetes 컨트롤 플레인은 클러스터의 node 전반에서 Pod 스케줄링을 자동으로 처리한다. 컨트롤 플레인의 자동 스케줄링은 각 node에서 사용 가능한 리소스를 고려한다.

모든 Kubernetes node는 다음 것을 최소한 하나 이상 실행한다.

- Kubelet: Kubernetes 컨트롤 플레인과 노드 간의 통신을 담당하는 프로세스로, 머신에서 실행 중인 Pod와 컨테이너를 관리한다.
- 레지스트리에서 컨테이너 이미지를 가져오고, 컨테이너의 압축을 풀고, 어플리케이션을 실행하는 컨테이너 런타임(예: Docker).

### Node 개요

![](../../images/tutorials/module_03_nodes.svg)

### kubectl으로 문제 해결하기

모듈 [2](./deploy-app.md/#kubectl을-사용하여-deployment-생성하기)에서는 kubectl 명령 인터페이스를 사용했다. 모듈 3에서도 계속해서 이 인터페이스를 사용하여 배포된 어플리케이션과 해당 환경에 대한 정보를 얻는다. 가장 일반적인 작업은 다음과 같은 kubectl 서브커맨드를 사용하여 수행할 수 있다.

- `kubectl get` - 리소스를 나열한다.
- `kubectl describe` - 리소스에 대한 상세 정보를 표시한다.
- `kubectl logs` - Pod의 컨테이너에서 로그를 출력한다.
- `kubectl exec` - Pod의 컨테이너에서 명령을 실행한다.

이러한 명령을 사용하여 어플리케이션이 언제 배포되었는지, 현재 상태는 어떤지, 실행 중인 위치는 어디인지, 구성은 어떤지 확인할 수 있다.

이제 클러스터 구성 요소와 명령에 대해 자세히 알았다. 어플리케이션을 살펴보자.

#### 애플리케이션 구성 확인
이전 시나리오에서 배포한 애플리케이션이 실행 중인지 확인해 보겠다. `kubectl get` 명령을 사용하여 기존 Pod를 찾는다.

```bash
$ kubectl get pods
```

실행 중인 Pod가 없으면 몇 초간 기다렸다가 Pod를 다시 나열한다. 실행 중인 Pod가 하나 확인되면 계속 진행할 수 있다.

다음으로, 해당 Pod 안에 어떤 컨테이너가 있는지, 그리고 해당 컨테이너를 빌드하는 데 어떤 이미지가 사용되는지 확인하기 위해 `kubectl describe pods` 명령을 실행한다.

```bash
$ kubectl 설명 파드
```

여기에서 IP 주소, 사용된 포트와 Pod의 생애주기와 관련된 이벤트 목록같은 Pod의 컨테이너에 대한 세부 정보를 볼 수 있다. 

d`escribe` 서브커맨드의 출력은 광범위하며 아직 설명하지 않은 몇 가지 개념을 다루고 있지만, 이 튜토리얼이 끝날 때 쯤이면 익숙해질 것이니 걱정하지 마세요.

> **Note**: `describe` 서브커맨드는 Node, Pod, Deployment를 포함한 대부분의 Kubernetes 기본 요소에 대한 자세한 정보를 얻는 데 사용할 수 있다. `describe` 출력은 스크립팅이 아닌 사람이 읽을 수 있도록 설계되어 있다.

#### 터미널에서 앱 표시
Pod는 격리된 사설 네트워크에서 실행되고 있으므로 디버깅하고 상호 작용할 수 있도록 Pod에 대한 프록시 액세스가 필요하다는 것을 기억하자. 이를 위해, 두 번째 터미널에서 프록시를 실행하기 위해 `kubectl proxy` 명령을 사용하겠다. 새 터미널 창을 열고, 그 터미널에서 다음을 실행한다.

```bash
$ kubectl 프록시
```

이제 다시 Pod 이름을 가져와서 프록시를 통해 해당 Pod에 직접 쿼리한다. Pod 이름을 가져와서 `POD_NAME` 환경 변수에 저장한다.

```bash
$ export POD_NAME="$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')"
$ echo Name of Pod: $POD_NAME
```

어플리케이션의 출력을 확인하려면 `curl` 요청을 실행한다.

```bash
$ curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME:8080/proxy/
```

URL은 Pod의 API에 대한 경로이다.

#### 컨테이너 로그 보기
어플리케이션이 일반적으로 표준 출력으로 보내는 모든 것은 POd 내의 컨테이너에 대한 로그가 된다. 이러한 로그는 `kubectl logs` 명령를 사용하여 검색할 수 있다.

```bash
$ kubectl logs "$POD_NAME"
```

> **Note**: 위에서 Pod 내부에는 컨테이너가 하나만 있기 때문에 컨테이너 이름을 지정할 필요가 없다.

#### 컨테이너에서 명령 실행
Pod가 실행되면 컨테이너에서 직접 명령을 실행할 수 있다. 이를 위해, 실행 서브커맨드를 사용하고 Pod의 이름을 파라미터로 사용한다. 환경 변수를 나열해보자.

```bash
$ kubectl exec "$POD_NAME" -- env
```

다시 말하지만, Pod에는 컨테이너가 하나만 있기 때문에 컨테이너 이름 자체는 생략할 수 있다는 점을 다시 한번 언급한다.

다음으로 Pod의 컨테이너에서 bash 세션을 시작해 보자.

```bash
kubectl exec -ti $POD_NAME -- bash
```

이제 컨테이너에 오픈 콘솔이 생겼고, 여기서 NodeJS 어플리케이션을 실행한다. 앱의 소스 코드는 `server.js` 파일에 있다.

```bash
cat server.js
```

`curl` 명령을 실행하여 어플리케이션이 실행 중인지 확인할 수 있다.

```bash
$ curl http://localhost:8080
```

> **Note**: 여기서는 NodeJS Pod 내에서 명령을 실행했기 때문에 `localhost`를 사용했다. `localhost:8080`에 연결할 수 없는 경우, `kubectl exec` 명령을 실행하고 Pod 내에서 명령을 실행하고 있는지 확인한다.

컨테이너 연결을 닫으려면 `exit`를 입력한다.

준비가 완료되면 [서비스를 사용하여 앱 노출](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/)로 이동한다.
