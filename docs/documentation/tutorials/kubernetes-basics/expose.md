Kubernetes의 서비스에 대해 알아본다. 레이블과 셀렉터가 서비스와 어떻게 연관되는지 이해한다. Kubernetes 클러스터 외부에 어플리케이션을 노출한다.

## 서비스를 사용하여 앱 노출

### 목표
- Kubernetes의 서비스에 대해 알아보기
- 레이블과 셀렉터가 서비스와의 연관에 대하여 이해하기
- 서비스를 사용하여 Kubernetes 클러스터 외부에 어플리케이션 노출하기

### Kubernetes 서비스 개요
Kubernetes [Pod](../../concepts/workloads/pods/index.md)는 필멸이다. Pod에는 [수명주기](../../concepts/workloads/pods/pod-lifecycle.md)가 있다. 워커 node가 죽으면, 해당 node에서 실행 중인 Pod도 손실된다. 그러면 [ReplicaSet](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/)은 어플리케이션을 계속 실행하기 위해 새 Pod를 생성하여 클러스터를 원하는 상태로 동적으로 다시 구동할 수 있다. 다른 예로, 3개의 복제본(replicas)가 있는 이미지 처리 백엔드를 생각해 보자. 이러한 복제본은 교환이 가능하며, 프론트엔드 시스템은 백엔드 복제본이나 심지어 Pod가 손실되었다가 다시 생성되더라도 신경 쓰지 않아야 한다. 즉, Kubernetes 클러스터의 각 Pod는 동일한 node에 있는 Pod라도 고유한 IP 주소를 가지므로 어플리케이션이 계속 작동할 수 있도록 Pod간의 변경 사항을 자동으로 조정하는 방법이 있어야 한다.

Kubernetes의 서비스는 Pod의 논리적 집합과 Pod를 액세스하는 정책을 정의하는 추상화이다. 서비스는 종속 Pod 간의 느슨한 결합을 가능하게 한다. 서비스는 모든 Kubernetes 객체 매니페스트와 마찬가지로 YAML 또는 JSON을 사용하여 정의된다. 서비스가 타겟팅하는 Pod 집합은 일반적으로 *레이블 셀렉터(selector)*에 의해 결정된다(spec에 `selector`를 포함하지 않고 서비스를 원하는 이유는 아래를 참조한다).

각 Pod에는 고유한 IP 주소가 있지만, 이러한 IP는 서비스 없이는 클러스터 외부에 노출되지 않는다. 서비스는 어플리케이션이 트래픽을 수신할 수 있게 해준다. 서비스는 서비스 `spec`에 `type`을 지정하여 다양한 방식으로 노출될 수 있다.

- *ClusterIP(기본값)* - 클러스터의 내부 IP에 서비스를 노출한다. 이 타입은 클러스터 내에서만 서비스에 연결할 수 있도록 한다.
- *NodePort* - NAT를 사용하여 클러스터에서 선택한 각 Node의 동일한 포트에 서비스를 노출한다. `<NodeIP>:<NodePort>`를 사용하여 클러스터 외부에서 서비스를 액세스할 수 있도록 한다. ClusterIP의 상위 집합이다.
- *LoadBalancer* - 현재 클라우드에 외부 로드 밸런서를 생성하고(지원되는 경우) 고정된 외부 IP를 서비스에 할당한다. NodePort의 상위 집합이다.
- *ExternalName* - 해당 값과 함께 `CNAME` 레코드를 반환하여 서비스를 `externalName` 필드의 내용(예: `foo.bar.example.com`)에 매핑 한다. 어떤 종류의 프록시도 설정되지 않는다. 이 타입을 사용하려면 v1.7 이상의 `kube-dns` 또는 CoreDNS 버전 0.0.8 이상이 필요하다.

다양한 타입의 서비스에 대한 자세한 내용은 [Using Source IP](https://kubernetes.io/docs/tutorials/services/source-ip/) 튜토리얼에서 확인할 수 있다. 또한 [Connectiong Application with Servies]()를 참조하세요.

또한 spec에 `selector`를 정의하지 않는 서비스 사용 사례도 있다는 점에 유의하자. `selector` 없이 생성된 서비스는 해당 엔드포인트 객체도 생성하지 않는다. 따라서 사용자가 수동으로 서비스를 특정 엔드포인트에 매핑할 수 있다. selector가 없는 또 다른 경우에는 `type:ExternalName`을 엄격하게 사용한다.

### 서비스와 레이블
서비스는 Pod 집합으로 트래픽을 라우팅한다. 서비스는 어플리케이션에 영향을 주지 않고 Kubernetes에서 Pod가 죽고 복제될 수 있도록 하는 추상화이다. 종속 Pod(예: 어플리케이션의 프론트엔드와 백엔드 구성 요소)간의 검색과 라우팅은 Kubernetes 서비스에서 처리한다.

서비스는 Kubenetes의 객체에 대한 논리적 연산을 허용하는 그룹화 기본 요소인 [레이블과 셀렉터](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels)를 사용하여 Pod 집합과 일치시킨다. 레이블은 객체에 연결된 키/값 쌍으로, 여러 가지 방식으로 사용할 수 있다.

- 개발, 테스트 및 프로덕션을 위한 객체 지정
- 버전 태그 포함
- 태그를 사용하여 객체 분류

![](../../images/tutorials/module_04_labels.svg)

레이블은 생성 시 또는 나중에 객체에 첨부할 수 있다. 언제든지 수정할 수 있다. 이제 서비스를 사용하여 어플리케이션을 노출하고 몇 가지 레이블을 적용해 보겠다.

### 새 서비스 생성
어플리케이션이 실행 중인지 확인해 보자. `kubectl get` 명령을 사용하여 기존 Pod를 찾아보겠다.

```bash
$ kubectl get pods
```

실행 중인 파드가 없다면 이전 튜토리얼의 객체가 정리되었다는 뜻이다. 이 경우, 돌아가서 [kubectl을 사용하여 디플로이먼트 생성](../kubernetes-basics/deploy-app.md/#앱-배포)
 튜토리얼에서 deployment를 다시 생성한다. 몇 초간 기다렸다가 Pod를 다시 리스팅한다. 하나의 Pod가 실행되는 것을 확인하면 계속 진행한다.

다음으로, 클러스터의 현재 서비스를 나열해 보겠다.

```bash
$ kubectl get services
```

minikube가 클러스터를 시작할 때 기본적으로 생성되는 kubernetes라는 서비스가 있다. 새 서비스를 생성하고 외부 트래픽에 노출하기 위해 NodePort를 파라미터로 하는 expose 명령을 사용한다.

```bash
$ kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

`get services` 하위 명령을 다시 실행해 보자.

```bash
$ kubectl get services
```

이제 `kubernetes-bootcamp`라는 서비스가 실행 중이다. 여기서는 서비스가 고유한 클러스터-IP, 내부 포트 및 외부-IP(Node의 IP)를 수신했음을 알 수 있다.

외부에서 어떤 포트가 열렸는지(`type: NodePort` Service)를 확인하기 위해 `describe service` 서브커맨드를 실행한다.

```bash
$ kubectl describe services/kubernetes-bootcamp
```

Node 포트 값이 할당된 NODE_PORT라는 환경 변수를 생성한다.

```bash
$ export NODE_PORT="$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')"
$ echo "NODE_PORT=$NODE_PORT"
```

이제 Node의 IP 주소와 외부에 노출된 포트로 `curl`을 사용하여 앱이 클러스터 외부에 노출되었는지 테스트할 수 있다.

```bash
$ curl http://"$(minikube ip):$NODE_PORT"
```

그리고 서버로부터 응답을 받았으면 서비스가 노출된 것이다.

### 2단계: 레이블 사용하기
Deployment는 자동으로 Pod에 대한 레이블을 생성했다. `describe deployment` 서브커맨드를 사용하면 해당 레이블의 이름(*키*)을 확인할 수 있다.

```bash
$ kubectl describe deployment
```

이 레이블을 사용하여 Pod 목록을 쿼리해 보자. 매개변수로 `-l`을 사용하고 그 뒤에 레이블 값을 사용하여 `kubectl get pods` 명령을 사용하겠다.

```bash
$ kubectl get pods -l app=kubernetes-bootcamp
```

동일한 방법으로 기존 서비스를 나열할 수 있다.

```bash
$ kubectl get services -l app=kubernetes-bootcamp
```

Pod의 이름을 가져와서 `POD_NAME` 환경 변수에 저장한다.

```bash
export POD_NAME="$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')"
echo "Name of the Pod: $POD_NAME"
```

새 레이블을 적용하려면 레이블 서브커맨드 뒤에 객체 타입, 객체 이름, 새 레이블을 사용한다.

```bash
$ kubectl label pods "$POD_NAME" 버전=v1
```

이렇게 하면 Pod에 새 레이블이 적용되며(어플리케이션 버전을 Pod에 고정했다), `describe pod` 명령으로 확인할 수 있다.

```bash
$ kubectl describe pods "$POD_NAME"
```

이제 레이블이 파드에 첨부된 것을 볼 수 있다. 이제 새 레이블을 사용하여 Pod 목록을 쿼리할 수 있다.

```bash
$ kubectl get pods -l version=v1
```

그리고 파드를 볼 수 있다.

### 서비스 삭제
서비스를 삭제하려면 `delete service` 하위 명령을 사용할 수 있다. 여기에서도 레이블을 사용할 수 있다.

```bash
$ kubectl delete service -l app=kubernetes-bootcamp
```

서비스가 사라졌는지 확인한다.

```bash
$ kubectl get services
```

이렇게 하면 서비스가 제거되었음을 확인할 수 있다. 경로가 더 이상 노출되지 않았는지 확인하려면 이전에 노출된 IP와 포트를 `curl`하면 된다.

```bash
$ curl http://"$(minikube ip):$NODE_PORT"
```

이렇게 하면 클러스터 외부에서 어플리케이션에 더 이상 연결할 수 없다는 것이 증명된다. Pod 내부에서 `curl`을 사용하여 앱이 여전히 실행 중인지 확인할 수 있다.

```bash
$ kubectl exec -ti $POD_NAME -- curl http://localhost:8080
```

여기에서 어플리케이션이 실행 중인 것을 볼 수 있다. 이는 Deployment가 어플리케이션을 관리하고 있기 때문이다. 어플리케이션을 종료하려면 Deployment도 삭제해야 한다.

준비가 완료되면 [앱의-여러-인스턴스-실행](./scale.md/#앱의-여러-인스턴스-실행)으로 이동한다.
