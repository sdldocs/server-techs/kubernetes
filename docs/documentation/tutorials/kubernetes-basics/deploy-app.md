어플리케이션 Deployment에 대해 알아본다. `kubectl`을 사용하여 Kubernetes에 첫 번째 앱을 배포한다.

## kubectl을 사용하여 Deployment 생성하기

#### 목표
- 어플리케이션 배포에 대해 알아본다.
- kubectl을 사용하여 Kubernetes에 첫 번째 앱을 배포한다.

#### Kubernetes Deployment
[실행 중인 Kubernetes 클러스터](./create-cluster.md/#미니큐브를-사용하여-클러스터-생성)가 있으면, 그 위에 컨테이너화된 어플리케이션을 배포할 수 있다. 그렇게 하려면 Kubernetes **Deployment**를 생성하여야 한다. Deployement는 어플리케이션의 인스턴스를 생성하고 업데이트하는 방법을 Kubernetes에 지시한다. Deployment를 생성하면, Kubernetes 컨트롤 플레인은 해당 Deployment에 포함된 어플리케이션 인스턴스가 클러스터의 개별 Node에서 실행되도록 스케줄링한다.

어플리케이션 인스턴스가 생성되면, Kubernetes Deployment 컨트롤러는 해당 인스턴스를 지속적으로 모니터링한다. 인스턴스를 호스팅하는 node가 다운되거나 삭제되면, Deployment 컨트롤러는 해당 인스턴스를 클러스터의 다른 node에 있는 인스턴스로 대체한다. **이는 머신 장애 또는 유지보수 문제를 해결하기 위한 자가 복구 메커니즘을 제공하는 것이다**.

오케스트레이션 이전 환경에서는 설치 스크립트를 사용하여 어플리케이션을 시작하는 경우가 많았지만, 머신 장애로부터 복구할 수 없었다. 어플리케이션 인스턴스를 생성하고 Node 전반에서 실행을 유지함으로써, Kubernetes Deployment는 어플리케이션 관리에 대한 근본적으로 다른 접근 방식을 제공한다.

### Kubernetes에 첫 번째 앱 배포
![](../../images/tutorials/module_02_first_app.svg)

Kubernetes 명령 인터페이스인 **kubectl**을 사용하여 Deployment를 생성하고 관리할 수 있다. kubectl은 Kubernetes API를 사용하여 클러스터와 상호 작용한다. 이 모듈에서는 Kubernetes 클러스터에서 어플리케이션을 실행하는 Deployment를 생성하는 데 필요한 가장 일반적인 kubectl 명령어를 배운다.

Deployment를 생성할 때, 어플리케이션의 컨테이너 이미지와 실행할 레플리카의 수를 지정해야 한다. 나중에 Deployment를 업데이트하여 해당 정보를 변경할 수 있으며, 부트캠프의 모듈 [5](https://kubernetes.io/docs/tutorials/kubernetes-basics/scale/scale-intro/)와 [6](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/)에서는 배포를 확장하고 업데이트하는 방법에 대해 설명한다.

첫 번째 Deployment에서는 모든 요청을 에코백하기 위해 NGINX를 사용하는 Docker 컨테이너에 패키징된 hello-node 어플리케이션을 사용한다. (hello-node 어플리케이션을 생성하고 컨테이너를 사용하여 배포해 본 적이 없다면, [Hello Minikube 튜토리얼](../hello-minikube.md)의 지침에 따라 먼저 시도해 볼 수 있다.)

kubectl도 설치해야 합니다. 설치해야 하는 경우, [설치 도구](https://kubernetes.io/docs/tasks/tools/#kubectl)를 방문한다.

이제 Deployment가 무엇인지 알았으니 첫 번째 앱을 배포해 보겠다!

#### kubectl 기초
`kubectl action resource`은 kubectl 명령의 일반적인 형식이다.

이는 지정된 `resource`(예: node 또는 deployment)에 대해 지정된 `action`(예: create, describe 또는 delete)을 수행한다. 가능한 파라미터에 대한 추가 정보를 얻으려면 서브커맨드 뒤에 `--help`를 사용할 수 있다(예: `kubectl get nodes --help`).

**`kubectl version`** 명령을 실행하여 클러스터와 통신하도록 kubectl이 구성되었는지 확인한다.

kubectl이 설치되어 있고 클라이언트 버전과 서버 버전을 모두 볼 수 있는지 확인한다.

클러스터의 node를 보려면 **`kubectl get nodes`** 명령을 실행한다.

사용 가능한 node가 표시된다. 나중에 Kubernetes는 가용한 node 리소스에 따라 어플리케이션을 배포할 위치를 선택한다.

#### 앱 배포
`kubectl create deployment` 명령으로 첫 번째 앱을 Kubernetes에 배포해 보겠다. 배포 이름과 앱 이미지 위치를 제공해야 한다(외부 Docker Hub에서 호스팅되는 이미지의 경우 전체 리포지토리 URL을 포함).

```bash
$ kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
```

훌륭! 방금 deployment를 생성하여 첫 번째 어플리케이션을 배포했다. 이렇게 하면 몇 가지 작업이 수행된다.

- 어플리케이션 인스턴스를 실행할 수 있는 적절한 node를 검색했다(사용 가능한 node는 1개뿐).
- 해당 node에서 어플리케이션 실행을 예약했다.
- 필요할 때 새 node에서 인스턴스를 다시 예약하도록 클러스터를 구성했다.

Deployment를 나열하려면 `kubectl get deployments` 명령을 사용한다.

```bㅁ노
$ kubectl get deployments
```

앱의 단일 인스턴스를 실행하는 deployment가 1개 있음을 알 수 있다. 인스턴스는 node의 컨테이너 내에서 실행되고 있다.

#### app 보기
Kubernetes 내부에서 실행 중인 Pod는 격리된 프라이빗 네트워크에서 실행된다. 기본적으로 동일한 Kubernetes 클러스터 내의 다른 Pod와 서비스에서는 볼 수 있지만 해당 네트워크 외부에서는 볼 수 없다. 우리가 `kubectl`을 사용할 때, 우리는 API 엔드포인트를 통해 어플리케이션과 통신한다.

나중에 [모듈 4](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/)에서 Kubernetes 클러스터 외부에 어플리케이션을 노출하는 방법에 대한 다른 옵션을 다루겠다.

`kubectl proxy` 명령은 클러스터 전체의 프라이빗 네트워크로 통신을 전달할 프록시를 생성할 수 있다. 프록시는 control-C를 눌러 종료할 수 있으며 실행 중에는 출력을 표시하지 않는다.

**프록시를 실행하려면 두 번째 터미널 창을 열어야 한다**.

```bash
$ kubectl proxy
```

이제 호스트(터미널)와 Kubernetes 클러스터 사이에 연결이 이루어졌다. 프록시는 터미널에서 API를 직접 액세스할 수 있게 해준다.

프록시 엔드포인트를 통해 호스팅되는 모든 API를 볼 수 있다. 예를 들어, `curl` 명령을 사용하여 API를 통해 직접 버전을 쿼리할 수 있다.

```bash
$ curl http://localhost:8001/version
```

> **Note**: 포트 8001을 액세스할 수 없는 경우, 위에서 시작한 `kubectl proxy`가 두 번째 터미널에서 실행 중인지 확인한다.

API 서버는 프록시를 통해서도 액세스할 수 있는 각 Pod에 대한 엔드포인트를 Pod 이름을 기반으로 자동으로 생성한다.

먼저 파드 이름을 가져와서 환경 변수 POD_NAME에 저장해야 한다.

```bash
$ export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
$ echo Name of the Pod: $POD_NAME
```

프록시된 API를 실행하여 Pod를 액세스할 수 있다.

```bash
$ curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/
```

프록시를 사용하지 않고 새 Deployment를 액세스하려면 [모듈 4](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose)에서 설명할 서비스가 필요하다.

준비가 되면 [Pod와 노드 보기](https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-intro/)로 이동한다.
