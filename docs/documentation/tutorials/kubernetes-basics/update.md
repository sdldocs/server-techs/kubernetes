kubectl을 사용하여 롤링 업데이트를 수행한다.

## 롤링 업데이트 수행

#### 목표
- kubectl을 사용하여 롤링 업데이트를 수행한다.

#### 어플리케이션 업데이트
사용자는 어플리케이션을 항상 사용할 수 있기를 기대하며 개발자는 하루에 여러 번 어플리케이션의 새 버전을 배포해야 한다. Kubernetes에서는 롤링 업데이트를 통해 이를 수행한다. 롤링 업데이트는 Pod 인스턴스를 새로운 인스턴스로 점진적으로 업데이트하여 다운타임 없이 Deployment 업데이트를 수행할 수 있게 해준다. 새 Pod는 사용 가능한 리소스가 있는 Node에 스케줄링된다.

이전 모듈에서 우리는 여러 인스턴스를 실행하도록 어플리케이션을 스케일링했다. 이는 어플리케이션 가용성에 영향을 주지 않고 업데이트를 수행하기 위한 요구사항이다. 기본적으로 업데이트 중에 사용할 수 없는 최대 Pod 수와 생성할 수 있는 최대 새 Pod 수는 하나이다. 두 옵션 모두 숫자 또는 (Pod의) 백분율로 구성할 수 있다. Kubernetes에서 업데이트는 버전이 지정되며, 모든 Deployment 업데이트는 이전(안정된) 버전으로 되돌릴 수 있다.

### 롤링 업데이트 개요

![](../../images/tutorials/module_06_rollingupdates1.svg)

어플리케이션 스케일링과 유사하게, Deployment가 공개적으로 노출된 경우, 서비스는 업데이트 중에 사용 가능한 Pod로만 트래픽을 로드 밸런싱한다. 사용 가능한 Pod는 어플리케이션 사용자가 사용할 수 있는 인스턴스이다.

롤링 업데이트는 다음과 같은 작업을 허용한다.

- 컨테이너 이미지 업데이트를 통해 한 환경에서 다른 환경으로 어플리케이션 승격
- 이전 버전으로 롤백
- 다운타임 없이 어플리케이션의 지속적 통합과 지속적 배포

다음 대화형 튜토리얼에서는 어플리케이션을 새 버전으로 업데이트하고 롤백도 수행해 보겠다.

#### 앱 버전 업데이트
Deployment를 나열하려면, `get deployments` 서브커맨드를 실행한다. 

```bash
$ kubectl get deployments
```

실행 중인 Pod를 나열하려면, `get pods` 서브커맨드를 실행한다.

```bash
$ kubectl get pods
```

앱의 현재 이미지 버전을 보려면, `describe pods` 서브커맨드를 실행하고 이미지 필드를 찾는다.

```bash
$ kubectl describe pods
```

어플리케이션의 이미지를 버전 2로 업데이트하려면, `set image` 서브커맨드와 그 뒤에 Deployment 이름과 새 이미지 버전을 사용한다.

```bash
$ kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2
```

이 명령은 Deployment에 앱의 다른 이미지를 사용하도록 알리고 롤링 업데이트를 시작했다. 새 Pod의 상태를 확인하고, `get pods` 서브커맨드로 종료되는 이전 Pod를 확인한다.

```bash
$ kubectl get pods
```

#### 2 단계: 업데이트 확인
먼저 앱이 실행 중인지 확인한다. 노출된 IP 주소와 포트를 찾으려면 `describe service` 명령을 실행한다.

```bash
$ kubectl describe services/kubernetes-bootcamp
```

할당된 Node 포트의 값이 있는 `NODE_PORT`라는 환경 변수를 생성한다,

```bash
$ export NODE_PORT="$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')"
$ echo "NODE_PORT=$NODE_PORT"
```

다음으로, 노출된 IP와 포트로 `curl`을 실행한다.

```bash
$ curl http://"$(minikube ip):$NODE_PORT"
```

`curl` 명령을 실행할 때마다 다른 Pod를 실행하게 된다. 이제 모든 Pod가 최신 버전(v2)을 실행하고 있음을 알 수 있다.

`rollout status` 서브커맨드를 실행하여 업데이트를 확인할 수도 있다.

```bash
$ kubectl 롤아웃 상태 배포/쿠버네티스-부트캠프
```

앱의 현재 이미지 버전을 보려면, `describe pods` 서브커맨드를 실행한다.

```bash
$ kubectl 설명 파드
```

출력의 `Image` 필드에서 최신 이미지 버전(v2)을 실행하고 있는지 확인한다.

#### 업데이트 롤백
다른 업데이트를 수행하고 v10으로 태그가 지정된 이미지를 배포해 보자.

```bash
$ kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=gcr.io/google-samples/kubernetes-bootcamp:v10
```

Deployment의 상태를 확인하려면 `get deployments`를 사용한다.

```bash
$ kubectl get deployments
```

출력에 원하는 수의 사용 가능한 Pod가 나열되지 않는 것을 확인한다. 모든 Pod를 나열하려면 `get pods` 서브커맨드를 실행한다.

```bash
$ kubectl get pods
```

일부 Pod의 상태가 `ImagePullBackOff`인 것을 확인할 수 있다.

문제에 대한 더 많은 인사이트를 얻으려면, `describe pods` 서브커맨드를 실행한다.

```bash
$ kubectl 설명 파드
```

영향을 받는 Pod에 대한 출력의 `Event` 섹션에서, 리포지토리에 v10 이미지 버전이 존재하지 않는 것을 확인한다.

deployment를 마지막으로 작동하는 버전으로 롤백하려면 `rollout undo` 서브커맨드를 사용한다.

```bash
$ kubectl rollout undo deployments/kubernetes-bootcamp
```

`rollout undo` 명령은 deployment를 이전에 알려진 상태(이미지의 v2)로 되돌린다. 업데이트는 버전이 지정되며 이전에 알려진 배포 상태로 되돌릴 수 있다.

Pod를 다시 나열하려면 `get pods` 서브커맨드를 사용한다.

```bash
$ kubectl get pods
```

4개의 Pod가 실행 중이다. 이 Pod에 배포된 이미지를 확인하려면 `describe pods` 서브커맨드를 사용한다.

```bash
$ kubectl describe pods
```

배포는 다시 한 번 안정적인 버전의 앱(v2)을 사용하고 있다. 롤백이 성공했다.

로컬 클러스터를 정리하는 것을 잊지 마세요.

```bash
$ kubectl delete deployments/kubernetes-bootcamp services/kubernetes-bootcamp
```
