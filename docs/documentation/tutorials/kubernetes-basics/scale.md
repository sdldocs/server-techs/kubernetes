## 앱의 여러 인스턴스 실행
kubectl을 사용하여 기존 앱을 수동으로 확장한다.

#### 목표
- kubectl을 사용하여 앱 스케일링.

#### 애플리케이션 스케일링
이전에는 [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)를 생성한 다음 [서비스](https://kubernetes.io/docs/concepts/services-networking/service/)를 통해 공개적으로 노출했다. Deployment는 어플리케이션을 실행하기 위해 하나의 Pod만 생성했다. 트래픽이 증가하면 사용자 수요를 따라잡기 위해 어플리케이션을 스케일링해야 한다.

이전 섹션을 진행하지 않았다면, [minikube를 사용하여 클러스터 생성](./create-cluster.md/#미니큐브를-사용하여-클러스터-생성)하기부터 시작하세요.

스케일링은 Deployment의 복제본 수를 변경하여 수행한다.

### 스케일링 개요

![](../../images/tutorials/module_05_scaling2.svg)

Deployment를 스케일아웃하면 사용 가능한 리소스가 있는 Node에 새 Pod가 생성되고 스케줄링된다. 스케일링은 Pod의 수를 원하는 새로운 상태로 증가시킨다. Kubernetes는 Pod의 [자동 스케일링](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)도 지원하지만, 이 튜토리얼의 범위를 벗어난다. 0으로 스케일링하는 것도 가능하며, 이 경우 지정된 Deployment의 모든 Pod가 종료된다.

어플리케이션의 여러 인스턴스를 실행하려면 모든 인스턴스에 트래픽을 분산할 수 있는 방법이 필요하다. 서비스에는 노출된 Deployment의 모든 Pod에 네트워크 트래픽을 분산하는 통합 로드밸런서가 있다. 서비스는 엔드포인트를 사용하여 실행 중인 Pod를 지속적으로 모니터링하여 트래픽이 사용 가능한 Pod로만 전송되도록 한다.

스케일링은 Deployment의 복제본 수를 변경하여 수행한다.

어플리케이션의 여러 인스턴스가 실행 중이면 다운타임 없이 롤링 업데이트를 수행할 수 있다. 이에 대해서는 튜토리얼의 다음 섹션에서 다루겠다. 이제 터미널로 이동하여 어플리케이션을 확장해 보자.

#### Deployment 스케일링
Deployment를 나열하려면 다음과 같이 `get deployments` 서브커맨드를 사용한다.

```bash
$ kubectl get deployments
```

출력은 다음과 유사하다.

```
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   1/1     1            1           11m
```

Pod가 1개 있어야 한다. 그렇지 않은 경우, 명령을 다시 실행한다. 다음과 같이 표시된다.

- *NAME*은 클러스터에 있는 Deployment의 이름을 나열한다.
- *READY*는 현재/희망하는 복제본의 비율을 표시한다.
- *UP-TO-DATE*는 원하는 상태에 도달하기 위해 업데이트된 복제본의 수를 표시한다.
- *AVAILABLE*은 사용자가 사용할 수 있는 어플리케이션 복제본의 수를 표시한다.
- *AGE*는 어플리케이션이 실행된 시간을 표시한다.

Deployment가 생성한 ReplicaSet을 확인하려면, `kubectl get rs`를 실행한다.

ReplicaSet의 이름은 항상 `[DEPLOYMENT-NAME]-[RANDOM-STRING]`으로 형식이 지정된다. randon string은 무작위로 생성되며 *pod-template-hash*를 시드로 사용한다.

다음 두 열이 이 출력에서 중요하다.

- *DESIRED*는 Deployment를 만들 때 정의하는 어플리케이션의 원하는 복제본 수를 표시한다. 이것이 원하는 상태이다.
- *CURRENT*는 현재 실행 중인 복제본 수를 표시한다.

다음으로, Deployement를 4 복제본로 확장해 보겠다. `kubectl scale` 명령과 그 뒤에 Deployment 타입, 이름, 원하는 인스턴스 수를 입력한다.

```bash
$ kubectl scale deployments/kubernetes-bootcamp --replicas=4
```

Deployement를 다시 한 번 나열하려면 `get deployments`를 사용한다.

```bash
$ kubectl get deployments
```

변경 사항이 적용되었으며, 4개의 어플리케이션 인스턴스를 사용할 수 있다. 다음으로, Pod 수가 변경되었는지 확인해보자.

```bash
$ kubectl get pods -o wide
```

이제 서로 다른 IP 주소를 가진 4개의 파드가 있다. 변경 사항은 Deployment 이벤트 로그에 등록되었다. 이를 확인하려면 `describe` 서브커맨드를 사용한다.

```bash
$ kubectl describe deployments/kubernetes-bootcamp
```

이 명령의 출력에서 이제 4개의 복제본이 있는 것을 확인할 수도 있다.

#### 로드 밸런싱
서비스가 트래픽을 로드 밸런싱하고 있는지 확인해 보자. 노출된 IP와 포트를 확인하기 위해 튜토리얼의 이전 부분에서 배운 것처럼 describe 서비스를 사용할 수 있다.

```bash
$ kubectl describe services/kubernetes-bootcamp
```

Node 포트 값을 갖는 `NODE_PORT`라는 환경 변수를 생성한다.

```bash
$ export NODE_PORT="$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')"

$ echo NODE_PORT=$NODE_PORT
```

다음으로, 노출된 IP 주소와 포트에 대한 `curl`을 수행한다. 명령을 여러 번 실행한다.

```bash
$ curl http://"$(minikube ip):$NODE_PORT"
```

요청할 때마다 다른 파드에 접속한다. 이것은 로드 밸런싱이 작동하고 있음을 보여주는 것이다.

#### 스케일 다운
Deployment를 2 복제본으로 축소하려면 `scale` 서브커맨드를 다시 실행한다.

```bash
$ kubectl scale deployments/kubernetes-bootcamp --replicas=2
```

Deployment를 나열하여 `get deployments` 서브커맨드로 변경 사항이 적용되었는지 확인한다.

```bash
$ kubectl get deployments
```

레플리카 수가 2로 감소했다. `get pods`를 사용하여 Pod 수를 나열한다.

```bash
$ kubectl get pods -o wide
```

이렇게 하면 파드 2개가 종료되었음을 확인할 수 있다.

준비가 되면 [롤링 업데이트 수행](./update.md/#롤링-업데이트-수행)으로 이동한다.
