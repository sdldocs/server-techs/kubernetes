이 튜토리얼은 minikube를 사용하여 Kubernetes에서 샘플 앱을 실행하는 방법을 보인다. 이 튜토리얼은 모든 요청에 대한 에코백하기 위해 NGINX를 사용하는 컨테이너 이미지를 제공한다.

## 목적
- minikube에 샘플 어플리케이션을 배포
- 앱을 실행
- 어플리케이션 로그를 확인

## 시작 전에
이 튜토리얼에서는minikube를  이미 설정했다고 가정한다. 설치 지침은 미니큐브 시작을 참조한다.

또한 kubectl을 설치해야 한다. 설치 지침은 [도구 설치](https://kubernetes.io/docs/tasks/tools/#kubectl)를 참조한다.

## minikube 클러스터 만들기

```bash
$ minikube start
```

## 대시보드 열기
Kubernetes 대시보드를 연다. 두 방법으로 이 작업을 수행할 수 있다.

##### 브라우저 실행
터미널을 **새로** 열고, 다음을 실행한다.

```bash
# Start a new terminal, and leave this running.
$ minikube dashboard
```

##### URL 복사 및 붙이넣기
minikube가 웹 브라우저를 열지 않게 하려면 `--url` 플래그와 함께 dashboard 명령을 실행한다. minikube는 사용자가 원하는 브라우저에서 열 수 있는 URL을 출력한다.

터미널을 **새로** 열고, 다음을 실행한다.

```bash
# Start a new terminal, and leave this running.
$ minikube dashboard --url
```

이제 minikube를 실행한 터미널로 다시 돌아온다.

> **Note**: 
>
> `dashboard` 명령은 dashboard 애드온을 활성화하고 기본 웹 브라우저에서 프록시를 연다. 대시보드에서 배포와 서비스와 같은 kubernetes 리소스를 생성할 수 있다.
>
> 루트 환경으로 실행 중인 경우 [Open Dashboard with URL](#open-dashboard-with-url)를 참조하세요.
>
> 기본적으로 대시보드는 내부 Kubernetes 가상 네트워크 내에서만 액세스할 수 있다. `dashboard` 명령은 임시 프록시를 생성하여 Kubernetes 가상 네트워크 외부에서 대시보드를 액세스할 수 있도록 한다.
>
> 프록시를 중지하려면 `Ctrl+C`를 실행하여 프로세스를 종료한다. 명령이 종료된 후에도 대시보드는 Kubernetes 클러스터에서 계속 실행된다. 대시보드 명령을 다시 실행하여 대시보드를 액세스할 다른 프록시를 만들 수 있다.

## Deployment 생성하기
쿠버네티스 [*Pod*](../concepts/workloads/pods/index.md/)는 관리와 네트워킹을 위해 함께 묶은 하나 이상의 컨테이너로 구성된 그룹이다. 이 튜토리얼의 Pod에는 컨테이너가 하나만 있다. Kubernetes [Deployment](../concepts/workloads/controllers/index.md)는 Pod의 상태를 확인하고 종료되면 Pod의 컨테이너를 다시 시작한다. Deployment는 Pod의 생성과 확장을 관리하기 위한 권장하는 방법이다.

<span>1.</span> Pod를 관리하는 Deployment를 생성하려면 `kubectl create` 명령을 사용한다. Pod는 제공된 Docker 이미지를 기반으로 컨테이너를 실행한다.

```bash
# Run a test container image that includes a webserver
$ kubectl create deployment hello-node --image=registry.k8s.io/e2e-test-images/agnhost:2.39 -- /agnhost netexec --http-port=8080
```

<span>2.</span> Deployment 보기

```bash
$ kubectl get deployments
```

출력은 다음과 유사할 것이다.

```
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
hello-node   1/1     1            1           1m
```

<span>3.</span> Pod 보기

```bash
$ kubectl get pods
```

출력은 다음과 유사할 것이다.

```
NAME                          READY     STATUS    RESTARTS   AGE
hello-node-5f76cf6ccf-br9b5   1/1       Running   0          1m
```

<span>4.</span> 클러스터 이벤트 보시

```bash
$ kubectl get events
```

<span>5.</span> 'kubectl` 구성 보기

```bash
$ kubectl config view
```

> **Note**: `kubectl` 명령에 대한 자세한 내용은 [kubectl overview](https://kubernetes.io/docs/reference/kubectl/)를 참조한다.

## 서비스 생성하기
기본적으로 Pod는 Kubernetes 클러스터 내의 내부 IP 주소로만 접근할 수 있다. Kubernetes 가상 네트워크 외부에서 `hello-node` 컨테이너에 접근하려면, Pod를 Kubernetes 서비스로 노출해야 한다.

<span>1.</span> `kubectl expose` 명령을 사용하여 Pod를 공용 인터넷에 노출한다.

```bash
$ kubectl expose deployment hello-node --type=LoadBalancer --port=8080
```

`--type=LoadBalancer` 플래그는 서비스를 클러스터 외부에 노출하려는 것을 나타낸다.

테스트 이미지 내부의 어플리케이션 코드는 TCP 포트 8080에서만 수신 대기한다. 다른 포트를 노출하기 위해 `kubectl expose`를 사용하면 클라이언트가 다른 포트에 연결할 수 없다.

<span>2.</span> 생성한 서비스를 확인할 수 있다.

```bash
$ kubectl get services
```

출력은 다음과 유사할 것이다.

```
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
hello-node   LoadBalancer   10.108.144.78   <pending>     8080:30369/TCP   21s
kubernetes   ClusterIP      10.96.0.1       <none>        443/TCP          23m
```

로드 밸런서를 지원하는 클라우드 공급자의 경우, 서비스를 액세스하기 위해 외부 IP 주소가 프로비저닝된다. minikube에서 로드밸런서 타입은 `minikube service` 명령을 통해 서비스를 액세스할 수 있도록 한다.

<span>3.</span> 다음 명령을 실행한다.

```bash
$ minikube service hello-node
```

그러면 앱을 제공하는 브라우저 창이 열리고 앱의 응답이 표시된다.

## 에드온(addon) 활성화
minikube 도구에는 로컬 Kubernetes 환경에서 활성화, 비활성화 및 열 수 있는 내재된 애드온 세트가 포함되어 있다.

<span>1.</span> 현재 지원되는 애드온 목록.

```bash
$ minikube addons list
```

출력은 다음과 유사할 것이다.

```
addon-manager: enabled
dashboard: enabled
default-storageclass: enabled
efk: disabled
freshpod: disabled
gvisor: disabled
helm-tiller: disabled
ingress: disabled
ingress-dns: disabled
logviewer: disabled
metrics-server: disabled
nvidia-driver-installer: disabled
nvidia-gpu-device-plugin: disabled
registry: disabled
registry-creds: disabled
storage-provisioner: enabled
storage-provisioner-gluster: disabled
```

<span>2.</span> 예를 들어 `metrics-server` 에드온을 활성화한다.

```bash
$ minikube addons enable metrics-server
```

출력은 다음과 유사할 것이다.

```
The 'metrics-server' addon is enabled
```

<span>3.</span> 해당 애드온을 설치하여 생성한 Pod와 서비스 보기

```bash
kubectl get pod,svc -n kube-system
```

출력은 다음과 유사할 것이다.

```
NAME                                        READY     STATUS    RESTARTS   AGE
pod/coredns-5644d7b6d9-mh9ll                1/1       Running   0          34m
pod/coredns-5644d7b6d9-pqd2t                1/1       Running   0          34m
pod/metrics-server-67fb648c5                1/1       Running   0          26s
pod/etcd-minikube                           1/1       Running   0          34m
pod/influxdb-grafana-b29w8                  2/2       Running   0          26s
pod/kube-addon-manager-minikube             1/1       Running   0          34m
pod/kube-apiserver-minikube                 1/1       Running   0          34m
pod/kube-controller-manager-minikube        1/1       Running   0          34m
pod/kube-proxy-rnlps                        1/1       Running   0          34m
pod/kube-scheduler-minikube                 1/1       Running   0          34m
pod/storage-provisioner                     1/1       Running   0          34m

NAME                           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
service/metrics-server         ClusterIP   10.96.241.45    <none>        80/TCP              26s
service/kube-dns               ClusterIP   10.96.0.10      <none>        53/UDP,53/TCP       34m
service/monitoring-grafana     NodePort    10.99.24.54     <none>        80:30002/TCP        26s
service/monitoring-influxdb    ClusterIP   10.111.169.94   <none>        8083/TCP,8086/TCP   26s
```

<span>4.</span> `metrics-server` 비활성화

```bash
$ minikube addons disable metrics-server
```

출력은 다음과 유사할 것이다.

```
metrics-server was successfully disabled
```

## 정리
이제 클러스터에서 생성한 리소스를 정리할 수 있다.

```bash
$ kubectl delete service hello-node
$ kubectl delete deployment hello-node
```

Minikube 클러스터 중지

```bash
$ minikube stop
```

선택 사항으로 minikube VM을 삭제한다.

```bash
# Optional
$ minikube delete
```

minikube를 다시 사용하여 Kubernetes에 대해 자세히 알아보려는 경우, 삭제할 필요 없다.

## 다음
- [deploy your first app on Kubernetes with kubectl](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/)를 위한 튜토리얼
- Learn more about [Deployment objects](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)에 대하여 자세히 알아보기
- Learn more about [Deploying applications](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/)에 대하여 자세히 알아보기
- Learn more about [Service objects](https://kubernetes.io/docs/concepts/services-networking/service/)에 대하여 자세히 알아보기
