이 튜토리얼에서는 Kubernetes와 [Docker](https://www.docker.com/)를 사용하여 간단한(*프로덕션 준비가 되지 않은*) 멀티티어 웹 어플리케이션을 빌드하고 배포하는 방법을 보인다. 이 예는 다음 구성 요소로 이루어져 있다.

- 방명록 항목을 저장하는 단일 [Redis](https://www.redis.io/) 인스턴스
- 여러 웹 프론트엔드 인스턴스

## 목표
-  Redis 리더를 시작한다.
- 두 개의 Redis 팔로워를 시작한다.
- 방명록 프론트엔드를 시작한다.
- 프론트엔드 서비스를 노출하고 확인한다.
- 정리한다.

## 시작하려면
Kubernetes 클러스터가 있어야 하며, 클러스터와 통신하도록 kubectl 명령 도구가 구성되어 있어야 한다. 컨트롤 플레인 호스트로 작동하지 않는 node가 2개 이상 있는 클러스터에서 이 튜토리얼을 실행하는 것이 좋다. 클러스터가 아직 없는 경우, [minikube](https://minikube.sigs.k8s.io/docs/tutorials/multi_node/)를 사용하여 클러스터를 생성하거나 다음 Kubernetes 플레이그라운드 중 하나를 사용할 수 있다.

- [Killercoda](https://killercoda.com/playgrounds/scenario/kubernetes)
- [Play with Kubernetes](https://labs.play-with-k8s.com/)

Kubernetes 서버는 버전 v1.14 이상이어야 한다. 버전을 확인하려면 `kubectl version`을 입력한다.

## Redis 데이터메이스 시작하기

방명록 어플리케이션은 데이터를 저장하기 위하여 Redis를 사용한다.

### Redis Deployment 생성
아래에 포함된 매니페스트 파일은 단일 복제본 Redis Pod를 실행하는 Deployment 컨트롤러를 지정한다.

```yml
# application/guestbook/redis-leader-deployment.yaml
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-leader
  labels:
    app: redis
    role: leader
    tier: backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
        role: leader
        tier: backend
    spec:
      containers:
      - name: leader
        image: "docker.io/redis:6.0.5"
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 6379
```

<span>1.</span> 매니페스트 파일을 다운로드한 디렉토리에서 터미널 창을 시작한다.

<span>2.<span> `redis-leader-deployment.yaml` 파일에서 Redis Deployment를 적용한다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/guestbook/redis-leader-deployment.yaml
```

<span>3.</span> Pod 목록을 쿼리하여 Redis Pod가 실행 중인지 확인한다.

```bash
$ kubectl get pods
```

다음과 유사한 결과를 출력한다.

```
NAME                           READY   STATUS    RESTARTS   AGE
redis-leader-fb76b4755-xjr2n   1/1     Running   0          13s
```

<span>4.</span> 다음 명령을 실행하여 Redis 리더 Pod의 로그를 확인한다.

```bash
kubectl logs -f deployment/redis-leader
```

### Redis leader 서비스 생성하기
방명록 어플리케이션은 데이터를 쓰기 위해 Redis와 통신해야 한다. Redis Pod에 트래픽을 프록시하려면 서비스를 적용해야 한다. 서비스는 Pod를 액세스하기 위한 정책을 정의한다.

```yml
# application/guestbook/redis-leader-service.yaml
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: v1
kind: Service
metadata:
  name: redis-leader
  labels:
    app: redis
    role: leader
    tier: backend
spec:
  ports:
  - port: 6379
    targetPort: 6379
  selector:
    app: redis
    role: leader
    tier: backend
```

<span>1.</span> 다음 `redis-leader-service.yaml` 파일에서 Redis 서비스를 적용한다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/guestbook/redis-leader-service.yaml
```

<span>2.</span> 서비스 목록을 쿼리하여 Redis 서비스가 실행 중인지 확인한다.

```bash
$ kubectl get service
```

다음과 유사한 결과를 출력한다.

```
NAME           TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
kubernetes     ClusterIP   10.0.0.1     <none>        443/TCP    1m
redis-leader   ClusterIP   10.103.78.24 <none>        6379/TCP   16s
```

> **Note**: 이 매니페스트 파일은 이전에 정의된 레이블과 일치하는 레이블 집합을 가진 `redis-leader`라는 이름의 서비스를 생성하여 이 서비스가 네트워크 트래픽을 Redis 파드로 라우팅한다.

### Redis 팔로워 설정
Redis leader는 단일 Pod이지만, 몇 개의 Redis 팔로워 또는 복제본을 추가하여 가용성을 높이고 트래픽 수요를 충족시킬 수 있다.

```yml
# application/guestbook/redis-follower-deployment.yaml
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-follower
  labels:
    app: redis
    role: follower
    tier: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
        role: follower
        tier: backend
    spec:
      containers:
      - name: follower
        image: gcr.io/google_samples/gb-redis-follower:v2
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 6379
```

<span>1.</span> Redis 리더는 단일 Pod이지만, 몇 개의 Redis 팔로워 또는 복제본을 추가하여 가용성을 높이고 트래픽 수요를 충족시킬 수 있다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/guestbook/redis-follower-deployment.yaml
```

<span>2.</span> Pod 목록을 쿼리하여 두 개의 Redis 팔로워 복제본이 실행 중인지 확인한다.

```bash
$ kubectl get pods
```

다음과 유사한 결과를 출력한다.

```
NAME                             READY   STATUS    RESTARTS   AGE
redis-follower-dddfbdcc9-82sfr   1/1     Running   0          37s
redis-follower-dddfbdcc9-qrt5k   1/1     Running   0          38s
redis-leader-fb76b4755-xjr2n     1/1     Running   0          11m
```

### Redis 팔로워 서비스 생성하기
방명록 어플리케이션은 데이터를 읽으려면 Redis 팔로워와 통신해야 합니다. Redis 팔로워를 검색할 수 있게 하려면 다른 [서비스](https://kubernetes.io/docs/concepts/services-networking/service/)를 설정해야 한다.

```yml
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: v1
kind: Service
metadata:
  name: redis-follower
  labels:
    app: redis
    role: follower
    tier: backend
spec:
  ports:
    # the port that this service should serve on
  - port: 6379
  selector:
    app: redis
    role: follower
    tier: backend
```

<span>1.</span> 다음 `redis-follower-service.yaml` 파일에서 Redis 서비스를 적용한다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/guestbook/redis-follower-service.yaml
```

<span>2.</span> 서비스 목록을 쿼리하여 Redis 서비스가 실행 중인지 확인한다.

```bash
$ kubectl get service
```

다음과 유사한 결과를 출력한다.

```
NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP    3d19h
redis-follower   ClusterIP   10.110.162.42   <none>        6379/TCP   9s
redis-leader     ClusterIP   10.103.78.24    <none>        6379/TCP   6m10s
```

> **Note**: 이 매니페스트 파일은 이전에 정의된 레이블과 일치하는 레이블 집합을 가진 `redis-follower`라는 이름의 서비스를 생성하여 이 서비스가 네트워크 트래픽을 Redis Pod로 라우팅하도록 한다.

## 방명록 프론트엔드 설정과 노출
이제 방명록의 Redis 저장소가 실행 중이므로 방명록 웹 서버를 시작하세요. Redis 팔로워와 마찬가지로, 프론트엔드를 Kubernetes Deployment를 사용하여 배포한다.

방명록 앱은 PHP 프론트엔드를 사용한다. 요청이 읽기인지 쓰기인지에 따라 Redis 팔로워 또는 리더 서비스와 통신하도록 구성되어 있다. 프런트엔드는 JSON 인터페이스를 노출하고 jQuery-Ajax 기반 UX를 제공한다.

### 방명록 프론트엔드 Deployment 생성허기

```yml
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 3
  selector:
    matchLabels:
        app: guestbook
        tier: frontend
  template:
    metadata:
      labels:
        app: guestbook
        tier: frontend
    spec:
      containers:
      - name: php-redis
        image: gcr.io/google_samples/gb-frontend:v5
        env:
        - name: GET_HOSTS_FROM
          value: "dns"
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 80
```

<span>1.</span> 프론트엔드 Deployment를 `frontend-deployment.yaml` 파일에서 적용한다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/guestbook/frontend-deployment.yaml
```

<span>2.</span> Pod 목록을 쿼리하여 프론트엔드 복제본 3개가 실행 중인지 확인한다.

```bash
$ kubectl get pods -l app=guestbook -l tier=frontend
```

다음과 유사한 결과를 출력한다.

```
NAME                        READY   STATUS    RESTARTS   AGE
frontend-85595f5bf9-5tqhb   1/1     Running   0          47s
frontend-85595f5bf9-qbzwm   1/1     Running   0          47s
frontend-85595f5bf9-zchwc   1/1     Running   0          47s
```

### 프론트엔드 서비스 생성하기
서비스의 기본 유형은 클러스터IP이기 때문에 적용한 Kubernetes 클러스터 내에서만 레디스 서비스를 액세스할 수 있다. 클러스터IP는 서비스가 가리키는 Pod 집합에 대해 단일 IP 주소를 제공한다. 클러스터 내에서만 이 IP 주소를 액세스할 수 있다.

게스트가 방명록에 액세스할 수 있도록 하려면, 클라이언트가 Kubernetes 클러스터 외부에서 서비스를 요청할 수 있도록 프론트엔드 서비스를 외부에서 볼 수 있도록 구성해야 한다. 그러나 Kubernetes 사용자는 클러스터IP를 사용하더라도 kubectl 포트 포워드를 사용하여 서비스를 액세스할 수 있다.

> **Note**: Google 컴퓨트 엔진 또는 Google Kubernetes 엔진과 같은 일부 클라우드 제공업체는 외부 로드 밸런서를 지원한다. 클라우드 공급자가 로드 밸런서를 지원하며 이를 사용하려면 `type:LoadBalancer`를 주석 처리에서 해제하세요.

```yml
# SOURCE: https://cloud.google.com/kubernetes-engine/docs/tutorials/guestbook
apiVersion: v1
kind: Service
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  # if your cluster supports it, uncomment the following to automatically create
  # an external load-balanced IP for the frontend service.
  # type: LoadBalancer
  #type: LoadBalancer
  ports:
    # the port that this service should serve on
  - port: 80
  selector:
    app: guestbook
    tier: frontend
```

<span>1.</sapn> `frontend-service.yaml` 파일에서 프론트엔드 서비스를 적용한다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/guestbook/frontend-service.yaml
```

<span>2.</span> 서비스 목록을 쿼리하여 프런트엔드 서비스가 실행 중인지 확인한다.

```bash
$ kubectl get services
```

다음과 유사한 결과를 출력한다.

```
NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
frontend         ClusterIP   10.97.28.230    <none>        80/TCP     19s
kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP    3d19h
redis-follower   ClusterIP   10.110.162.42   <none>        6379/TCP   5m48s
redis-leader     ClusterIP   10.103.78.24    <none>        6379/TCP   11m
```

### `kubectl port-forward`을 통해 프론트엔드 서비스 보기
<span>1.</span> 다음 명령을 실행하여 로컬 컴퓨터의 포트 8080을 서비스의 포트 80으로 포워딩한다.

```bash
$ kubectl port-forward svc/frontend 8080:80
```

다음과 유사한 결과를 출력한다.

```
NAME       TYPE           CLUSTER-IP      EXTERNAL-IP        PORT(S)        AGE
frontend   LoadBalancer   10.51.242.136   109.197.92.229     80:32372/TCP   1m
```

<span>2.</span> 외부 IP 주소를 복사하고 브라우저에서 페이지를 로드하여 방명록을 확인한다.

> **Note**: 메시지를 입력하고 `Submit`을 클릭하여 방명록 항목을 추가해 보세요. 입력한 메시지가 프런트엔드에 나타난다. 이 메시지는 이전에 만든 서비스를 통해 데이터가 Redis에 성공적으로 추가되었음을 나타낸다.

### 웹 프론트엔드 확장
서버가 Deployment 컨트롤러를 사용하는 서비스로 정의되어 있으므로 필요에 따라 확장하거나 축소할 수 있다.

<span>1.</span> 프론트엔드 Pod 수를 확장하려면 다음 명령을 실행한다.

```bash
$ kubectl scale deployment frontend --replicas=5
```

<span>2.</span> Pod 목록을 쿼리하여 실행 중인 프론트엔드 파드 수를 확인한다.

```bash
$ kubectl get pods
```

다음과 유사한 결과를 출력한다.

```
NAME                             READY   STATUS    RESTARTS   AGE
frontend-85595f5bf9-5df5m        1/1     Running   0          83s
frontend-85595f5bf9-7zmg5        1/1     Running   0          83s
frontend-85595f5bf9-cpskg        1/1     Running   0          15m
frontend-85595f5bf9-l2l54        1/1     Running   0          14m
frontend-85595f5bf9-l9c8z        1/1     Running   0          14m
redis-follower-dddfbdcc9-82sfr   1/1     Running   0          97m
redis-follower-dddfbdcc9-qrt5k   1/1     Running   0          97m
redis-leader-fb76b4755-xjr2n     1/1     Running   0          108m
```

<span>3.</span> 프론트엔드 Pod의 수를 줄이려면 다음 명령을 실행한다.

```bash
$ kubectl scale deployment frontend --replicas=2
```

<span>4.</span> Pod 목록을 쿼리하여 실행 중인 프론트엔드 파드 수를 확인한다.

```bash
$ kubectl get pods
```

다음과 유사한 결과를 출력한다.

```
NAME                             READY   STATUS    RESTARTS   AGE
frontend-85595f5bf9-cpskg        1/1     Running   0          16m
frontend-85595f5bf9-l9c8z        1/1     Running   0          15m
redis-follower-dddfbdcc9-82sfr   1/1     Running   0          98m
redis-follower-dddfbdcc9-qrt5k   1/1     Running   0          98m
redis-leader-fb76b4755-xjr2n     1/1     Running   0          109m
```

## 정리
Deployment와 서비스를 삭제하면 실행 중인 모든 Pod도 삭제된다. 레이블을 사용하여 하나의 명령으로 여러 리소스를 삭제할 수 있다.

<span>1.</span> 다음 명령을 실행하여 모든 Pod, Deployment와 서비스를 삭제한다.

```bash
$ kubectl delete deployment -l app=redis
$ kubectl delete service -l app=redis
$ kubectl delete deployment frontend
$ kubectl delete service frontend
```

다음과 유사한 결과를 출력한다.

```
deployment.apps "redis-follower" deleted
deployment.apps "redis-leader" deleted
deployment.apps "frontend" deleted
service "frontend" deleted
```

<span>2.</span> 다음과 유사한 결과를 출력한다.

```
$ kubectl get pods
```

다음과 유사한 결과를 출력한다.

```
No resources found in default namespace.
```

## 다음
- [쿠버네티스 기초](../kubernetes-basics/index.md) 인터랙티브 튜토리얼 완료하기
- Kubernetes를 사용하여 [MySQL과 워드프레스용 퍼시스턴트 볼륨](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/#visit-your-new-wordpress-blog)을 활용하여 블로그 생성하기
- [어플리케이션과 서비스 연결](https://kubernetes.io/docs/tutorials/services/connect-applications-service/)에 대해 더 알아보기
- [리소스 관리](https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/#using-labels-effectively)에 대해 더 알아보기
