 이 페이지는 외부 IP 주소를 노출하는 Kubernetes 서비스 객체를 생성하는 방법을 보인다.

## 시작을 위하여
- [kubectl](https://kubernetes.io/docs/tasks/tools/)을 설치한다
- Google Kubernetes Engine 또는 Amazon Web Services와 같은 클라우드 공급자를 사용하여 Kubernetes 클러스터를 생성한다. 이 튜토리얼은 클라우드 공급자가 필요한 [외부 로드 밸런서](https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/)를 생성한다.
- Kubernetes API 서버와 통신하도록 `kubectl`을 구성한다. 지침은 클라우드 제공자의 설명서를 참조한다.

## 목표
- Hello World 어플리케이션의 인스턴스 5개를 실행한다.
- 외부 IP 주소를 노출하는 서비스 객체를 만든다.
- 서비스 객체를 사용하여 실행 중인 어플리케이션에 액세스한다.

##  5 pod에서 실행되는 어플리케이션을 위한 서비스 생성하기

<span>1.</span>  클러스터에서 Hello World 어플리케이션을 실행한다.

```yml
# service/load-balancer-example.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/name: load-balancer-example
  name: hello-world
spec:
  replicas: 5
  selector:
    matchLabels:
      app.kubernetes.io/name: load-balancer-example
  template:
    metadata:
      labels:
        app.kubernetes.io/name: load-balancer-example
    spec:
      containers:
      - image: gcr.io/google-samples/node-hello:1.0
        name: hello-world
        ports:
        - containerPort: 8080
```

```bash
$ kubectl apply -f https://k8s.io/examples/service/load-balancer-example.yaml
```

앞의 명령은 Deployment와 연결된 ReplicaSet을 생성한다. ReplicaSet에는 각각 Hello World 어플리케이션을 실행하는 Pod 5개가 있다.

<span>2.</span> Deployment에 대한 정보를 표시한다.

```bash
$ kubectl get deployments hello-world
$ kubectl describe deployments hello-world
```

<span>3.</span> ReplicaSet 객체에 대한 정보를 디스플레이한다.

```bash
$ kubectl get replicasets
$ kubectl describe replicasets
```

<span>4.</span> deployment를 노출하는 서비스 객체를 생성한다.

```bash
$ kubectl expose deployment hello-world --type=LoadBalancer --name=my-service
```

<span>5.</span> 서비스에 다한 정보를 출력한다.

```bash
$ kubectl get services my-service
```

다음과 유사한 출력을 보인다.

```
NAME         TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)    AGE
my-service   LoadBalancer   10.3.245.137   104.198.205.71   8080/TCP   54s
```

> **Note**:  이 예에서는 다루지 않는 외부 클라우드 제공업체에서 `type=LoadBalancer` 서비스를 지원하므로 자세한 내용은 [이 페이지](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)를 참조하세요.

> **Note**:  외부 IP 주소가 `<pending>`으로 표시되면 잠시 기다렸다가 동일한 명령을 다시 입력한다.

<span>6.</span> 서비스에 대한 자세한 정보를 표시한다.

```bash
$ kubectl describe services my-service
```

다음과 유사한 출력을 보인다.

```
Name:           my-service
Namespace:      default
Labels:         app.kubernetes.io/name=load-balancer-example
Annotations:    <none>
Selector:       app.kubernetes.io/name=load-balancer-example
Type:           LoadBalancer
IP:             10.3.245.137
LoadBalancer Ingress:   104.198.205.71
Port:           <unset> 8080/TCP
NodePort:       <unset> 32377/TCP
Endpoints:      10.0.0.6:8080,10.0.1.6:8080,10.0.1.7:8080 + 2 more...
Session Affinity:   None
Events:         <none>
```

서비스에서 노출되는 외부 IP 주소(`LoadBalancer Ingress`)를 기록해 두세요. 이 예제에서 외부 IP 주소는 104.198.205.71이다. `Port`와 `NodePort`의 값도 기록하세요. 이 예에서 `Port`는 8080이고 `NodePort`는 32377이다.

<span>7.</span> 앞의 출력에서 서비스에 여러 엔드포인트가 있음을 알 수 있다. 10.0.0.6:8080, 10.0.1.6:8080, 10.0.1.7:8080와 2개 더 있다. 이는 Hello World 어플리케이션을 실행하는 Pod의 내부 주소이다. 이 주소가 Pod 주소인지 확인하려면 다음 명령을 입력한다.

```bash
$ kubectl get pods --output=wide
```

다음과 유사한 결과를 출력한다.

```
NAME                         ...  IP         NODE
hello-world-2895499144-1jaz9 ...  10.0.1.6   gke-cluster-1-default-pool-e0b8d269-1afc
hello-world-2895499144-2e5uh ...  10.0.1.8   gke-cluster-1-default-pool-e0b8d269-1afc
hello-world-2895499144-9m4h1 ...  10.0.0.6   gke-cluster-1-default-pool-e0b8d269-5v7a
hello-world-2895499144-o4z13 ...  10.0.1.7   gke-cluster-1-default-pool-e0b8d269-1afc
hello-world-2895499144-segjf ...  10.0.2.5   gke-cluster-1-default-pool-e0b8d269-cpuc
```

<span>8.</span>  외부 IP 주소(`LoadBalancer Ingress`)를 사용하여 Hello World 어플리케이션을 액세스한다.

```bash
$ curl http://<external-ip>:<port>
```

여기서 `<external-ip>`는 서비스의 외부 IP 주소(`LoadBalancer Ingress`)이고, `<port>`는 서비스 설명에 있는 Port 값이다. minikube를 사용하는 경우, `minikube service my-service`를 입력하면 브라우저에서 Hello World 어플리케이션이 자동으로 열린다.

성공적인 요청에 대한 응답은 hello 메시지이다.

```
Hello Kubernetes!
```

## 정리
서비스를 삭제하려면 다음 명령을 입력한다.

```bash
$ kubectl delete services my-service
```

Hello 어플리케이션을 실행하는 Deployment, ReplicaSet와 Pod를 삭제하려면 다음 명령을 입력한다.

```bash
$ kubectl delete deployment hello-world
```

## 다음
[어플리케이션과 서비스 연결](https://kubernetes.io/docs/tutorials/services/connect-applications-service/)에 대해 자세히 알아보세요.
