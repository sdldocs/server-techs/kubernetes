Kubernetes는 워크로드와 해당 워크로드의 구성 요소를 선언적으로 관리하기 위한 몇 가지 내장(built-in) API를 기본적으로 제공한다.

궁극적으로 어플리케이션은 Pod 내에서 컨테이너로 실행되지만, 개별 Pod를 관리하려면 많은 노력이 필요하다. 예를 들어, Pod에 장애가 발생하면 이를 대체하기 위해 새 Pod를 실행하고 싶을 것이다. Kubernetes가 이를 대신해줄 수 있다.

Kubernetes API를 사용하여 Pod보다 더 높은 추상화 수준을 나타내는 워크로드 객체를 생성하면, 사용자가 정의한 워크로드 객체의 사양에 따라 Kubernetes 컨트롤 플레인이 사용자를 대신하여 자동으로 Pod 객체를 관리한다.

워크로드 관리를 위한 내장 API는 다음과 같다.

[Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)(그리고 간접적으로 [ReplicaSet](https://kubernetes.io/docs/concepts/workloads/controllers/#:~:text=(and%2C%20indirectly%2C-,ReplicaSet,-)%2C%20the%20most%20common))는 클러스터에서 어플리케이션을 실행하는 가장 일반적인 방법이다. Deployment는 클러스터에서 상태를 저장하지 않는(stateless) 어플리케이션 워크로드 관리에 적합하며, Deployment의 모든 Pod는 상호 교환이 가능하고 필요한 경우 교체할 수 있다. (Deployment는 레거시 ReplicationController API를 대체한다).

[StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)을 사용하면 동일한 어플리케이션 코드를 실행하는 하나 이상의 Pod를 관리할 수 있으며, Pod가 고유한 아이덴티티를 갖는 데 의존한다. 이것은 Pod가 상호 교환 가능할 것으로 예상되는 Deployment와는 다르다. StatefulSet의 가장 일반적인 용도는 해당 Pod와 해당 지속성 있는(persistent) 스토리지 간에 링크를 만드는 것이다. 예를 들어, 각 Pod를 [PersistentVolume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)과 연결하는 StatefulSet를 실행할 수 있다. StatefulSet의 POd 중 하나에 장애가 발생하면, Kubernetes는 동일한 PersistentVolume에 연결된 대체 Pod를 생성한다.

[DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)는 특정 node에 로컬인 기능(예: 해당 node의 컨테이너가 스토리지 시스템을 액세스할 수 있도록 하는 드라이버)을 제공하는 Pod를 정의한다. 드라이버 또는 기타 node-레벨 서비스가 유용한 node에서 실행되어야 할 때 DaemonSet을 사용한다. DaemonSet의 각 Pod는 클래식 Unix/Posix 서버의 시스템 데몬과 유사한 역할을 수행한다. DaemonSet은 해당 node가 클러스터 네트워킹을 액세스할 수 있도록 하는 플러그인처럼 클러스터 운영에 기본이 될 수도 있고, node를 관리하는 데 도움이 될 수도 있으며, 실행 중인 컨테이너 플랫폼을 향상시키는 덜 필수적인 기능을 제공할 수도 있다. DaemonSet(과 해당 Pod)은 클러스터의 모든 node에서 실행하거나 일부 하위 집합에서만 실행할 수 있다(예: GPU가 설치된 노드에만 GPU 가속기드라이버를 설치).

[Job](https://kubernetes.io/docs/concepts/workloads/controllers/job/)] 및/또는 [Cronjob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)을 사용하여 완료까지 실행되었다가 중지되는 작업을 정의할 수 있다. job은 일회성 작업을 나타내는 반면, 각 Cronjob은 스케쥴에 따라 반복된다.

이 섹션의 다른 주제는 다음과 같다.

- [완료된 작업에 대한 자동 정리](https://kubernetes.io/docs/concepts/workloads/controllers/ttlafterfinished/)
- [ReplicationController](https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/)
