# 워크로드
워크로드는 Kubernetes에서 실행되는 어플리케이션이다. 워크로드가 단일 컴포넌트이든 함께 작동하는 여러 컴포넌트이든, Kubernetes에서는 [pod](https://kubernetes.io/docs/concepts/workloads/pods/index/) 집합 내에서 워크로드를 실행한다. Kubernetes에서 pod는 클러스터에서 실행 중인 컨테이너 집합을 나타낸다.

Kubernetes pod에는 [defined lifecycle](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/)이 있다. 예를 들어, 클러스터에서 pod가 실행 중일 때 해당 pod가 실행 중인 node에 심각한 오류가 발생하면 해당 node의 모든 pod가 실패한다. Kubernetes는 해당 수준의 장애를 최종 장애로 간주하므로 나중에 node가 정상 상태가 되더라도 복구하려면 새 pod를 생성해야 한다.

그러나 훨씬 더 쉽게 작업하기 위해 각 pod를 직접 관리할 필요는 없다. 대신, 사용자를 대신하여 일련의 pod를 관리하는 *워크로드 리소스*를 사용할 수 있다. 이러한 리소스는 사용자가 지정한 상태와 일치하도록 적절한 수의 적절한 종류의 pod가 실행되고 있는지 확인하는 컨트롤러를 구성한다.

쿠버네티스는 몇 가지 기본 워크로드 리소스를 제공한다.

- [Deployment](../../concepts/index.md)와 [ReplicaSet](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/)(레거시 리소스인 ReplicationController를 대체한다). Deployment는 클러스터에서 상태 비저장(stateless) 어플리케이션 워크로드를 관리하는 데 적합하며, Deployment의 모든 Pod는 상호 교환이 가능하고 필요한 경우 교체할 수 있다.
- [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)은 어떤 식으로든 상태를 추적하는 하나 이상의 관련 Pod를 실행할 수 있게 해준다. 예를 들어, 워크로드가 데이터를 영구적으로 기록해야 하는 경우, 각 Pod를 [PersistentVolume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)에 대응되는 StatefulSet를 실행할 수 있다. 해당 스StatefulSet에 대한 Pod에서 실행되는 코드는 동일한 StatefulSet의 다른 Pod로 데이터를 복제하여 전반적인 복원력을 향상시킬 수 있다.
- [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)은 node에 로컬인 기능을 제공하는 Pod를 정의한다. DaemonSet의 사양과 일치하는 node를 클러스터에 추가할 때마다 컨트롤 플레인은 해당 DaemonSet에 대한 Pod를 새 node에 스케줄링한다. DaemonSet의 각 pod는 클래식 Unix/POSIX 서버의 시스템 데몬과 유사한 작업을 수행한다. DaemonSet은 [클러스터 네트워킹](https://kubernetes.io/docs/concepts/cluster-administration/networking/#how-to-implement-the-kubernetes-network-model)을 실행하는 플러그인처럼 클러스터 운영의 기본이 될 수도 있고, node 관리에 도움이 될 수도 있으며, 실행 중인 컨테이너 플랫폼을 향상시키는 선택적 동작을 제공할 수도 있다.
- [Job]()과 [CronJob]()은 완료될 때까지 실행된 후 중지되는 작업을 정의하는 다양한 방법을 제공한다. [Job]()을 사용하여 완료 시까지 실행되는 작업을 한 번만 정의할 수 있습니다. [CronJob]()을 사용하여 일정에 따라 동일한 작업을 여러 번 실행할 수 있다.

더 넓은 Kubernetes 에코시스템에서 추가 동작을 제공하는 서드파티 워크로드 리소스를 찾을 수 있다. [커스텀 리소스 정의]()를 사용하여 Kubernetes 코어의 일부가 아닌 특정 동작을 원하는 경우 타사 워크로드 리소스를 추가할 수 있다. 예를 들어, 어플리케이션을 위해 Pod 그룹을 실행하지만 *모든* pod를 사용할 수 없는 경우(처리량이 많은 분산 작업의 경우) 작업을 중지하고자 한다면, 해당 기능을 제공하는 확장 기능을 구현하거나 설치할 수 있다.

## 다음
워크로드 관리를 위한 각 API 종류에 대한 설명과 함께 특정 작업을 수행하는 방법을 찾을 수 있다.

- [배포를 사용하여 상태 비저장 어플리케이션 실행하기]()
- 상태 저장(stateful) 어플리케이션을 [단일 인스턴스]() 또는 [복제된 집합]()으로 실행하기
- [Cronjob으로 자동화된 작업 실행하기]()

코드와 구성을 분리하는 Kubernetes의 메커니즘에 대해 알아보려면 [구성]()을 방문한다.

Kubernetes가 어플리케이션의 pod를 관리하는 방법에 대한 배경을 제공하는 두 지원 개념이 있다.

- [가비지 컬렉션]()은 *소유한 리소스*가 제거된 후 클러스터에서 객체를 정리한다.
- [time-to-live after finished controller]()는 작업이 완료된 후 정의된 시간이 지나면 작업을 제거한다.

어플리케이션이 실행 중이면 [서비스]()로 인터넷(internet as Service)을 제공하거나 웹 어플리케이션의 경우에만 [ingress]()를 사용하여 사용할 수 있도록 설정할 수 있다.
