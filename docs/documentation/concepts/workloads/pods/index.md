*Pods*는 Kubernetes에서 생성하고 관리할 수 있는 가장 작은 배포 가능한 컴퓨팅 단위이다.

*Pod*(고래 포드 또는 완두콩 포드)는 스토리지와 네트워크 리소스를 공유하며 컨테이너 실행 방법에 대한 사양을 갖춘 하나 이상의 컨테이너 그룹이다. Pod의 콘텐츠는 항상 공동으로 위치되고 공동 스케줄링되며 공유 컨텍스트에서 실행된다. Pod는 어플리케이션별 "논리적 호스트"를 모델링하며, 비교적 긴밀하게 결합된 하나 이상의 어플리케이션 컨테이너를 포함한다. 클라우드가 아닌 컨텍스트에서 동일한 물리적 또는 가상 머신에서 실행되는 어플리케이션은 동일한 논리적 호스트에서 실행되는 클라우드 어플리케이션과 유사하다.

Pod에는 어플리케이션 컨테이너뿐만 아니라 파드 시작 중에 실행되는 [init containers]()를 포함할 수 있다. 클러스터가 이를 제공하는 경우 디버깅을 위해 [ephemeral containers]()를 삽입할 수도 있다.

## Pod란?
> **Note**: 클러스터의 각 node에 컨테이너 런타임을 설치하여 pod를 실행할 수 있도록 해야 한다.

Pod의 공유 컨텍스트는 리눅스 네임스페이스, cgroups과 잠재적으로 다른 격리 측면의 집합으로, 컨테이너를 격리하는 것과 동일하다. Pod의 컨텍스트 내에서, 개별 어플리케이션에 추가적으로 하위 격리를 적용할 수 있다.

Pod는 공유 네임스페이스와 공유 파일시스템 볼륨을 가진 컨테이너 집합과 유사하다.

## Pod 사용
다음은 `nginx:1.14.2` 이미지를 실행하는 컨테이너로 구성된 Pod의 예이다.

```yml
# pods/simple-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
```

위에 보인 Pod를 생성하려면 다음 명령을 실행한다.

```bash
kubectl apply -f https://k8s.io/examples/pods/simple-pod.yaml
```

Pod는 일반적으로 직접 생성되지 않으며 워크로드 리소스를 사용하여 생성된다. 워크로드 리소스와 함께 Pod를 사용하는 방법에 대한 자세한 내용은 [Working with Pods]()를 참조한다.

### pod 관리를 위한 워크로드 리소스
통상적으로 싱글톤 Pods 조차도 Pod를 직접 생성할 필요는 없다. 대신, Deployment나 Job 같은 워크로드 리소스를 사용하여 생성한다. Pod 상태를 추적해야 하는 경우, StatefulSet 리소스를 고려한다.

Kubernetes 클러스터의 Pod는 크게 두 가지 방식으로 사용된다.

- **단일 컨테이너를 실행하는 Pod**. "Pod당 하나의 컨테이너" 모델은 가장 일반적인 Kerbernetes 사용 사례로, 이 경우 Pod는 단일 컨테이너를 감싸는 래퍼로 생각할 수 있으며, Kubernetes는 컨테이너를 직접 관리하지 않고 파드를 관리한다.

- **함께 작동해야 하는 여러 컨테이너를 실행하는 Pod**. Pod는 긴밀하게 결합되어 리소스를 공유해야 하는 같이 위치한(co-located) 여러 컨테이너로 구성된 어플리케이션을 캡슐화할 수 있다. 이러한 같이 위치한 컨테이너는 하나의 응집력 있는 서비스 단위를 형성한다. 예를 들어, 하나의 컨테이너는 공유 볼륨에 저장된 데이터를 대중에게 제공하는 반면, 별도의 사이드카 컨테이너는 해당 파일을 새로 고치거나 업데이트한다. Pod는 이러한 컨테이너, 스토리지 리소스, 임시 네트워크 아이덴티티 모두를 하나의 단위로 묶는다.

> **Note**: 하나의 Pod에 같이 위치하고 관리되는(co-managed) 여러 컨테이너를 그룹화하는 것은 비교적 고급 사용 사례이다. 이 패턴은 컨테이너가 긴밀하게 결합된 특정 경우에만 사용해야 한다.

각 Pod는 특정 어플리케이션의 단일 인스턴스를 실행하기 위한 것이다. 어플리케이션을 수평적으로 확장하려면(더 많은 인스턴스를 실행하여 더 많은 전체 리소스를 제공하려면) 각 인스턴스당 하나씩 여러 Pod를 사용해야 한다. Kubernetes에서는 이를 통상적으로 *replication*이라고 한다. 복제된 Pod는 일반적으로 워크로드 리소스와 해당 컨트롤러에 의해 그룹으로 생성되고 관리된다.

Kubernetes가 워크로드 리소스와 해당 컨트롤러를 사용하여 어플리케이션 확장과 자동 복구를 구현하는 방법에 대한 자세한 내용은 [Pods and controllers]()를 참조한다.

### Pod의 여러 컨테이너 관리 방법
Pod는 응집력 있는 서비스 단위를 형성하는 여러 협력 프로세스(컨테이너)를 지원하도록 설계되었다. Pod의 컨테이너들은 클러스터의 동일한 물리적 또는 가상 머신에 자동으로 공동 배치되고 공동 스케줄링된다. 컨테이너는 리소스와 종속성을 공유하고, 서로 통신하며, 종료 시기와 방법을 조정할 수 있다.

예를 들어 다음 다이어그램에서처럼 공유 볼륨에 있는 파일의 웹 서버 역할을 하는 컨테이너와 원격 소스에서 해당 파일을 업데이트하는 별도의 "사이드카" 컨테이너가 있을 수 있다.

![pod](../../../images/concepts/pod.svg)

일부 Pod에는 앱 컨테이너뿐만 아니라 init containers도 있다. init containers는 앱 컨테이너가 시작되기 전에 실행되고 완료된다.

Pods는 기본적으로 구성 컨테이너에 대해 [네트워킹](#pod-networking)과 [스토리지](#pod-storage)라는 두 종류의 공유 리소스를 제공한다.

## Pod로 작업
Kubernetes에서 개별 Pod, 심지어 싱글톤 파드를 직접 생성하는 경우는 거의 없다. 이는 Pod가 상대적으로 임시적이고 일회용 엔티티로 설계되었기 때문이다. Pod가 (사용자가 직접 또는 컨트롤러를 통해 간접적으로) 생성되면, 새 Pod는 클러스터의 Node에서 실행되도록 예약된다. Pod는 Pod가 실행을 완료하거나, Pod 객체가 삭제되거나, 리소스 부족으로 Pod가 퇴출되거나, node가 실패할 때까지 해당 node에 남아 있다.

> **Note**: Pod에서 컨테이너를 재시작하는 것을 Pod를 재시작하는 것과 혼동해서는 안 된다. Pod는 프로세스가 아니라 컨테이너를 실행하기 위한 환경이다. Pod는 삭제될 때까지 지속된다.

Pod의 이름은 유효한 DNS 서브도메인 값이어야 하지만, Pod 호스트 이름에 대하여 예기치 않은 결과가 발생할 수 있다. 최상의 호환성을 위해, 이름은 DNS 레이블에 대한 보다 제한적인 규칙을 따라야 한다.

### Pod OS
**FEATURE STATE**: Kubernetes v1.25 [stable]

`.spec.os.name` 필드를 `windows` 또는 `linux`로 설정하여 pod를 실행할 OS를 표시해야 한다. 이 두 가지 운영체제는 현재 Kubernetes가 지원하는 운영체제이다. 향후 이 목록은 확장될 수 있다.

Kubernetes v1.27에서는 이 필드에 설정한 값이 pods 스케줄링에 영향을 미치지 않는다. `.spec.os.name`을 설정하면 pod OS를 권한 있게 식별하는 데 도움이 되며 유효성 검사에 사용된다. 사용자가 pod OS를 지정한 곳에서 해당 pod가 실행 중인 node의 운영체제와 동일하지 않은 경우, kublet은 Pod 실행을 거부한다. [Pod security stabdards]()은 또한 이 필드를 사용하여 해당 운영체제와 관련이 없는 정책을 적용하지 않도록 한다.

### Pod와 컨트롤러
워크로드 리소스를 사용하여 여러 Pods를 생성하고 관리할 수 있다. 리소스에 대한 컨트롤러는 복제와 롤아웃을 처리하고 Pod 장애 시 자동 복구를 처리한다. 예를 들어, Node에 장애가 발생하면 컨트롤러는 해당 Node의 Pod가 작동을 멈춘 것을 감지하고 대체 파드를 생성한다. 스케줄러는 대체 Pod를 정상 Node에 배치한다.

다음은 하나 또는 그 이상의 Pods를 관리하는 워크로드 리소스의 몇몇 예이다.

- Deployment
- StatefulSet
- DaemonSet

### Pod 템플레트
워크로드 리소스용 컨트롤러는 *pod 템플릿*에서 Pod를 생성하고 사용자를 대신하여 해당 Pod를 관리한다.

PodTemplates은 Pod를 생성하기 위한 사양이며, [Deployments](), [Jobs]()과 [DaemonSets]() 같은 워크로드 리소스에 포함되어 있다.

워크로드 리소스에 대한 각 컨트롤러는 워크로드 오브젝트 내부의 `PodTemplate`을 사용하여 실제 Pod를 생성한다. `PodTemplate`은 앱을 실행하는 데 사용한 워크로드 리소스의 원하는 상태의 일부이다.

아래 샘플은 한 컨테이너를 시작하는 `template`이 있는 간단한 잡에 대한 매니페스트이다. 해당 파드의 컨테이너는 메시지를 출력한 후 일시 중지된다.

```yml
apiVersion: batch/v1
kind: Job
metadata:
  name: hello
spec:
  template:
    # This is the pod template
    spec:
      containers:
      - name: hello
        image: busybox:1.28
        command: ['sh', '-c', 'echo "Hello, Kubernetes!" && sleep 3600']
      restartPolicy: OnFailure
    # The pod template ends here
```

pod 템플릿을 수정하거나 새 pod 템플릿으로 전환해도 이미 존재하는 pod에는 직접적인 영향을 미치지 않는다. 워크로드 리소스의 pod 템플릿을 변경하는 경우, 해당 리소스는 업데이트된 템플릿을 사용하는 대체 pod를 생성해야 한다.

예를 들어, StatefulSet 컨트롤러는 실행 중인 Pod가 각 StatefulSet 객체에 대한 현재 pod 템플릿과 일치하는지 확인한다. StatefulSet을 편집하여 해당 pod 템플릿을 변경하면, StatefulSet은 업데이트된 템플릿을 기반으로 새 Pod를 생성하기 시작한다. 결국, 모든 이전 Pod가 새 Pod로 교체되고 업데이트가 완료된다.

각 워크로드 리소스는 Pod 템플릿의 변경을 처리하기 위한 자체 규칙을 구현한다. StatefulSet에 대해 구체적으로 자세히 알아보려면 StatefulSet 기본 튜토리얼의 [업데이트 전략]()을 읽어보시기 바란다.

Node에서, kubelet은 pod 템플릿과 업데이트에 대한 세부 사항을 직접 관찰하거나 관리하지 않으며, 이러한 세부 사항은 추상화되어 있다. 이러한 추상화와 관심사 분리는 시스템 시맨틱을 단순화하고 기존 코드를 변경하지 않고도 클러스터의 동작을 확장할 수 있도록 한다.

## Pod 업데이트와 교체
이전 섹션에서 언급했듯이, 워크로드 리소스에 대한 Pod 템플릿이 변경되면 컨트롤러는 기존 Pod를 업데이트하거나 패치하는 대신 업데이트된 템플릿을 기반으로 새 Pod를 생성한다.

Kubernetes는 사용자가 직접 Pod를 관리하는 것을 막지는 않는다. 실행 중인 Pod의 일부 필드를 제자리에서 업데이트할 수 있다. 그러나 [ptach]()와 [replace]() 같은 Pod 업데이트 작업에는 몇 가지 제한이 있다.

- Pod에 대한 대부분의 메타데이터는 변경할 수 없다. 예를 들어, `namespace`, `name`, `uid` 또는 `creationTimestamp` 필드는 변경할 수 없으며 `generation` 필드는 고유하다. 필드의 현재 값을 증가시키는 업데이트만 허용한다.
- `metadata.deletionTimestamp`가 설정된 경우, `metadata.finalizers` 목록에 새 항목을 추가할 수 없다.
- Pod 업데이트는 `spec.containers[*].image, spec.initContainers[*].image`, `spec.activeDeadlineSeconds` 또는 `spec.tolerations` 이외의 필드를 변경할 수 없다. `spec.tolerations`의 경우 새 항목만 추가할 수 있습니다.
- `spec.activeDeadlineSeconds` 필드를 업데이트할 때 두 가지 유형의 업데이트가 허용된다.
  1. 할당되지 않은 필드에 양수를 설정한다
  2. 양수에서 음수가 아닌 작은 숫자로 필드를 업데이트하는 경우

## 리소스 공유와 통신
Pod는 구성 컨테이너 간 데이터를 공유하고 통신할 수 있도록 해준다.

### <a name="pod-storage"></a>Pod의 스토리지
Pod는 공유 스토리지 볼륨 집합을 지정할 수 있다. Pod의 모든 컨테이너에서 공유 볼륨을 액세스할 수 있어 해당 컨테이너는 데이터를 공유할 수 있다. 또한 볼륨을 사용하면 Pod 내의 컨테이너 중 하나를 재시작해야 하는 경우에도 Pod의 지속(persistent) 데이터를 유지할 수 있다. Kubernetes가 공유 스토리지를 구현하고 Pod에서 사용할 수 있도록 하는 방법에 대한 자세한 내용은 [스토리지]()를 참조한다.

### <a name="pod-networking"></a>Pod 네트워킹
각 Pod에는 각 주소 계열에 대한 고유한 IP 주소가 할당된다. Pod의 모든 컨테이너는 IP 주소와 네트워크 포트를 포함한 네트워크 네임스페이스를 공유한다. Pod 내부에서만 Pod에 속한 컨테이너는 `localhost`를 사용하여 서로 통신할 수 있다. Pod 내의 컨테이너가 *Pod 외부*의 엔티티와 통신할 때는 공유 네트워크 리소스(예: 포트)를 사용하는 방법을 조정해야 한다. Pod 내에서 컨테이너는 IP 주소와 포트 공간을 공유하며, `localhost`를 통해 서로를 찾을 수 있다. 또한 Pod의 컨테이너는 SystemV semaphores 또는 POSIX 공유 메모리와 같은 표준 inter-process communication을 사용하여 서로 통신할 수 있다. 서로 다른 Pod의 컨테이너는 고유한 IP 주소를 가지며, 특별한 구성 없이는 OS-level IPC로 통신할 수 없다. 다른 Pod에서 실행 중인 컨테이너와 상호 작용하려는 컨테이너는 IP 네트워킹을 사용하여 통신할 수 있다.

Pod 내의 컨테이너는 시스템 호스트 이름을 Pod에 구성된 `name`과 동일한 것으로 본다. 이에 대한 자세한 내용은 [네트워킹]() 섹션에서 확인할 수 있다.

## 컨테이너를 위한 권한 모드
> **Note**: 이 설정을 적용하려면 컨테이너 런타임이 특권(priveileged) 컨테이너 개념을 지원해야 한다.

pod의 모든 컨테이너는 특권 모드로 실행하여 다른 방법으로는 액세스할 수 없는 운영 체제 관리 기능을 사용할 수 있다. 이 기능은 Windows와 Linux 모두에서 사용할 수 있다.

### Linux 특권 컨테이너
리눅스에서, Pod의 모든 컨테이너는 컨테이너 스펙의 [보안 컨텍스트]()에서 `previleged`(리눅스) 플래그를 사용하여 특권 모드를 활성화할 수 있다. 이는 네트워크 스택을 조작하거나 하드웨어 디바이스에 액세스하는 등 운영 체제 관리 기능을 사용하려는 컨테이너에 유용하다.

### Windows 특권 컨테이너
**FEATURE STATE**: Kubernetes v1.26 [stable]

Windows에서는 pod 스펙의 보안 컨텍스트에서 `windowsOptions.hostProcess` 플래그를 설정하여 [Windows HostProcess pod]()를 생성할 수 있다. 이 pod의 모든 컨테이너는 Windows HostProcess 컨테이너로 실행되어야 한다. HostProcess pod는 호스트에서 직접 실행되며, 리눅스 특권 컨테이너와 마찬가지로 관리 작업을 수행하는 데에도 사용할 수 있다.

## 정적(static) Pod
*정적 Pod*는 API 서버가 관찰하지 않고 특정 노드에서 kubelet 데몬에 의해 직접 관리된다. 대부분의 Pod는 컨트롤 플레인(예: Deployment)에서 관리되는 반면, 정적 Pod의 경우, kubelet이 각 정적 Pod를 직접 감독하고 실패하면 다시 시작한다.

정적 Pod는 항상 특정 노드에 있는 하나의 Kubelet에 바인딩된다. 정적 Pod의 주요 용도는 자체 호스팅된 컨트롤 플레인을 실행하는 것, 즉 개별 [control plane components]()를 감독하기 위해 kubelet을 사용하는 것이다.

kubelet은 각 정적 파드에 대해 Kubernetes API 서버에 미러(mirror) Pod를 자동으로 생성하려고 시도한다. 즉, node에서 실행 중인 Pod는 API 서버에서 볼 수 있지만 제어할 수는 없다. 자세한 내용은 [Create static Pods]() 가이드를 참조한다.

> **Note**: 정적 Pod의 spec은 다른 API 객체(예: ServiceAccount, ConfigMap, Secret 등)를 참조할 수 없다.

## 컨테이너 탐침
*프로브(probe)*는 컨테이너에 대해 kubelet이 주기적으로 수행하는 진단이다. 진단을 수행하기 위해, kubelet은 다양한 작업을 호출할 수 있다.

- `ExecAction`(컨테이너 런타임의 도움으로 수행)
- `TCPSocketAction`(kubelet이 직접 확인)
- `HTTPGetAction`(kubelet이 직접 확인)

[프로브]()에 대한 자세한 내용은 파드 라이프사이클 문서에서 확인할 수 있다.

## 다음
- [Pod의 새명주기]()에 대해 알아본다.
- [RuntimeClass]()에 대해 알아보고 이를 사용하여 다양한 컨테이너 런타임 구성으로 다양한 Pod를 구성하는 방법에 대해 알아본다.
- [PodDisruptionBudget]()과 이를 사용하여 중단 중 어플리케이션 가용성을 관리하는 방법에 대해 읽어본다.
- Pod는 Kubernetes REST API의 최상위 리소스이다. [Pod]() 객체 정의는 이에 대해 자세히 설명한다.
- [The Distributed System Toolkit: Patterns for Composite Containers](https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/)은 둘 이상의 컨테이너가 있는 Pod의 일반적인 레이아웃을 설명한다.
- [Pod topology spread constraints](https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/)에 대해 읽어보자

Kubernetes가 다른 리소스(예: StatefulSets 또는 Deployments)에서 공통 파드 API를 래핑하는 이유에 대한 컨텍스트를 이해하려면 다음과 같은 선행 기술에 대해 읽어야 한다.

- [Aurora](https://aurora.apache.org/documentation/latest/reference/configuration/#job-schema)
- [Borg](https://research.google.com/pubs/pub43438.html)
- [Marathon](https://mesosphere.github.io/marathon/docs/rest-api.html)
- [Omega](https://research.google/pubs/pub41684/)
- [Tupperware](https://engineering.fb.com/data-center-engineering/tupperware/)
