이 페이지는 Pod의 라이프사이클에 대해 설명한다. Pod는 정의된 라이프사이클을 따르며, `Pending` [단계](#pod-단계)에서 시작하여 기본 컨테이너 중 하나 이상이 정상적으로 시작되면 `Running`으로 이동하고, Pod의 컨테이너가 실패로 종료되었는지 여부에 따라 `Succeeded` 또는 `Failed` 단계로 이동한다.

Pod가 실행되는 동안, Kubelet은 일부 종류의 결함을 처리하기 위해 컨테이너를 재시작할 수 있다. Pod 내에서 Kubernetes는 다양한 [컨테이너 상태](#컨테이너-상태)를 추적하고 Pod를 다시 정상 상태로 만들기 위해 취해야 할 조치를 결정한다.

Kubernetes API에서 Pod는 사양과 실제 상태를 모두 가지고 있다. Pod 객체 상태는 [Pod 조건](#pod-조건) 집합으로 구성된다. 어플리케이션에 유용하다면 [custom readiness information](#pod-준비) 정보를 Pod의 상태 데이터에 주입할 수도 있다.

Pod는 일생 동안 한 번만 [스케줄](https://kubernetes.io/docs/concepts/scheduling-eviction/)된다. Pod가 Node에 스케줄(할당)되면, Pod는 Node가 중지되거나 [종료](#terminated)될 때까지 해당 Node에서 실행된다.

## Pod 수명
개별 어플리케이션 컨테이너와 마찬가지로, Pod는 상대적으로 (내구성이 아닌)임시적인 엔티티로 간주된다. Pod는 생성되고, 고유 ID([UID]())가 할당되고, node로 스케줄링되어 재시작 정책에 따라 종료되거나 삭제될 때까지 유지된다. node가 죽으면, 해당 node에 스케줄된 Pod는 타임아웃 기간 후에 [삭제되도록 예약](#pod-가베지-콜렉션)된다.

Pod는 자체적으로 자가 복구되지 않는다. Pod가 노드에 스케줄된 후 실패하면 Pod가 삭제되며, 마찬가지로 리소스 부족 또는 Node 유지보수로 인한 퇴거에서도 Pod는 살아남지 못한다. Kubernetes는 컨트롤러라고 하는 더 높은 수준의 추상화를 사용하여 상대적으로 일회용인 Pod 인스턴스를 관리하는 작업을 처리한다.

특정 Pod(UID로 정의됨)는 다른 node로 "재스케줄링"되지 않으며, 대신 해당 Pod는 원하는 경우 이름이 같지만 다른 UID를 가진 거의 동일한 새 Pod로 대체될 수 있다.

볼륨과 같이 어떤 것이 Pod와 동일한 수명을 갖는다고 할 때, 이는 특정 Pod(정확한 UID를 가진)가 존재하는 한 그것이 존재한다는 것을 의미한다. 어떤 이유로든 해당 Pod가 삭제되고 동일한 대체물이 생성되더라도 관련된 것(이 예에서는 볼륨)도 파괴되고 새로 생성된다.

![pod](../../../images/concepts/pod.svg)

##### Pod 다이아그램

파일 풀러와 컨테이너 간 공유 스토리지로 퍼시스턴트 볼륨을 사용하는 웹 서버를 포함하는 멀티 컨테이너 파드이다.

## Pod 단계
Pod의 `status` 필드는 [PodStatus] 객체이며, 여기에는 `phase` 필드가 있다.

Pod의 단계(Pahse)는 Pod가 라이프사이클에서 어디에 있는지에 대한 간단하고 높은 수준의 요약이다. 단계는 컨테이너 또는 Pod 상태에 대한 관찰의 포괄적인 롤업(rollup)이 아니며, 포괄적인 상태 머신이 될 의도도 없다.

Pod 단계 값의 수와 의미는 엄격하게 보호된다. 여기에 문서화된 것 외에, 특정 단계 값을 가진 Pod에 대해 어떤 것도 가정해서는 안 된다.

다음은 가능한 단계 값이다.

| **Value** | **Dscription** |
|-----------|----------------|
| Pending | Pod가 Kubernetes 클러스터에 의해 수락되었지만 하나 이상의 컨테이너가 설정되어 실행할 준비가 되지 않았다. 여기에는 Pod가 스케줄을 기다리는 데 소요되는 시간과 네트워크를 통해 컨테이너 이미지를 다운로드하는 데 소요되는 시간이 포함된다. |
| Running | Pod가 node에 바인딩되었고 모든 컨테이너가 생성되었다. 하나 이상의 컨테이너가 여전히 실행 중이거나 시작 또는 재시작 중이다. |
|Succeeded | Pod의 모든 컨테이너가 성공적으로 종료되었으며 다시 시작되지 않는다. |
| Failed | Pod의 모든 컨테이너가 종료되었고, 하나 이상의 컨테이너가 실패로 종료되었다. 즉, 컨테이너가 0이 아닌 상태로 종료되었거나 시스템에 의해 종료되었다. |
| Unknown | 어떤 이유로 Pod의 상태를 얻을 수 없다. 이 단계는 일반적으로 Pod가 실행되어야 하는 node와의 통신 오류로 인해 발생한다. |

> **Note**: Pod가 삭제될 때, 일부 kubectl 명령에 의해 파드가 `Terminating`으로 표시된다. 이 `Terminating` 상태는 Pod 단계 중 하나가 아니다. Pod는 정상적으로 종료될 수 있는 기간이 부여되며, 기본값은 30초이다. 플래그 `--force`를 사용하여 [Pod를 강제로 종료](#강제-종료된-pod)할 수 있다.

Kubernetes 1.27부터, kubelet은 [정적 Pod]()와 파이널라이저(finalizer)가 없는 [강제 삭제된 파드](#강제-종료된-pod)를 제외한 삭제된 파드를 API 서버에서 삭제하기 전에 터미널 단계(pod 컨테이너의 종료 상태에 따라 `Failed` 또는 `Succeeded`)로 전환한다.

node가 죽거나 나머지 클러스터와의 연결이 끊어지면, Kubernetes는 손실된 node에 있는 모든 Pod의 단계를 `Failed`로 설정하는 정책을 적용한다.

## 컨테이너 상태
Kebernetes는 전체 Pod의 [단계](#pod-단계)뿐만 아니라 Pod 내부의 각 컨테이너의 상태를 추적한다. [컨테이너 라이프사이클 훅]()을 사용하여 컨테이너 라이프사이클의 특정 시점에 이벤트를 실행하도록 트리거할 수 있다.

스케줄러가 Pod를 Node에 할당하면, Kubelet은 컨테이너 런타임을 사용하여 해당 Pod에 대한 컨테이너 생성을 시작한다. 가능한 컨테이너 상태는 `Waiting`, `Running` 그리고 `Terminated` 세 가지가 있다.

Pod의 컨테이너 상태를 확인하려면, `kubectl describe pod <name-of-pod>`를 사용하면 된다. 출력은 해당 Pod 내의 각 컨테이너에 대한 상태를 보여준다.

각 상태에는 특정한 의미가 있다,

### `Waiting`
컨테이너가 `Running`이거나 `Terminated` 상태가 아닌 경우, `Waiting`이다. `Waiting` 상태의 컨테이너는 시작을 위해 필요한 작업(예: 컨테이너 이미지 레지스트리에서 컨테이너 이미지 가져오기 또는 시크릿 데이터 적용)을 계속 실행하고 있다. `kubectl`을 사용하여 `Waiting`인 컨테이너가 있는 Pod를 쿼리할 때, 컨테이너가 해당 상태에 있는 이유를 요약하는 `Reason` 필드도 볼 수 있다.

### `Running`
`Running` 상태는 컨테이너가 문제 없이 실행하고 있음을 나타낸다. `postStart` 훅이 구성되었다면, 이미 실행되어 완료된 것이다. `Running`인 컨테이너가 있는 Pod를 쿼리하기 위해 `kubectl`을 사용하면 컨테이너가 언제 실행 상태에 들어갔는 지에 대한 정보도 볼 수 있다.

### `Terminated`
`Terminated` 상태의 컨테이너는 실행을 시작한 후 완료까지 실행되었거나 어떤 이유로 인해 실패한 상태이다. `Terminated` 컨테이너가 있는 Pod를 쿼리하기 위해 `kubectl`을 사용하면 이유, 종료 코드, 해당 컨테이너의 실행 기간에 대한 시작과 종료 시간을 볼 수 있다.

컨테이너에 `preStop` 훅이 구성된 경우, 이 훅은 컨테이너가 `Terminated` 상태가 되기 전에 실행된다.

## 컨테이너 재시작 정책
Pod의 `spec`에는 Always, OnFailure 또는 Never 값을 갖는 `restartPolicy` 필드가 있다. 기본값은 Always이다.

`restartPolicy`는 Pod의 모든 컨테이너에 적용되며, 같은 node에 있는 kubelet에 의한 컨테이너 재시작할 때만 참조한다. Pod의 컨테이너가 종료된 후, kubelet은 5분으로 제한되는 지수 백오프(back-off) 지연(10초, 20분, 40초, ...)으로 컨테이너를 재시작한다. 컨테이너가 문제 없이 10분 동안 실행되면, kubelet은 해당 컨테이너의 재시작 백오프 타이머를 재설정한다.

## Pod 조건
Pod에는 파드가 통과했거나 통과하지 못한 [PodConditions](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.27/#podcondition-v1-core)의 배열인 PodStatus가 있다. Kubelet은 다음과 같은 PodCondition을 관리한다:

- `PodScheduled`: Pod가 Node에 스케줄되었다.
- `PodHasNetwork`: (알파 기능, [명시적으로 활성화](#pod-네트워크-준비)해야 함) Pod 샌드박스가 성공적으로 생성되고 네트워킹이 구성되었다.
- `ContainersReady`: Pod의 모든 컨테이너가 준비되었다.
- `Initialized`: 모든 [init containers](./init-containers.md)가 성공적으로 완료되었다.
- `Ready`: Pod가 요청을 서비스할 수 있어 일치하는 모든 서비스의 로드 밸런싱 풀에 추가된다.

| **필드 이름** | **설명** |
|-------------|---------|
| type | Pod 조건의 이름 |
| status | 해당 조건이 적용 가능한지 여부를 나타내며, 가능한 값은 "True", "False" 또는 "Unknown" 이다 |
| lastProbeTime | Pod 조건이 마지막으로 프로브된 시점의 타임스탬프 |
| lastTransitionTime | Pod가 마지막으로 한 상태에서 다른 상태로 전환된 시점의 타임스탬프 |
| reason | 조건의 마지막 전환 이유를 나타내는 기계 판독 가능한 UpperCamelCase 텍스트 |
| message | 마지막 상태 전환에 대한 세부 정보를 나타내는 사람이 읽을 수 있는 메시지 |

### Pod 준비(readiness)
**FEATURE STATE**: Kubernetes v1.14 [stable]

어플리케이션은 추가 피드백이나 신호를 PodStatus에 주입할 수 있다: *파드 준비(Pod readiness)*. 이를 사용하려면, Pod의 `spec`에서 `readinessGates`를 설정하여 kubelet이 파드 준비에 대해 평가하는 추가 조건 목록을 지정한다.

readinessGates는 Pod에 대한 `status.condition` 필드의 현재 상태에 따라 결정된다. Kubernetes가 Pod의 `status.conditions` 필드에서 해당 조건을 찾을 수 없는 경우, 조건의 상태는 기본값인 "`False`"로 설정된다.

다음은 그 예이다.

```yml
ind: Pod
...
spec:
  readinessGates:
    - conditionType: "www.example.com/feature-1"
status:
  conditions:
    - type: Ready                              # a built in PodCondition
      status: "False"
      lastProbeTime: null
      lastTransitionTime: 2018-01-01T00:00:00Z
    - type: "www.example.com/feature-1"        # an extra PodCondition
      status: "False"
      lastProbeTime: null
      lastTransitionTime: 2018-01-01T00:00:00Z
  containerStatuses:
    - containerID: docker://abcd...
      ready: true
...
```

추가하는 Pod 조건은 Kubernetes [레이블 키 형식](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set)을 충족하는 이름을 가져야 한다.

### Pod 준비를 위한 상태
`kubectl patch` 명령은 객체 상태에서 패치를 지원하지 않는다. Pod에 대해 이러한 `status.conditions`를 설정하려면, 어플리케이션과 운영자는 `PATCH` 액션을 사용해야 한다. [kubernetes 클라이언트 라이브러리](https://kubernetes.io/docs/reference/using-api/client-libraries/)를 사용하여 파드 준비에 대한 커스텀 Pod 조건을 설정하는 코드를 작성할 수 있다.

커스텀 조건을 사용하는 Pod의 경우, 해당 Pod는 다음 두 조건이 모두 적용되는 경우에만 준비된 것으로 평가된다.

- Pod의 모든 컨테이너가 준비되었다
- `readinessGates`에 지정된 모든 조건이 `True`이다.

Pod의 컨테이너가 준비되었지만 하나 이상의 커스텀 조건이 누락되었거나 `False`인 경우, kubelet은 Pod의 [조건](#pod-조건)을 `ContainersReady`로 설정한다.

### Pod 네트워크 준비
**EATURE STATE**: Kubernetes v1.25 [alpha]

node에서 Pod가 스케줄된 후에는, Kubelet에 의해 어드미션되고 모든 볼륨이 마운트되어야 한다. 이러한 단계가 완료되면, Kubelet은 컨테이너 런타임(컨테이너 런타임 인터페이스(CRI) 사용)과 함께 작동하여 런타임 샌드박스를 설정하고 Pod에 대한 네트워킹을 구성한다. `PodHasNetworkCondition` [기능 게이트(feature gate)](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#:~:text=If%20the%20PodHasNetworkCondition-,feature%20gate,-is%20enabled%2C%20Kubelet)가 활성화된 경우, Kubelet은 Pod의 `status.conditions` 필드에 있는 `PodHasNetwork` 컨디션을 통해 Pod가 이 초기화 마일스톤에 도달했는지 여부를 보고한다.

Pod에 네트워킹이 구성된 런타임 샌드박스가 없는 것을 감지하면 Kubelet은 `PodHasNetwork` 조건을 `False`로 설정한다. 이는 다음 시나리오에서 발생한다.

- Pod의 라이프사이클 초기에, 아직 컨테이너 런타임을 사용하여 파드에 대한 샌드박스를 설정하기 시작하지 않은 경우
- Pod의 라이프사이클 후반에, Pod 샌드박스가 다음 중 하나로 인해 파괴된 경우
    - Pod가 퇴출되지 않고 node가 재부팅되는 경우
    - 격리를 위해 가상 머신을 사용하는 컨테이너 런타임의 경우, Pod 샌드박스 가상 머신이 재부팅되는 경우, 새로운 샌드박스와 새로운 컨테이너 네트워크 구성을 생성해야 한다

런타임 플러그인에 의해 샌드박스 생성과 Pod에 대한 네트워크 구성이 성공적으로 완료된 후 kubelet에 의해 `PodHasNetwork` 조건이 `True`로 설정된다. `PodHasNetwork` 조건이 `True`로 설정된 후, kubelet은 컨테이너 이미지 가져오기를 시작하고 컨테이너를 생성할 수 있다.

init container가 있는 Pod의 경우, init container가 성공적으로 완료된 후(런타임 플러그인에 의한 샌드박스 생성과 네트워크 구성이 성공적으로 완료된 후 발생) kubelet은 `Initialized` 조건을 `True`로 설정한다. init container가 없는 Pod의 경우, 샌드박스 생성과 네트워크 구성이 시작되기 전에 kubelet은 `Initialized` 조건을 `True`로 설정한다.

### Pod 스케쥴링 준비
**FEATURE STATE**: Kubernetes v1.26 [alpha]

자세한 내용은 [파드 스케줄링 준비(Pod Scheduling Readniess](https://kubernetes.io/docs/concepts/scheduling-eviction/pod-scheduling-readiness/)를 참조한다.

## 컨테이너 프로브(probe)
*프로브*는 컨테이너에 대해 [kubelet](https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/)이 주기적으로 수행하는 진단이다. 진단을 수행하기 위해, kubelet은 컨테이너 내에서 코드를 실행하거나 네트워크 요청을 수행한다.

### 확인 메카니즘
프로브를 사용하여 컨테이너를 확인하는 방법에는 네 가지가 있다. 각 프로브는 이 네 가지 메커니즘 중 정확히 하나를 정의해야 한다.

`exec`

컨테이너 내에서 지정된 명령을 실행한다. 명령의 상태 코드 `0`으로 종료되면 진단이 성공한 것으로 간주한다.

`grpc`

[gRPC]()를 사용하여 원격 프로시저 호출을 수행한다. 대상은 [gRPC health check]()를 구현해야 한다. 응답 `status`가 `SERVING`이면 진단이 성공한 것으로 간주한다.

`httpGet`

지정된 포트와 경로에서 Pod의 IP 주소에 대해 HTTP `GET` 요청을 수행한다. 응답의 상태 코드가 200 이상이고 400 미만이면 진단이 성공한 것으로 간주한다.

`tcpSokcet`

지정된 포트에서 Pod의 IP 주소에 대해 TCP 검사를 수행한다. 포트가 열려 있으면 진단이 성공한 것으로 간주한다. 원격 시스템(컨테이너)이 연결이 열린 후 즉시 연결을 닫으면 정상으로 간주한다.

> **Caution**: 다른 메커니즘과 달리, `exec` 프로브의 구현은 실행될 때마다 여러 프로세스를 생성/포크하는 것을 포함한다. 결과적으로, Pod 밀도가 높고 `initialDelaySeconds`의 간격,`periodSeconds`보다 짧은 클러스터의 경우, exec 메커니즘으로 프로브를 구성하면 노드의 CPU 사용량에 대한 오버헤드가 발생할 수 있다. 이러한 시나리오에서는 오버헤드를 피하기 위해 대체 프로브 메커니즘을 사용하는 것을 고려하여야 한다.

### 프로브 결과
각 프로브는 세 가지 중 하나를 결과로 갖는다.

`Success`

컨테이너가 진단을 통과했다.

`Failure`

컨테이너가 진단에 실패했다.

`Unknown`

진단에 실패했다(아무런 조치를 취하지 않아야 하며, kubelet이 추가 확인을 수행한다).

### 프로브 타입
kubelet은 실행 중인 컨테이너에서 세 가지 프로브를 선택적으로 수행하고 이에 반응할 수 있다.

`livenessProbe`

컨테이너가 실행 중인지 여부를 나타낸다. 활성 프로브가 실패하면, kubelet은 컨테이너를 종료하고 컨테이너에 [restart 정책](#컨테이너-재시작-정책)을 적용한다. 컨테이너가 활성 프로브를 제공하지 않는 경우, 기본 상태는 `Success`이다.

`readinessProbe`

컨테이너가 요청에 응답할 준비가 되었는지 여부를 나타낸다. 준비 프로브가 실패하면, 엔드포인트 컨트롤러는 Pod가 제공하는 모든 서비스의 엔드포인트에서 Pod의 IP 주소를 제거한다. 초기 지연 이전의 기본 준비 상태는 `Failure`이다. 컨테이너가 준비 프로브를 제공하지 않는 경우, 기본 상태는 `Success`이다.

`startupProbe`

컨테이너 내의 어플리케이션이 시작되었는지 여부를 나타낸다. 시작 프로브가 제공되면 성공할 때까지 다른 모든 프로브는 비활성화된다. 시작 프로브가 실패하면, kubelet은 컨테이너를 종료하고 컨테이너는 [restart 정책](#컨테이너-재시작-정책)의 적용을 받는다. 컨테이너가 시작 프로브를 제공하지 않는 경우, 기본 상태는 `Success`이다.

활성, 준비 또는 시작 프로브를 설정하는 방법에 대한 자세한 내용은 [Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)를 참조한다.

#### 활성 프로브는 언제 사용해야 할까?
컨테이너의 프로세스가 문제가 발생하거나 건강하지 않을 때마다 자체적으로 충돌할 수 있는 경우, 반드시 활성 프로브가 필요하지 않으며, kubelet은 Pod의 `restartPolicy`에 따라 자동으로 작업을 정확히 수행한다.

프로브가 실패하면 컨테이너가 종료되고 다시 시작되도록 하려면, 활성 프로브를 지정하고 `restartPolicy`를 항상 또는 실패 시로 지정하여야 한다.

#### 준비 프로브는 언제 사용해야 할까?
프로브가 성공할 때만 Pod로 트래픽을 전송하기 시작하려면, 준비 프로브를 지정한다. 이 경우, 준비 프로브는 활성 프로브와 동일할 수 있지만, spec에 준비 프로브가 존재한다는 것은 Pod가 트래픽을 수신하지 않고 시작하고 프로브가 성공하기 시작한 후에야 트래픽을 수신하기 시작한다는 것을 의미한다.

유지보수를 위해 컨테이너를 스스로 중단할 수 있도록 하려면, 상태 프로브와 다른 특정 엔드포인트를 확인하는 준비 상태 프로브를 지정할 수 있다.

앱이 백엔드 서비스에 엄격하게 종속되어 있는 경우, 활성 상태 프로브와 준비 상태 프로브를 모두 구현할 수 있습니다. 앱 자체의 상태가 양호하면 상태 프로브는 통과하지만, 준비 상태 프로브는 필요한 각 백엔드 서비스를 사용할 수 있는지 추가로 확인합니다. 이렇게 하면 오류 메시지로만 응답할 수 있는 파드로 트래픽을 보내는 것을 방지할 수 있다.

컨테이너를 시작하는 동안 대용량 데이터, 구성 파일 또는 마이그레이션을 로드해야 하는 경우, 시작 프로브를 사용할 수 있다. 그러나 실패한 앱과 여전히 시작 데이터를 처리 중인 앱의 차이를 감지하려는 경우, 준비 프로브를 사용하는 것이 더 좋을 수 있다.

> **Note**: Pod가 삭제될 때 요청을 배출할 수 있기를 원한다면, 준비 프로브가 반드시 필요하지 않으며, 삭제 시 Pod는 준비 프로브의 존재 여부와 관계없이 자동으로 준비되지 않은 상태가 된다. Pod는 파드Pod의 컨테이너가 중지될 때까지 기다리는 동안 준비되지 않은 상태로 유지된다.

#### 시작 프로브는 언제 사용해야 할까?
시작 프로브(startup probe)는 서비스되는 데 시간이 오래 걸리는 컨테이너가 있는 Pod에 유용하다. 긴 활성 간격을 설정하는 대신, 컨테이너가 시작될 때 프로브하는 별도의 구성을 생성하여 활성 간격보다 더 긴 시간을 허용할 수 있다.

컨테이너가 일반적으로 `nitialDelaySeconds + failureThreshold × periodSeconds`를 초과하여 시작되는 경우, 활성 프로브와 동일한 엔드포인트를 검사하는 시작 프로브를 지정해야 한다. `periodSeconds`의 기본값은 10초이다. 그런 다음 활성 프로브의 기본값을 변경하지 않고 컨테이너가 시작될 수 있을 만큼 충분히 높게 `failureThreshold`를 설정해야 한다. 이렇게 하면 교착 상태를 방지하는 데 도움이 된다.

## Pod 종료
Pod는 클러스터의 node에서 실행되는 프로세스를 나타내므로, 더 이상 필요하지 않을 때 해당 프로세스가 (`KILL` 신호로 갑자기 중지되어 정리할 기회를 갖지 못하는 대신) 정상적으로 종료될 수 있도록 하는 것이 중요하다.

설계 목표는 사용자가 삭제를 요청하고 프로세스가 언제 종료되는지 알 수 있도록 하면서도 삭제가 최종적으로 완료되도록 하는 것이다. 사용자가 Pod 삭제를 요청하면, 클러스터는 Pod를 강제 종료하기 전에 의도한 유예 기간을 기록하고 추적한다. 강제 종료 추적이 완료되면, kubelet은 유예된 종료를 시도한다.

일반적으로 컨테이너 런타임은 각 컨테이너의 메인 프로세스에 `TERM` 신호를 보낸다. 많은 컨테이너 런타임은 컨테이너 이미지에 정의된 `STOPSIGNAL` 값을 존중하여 `TERM `대신 이 값을 전송한다. 유예 기간이 만료되면, 나머지 프로세스에 `KILL` 신호가 전송되고, 그 후 Pod가 API 서버에서 삭제된다. 프로세스가 종료되기를 기다리는 동안 kubelet 또는 컨테이너 런타임의 관리 서비스가 다시 시작되면, 클러스터는 원래의 전체 유예 기간을 포함하여 처음부터 다시 시도한다.

흐름 예:

1. 기본 유예 기간(30초)을 사용하여 특정 Pod를 수동으로 삭제하려면 kubectl 도구를 사용한다.
2. API 서버의 Pod는 유예 기간과 함께 Pod가 "dead" 것으로 간주되는 시간으로 업데이트된다. 삭제하려는 Pod를 확인하기 위해 `kubectl describe`를 사용하면 해당 Pod가 "Terminating으로 표시된다. Pod가 실행 중인 node에서 파드가 종료되는 것으로 표시된 것을 확인하자마자(유예 종료 기간이 설정되어 있음), kubelet은 로컬 파드 종료 프로세스를 시작한다.
    1. Pod의 컨테이너 중 하나에 `preStop` 훅이 정의되어 있고 Pod spec의 `terminationGracePeriodSeconds`가 0으로 설정되어 있지 않은 경우, kubelet은 컨테이너 내부에서 해당 훅을 실행한다. `terminationGracePeriodSeconds`의 기본 설정은 30초이다.
    유예 기간이 만료된 후에도 `preStop` 훅이 여전히 실행 중이면, kubelet은 2초의 짧은 일회성 유예 기간 연장을 요청한다.

    > **Note**: `preStop` 후크가 완료 시간이 기본 유예 기간보다 더 오래 걸리는 경우, 이에 맞게 `terminationGracePeriodSeconds`를 수정해야 한다.

    2. kubelet은 컨테이너 런타임을 트리거하여 각 컨테이너 내부에서 위의 1항을 처리하도록 `TERM` 신호를 보낸다.

    > **Note**: Pod의 컨테이너는 서로 다른 시간에 임의의 순서로 `TERM` 신호를 수신한다. 종료 순서가 중요하다면, 동기화를 위해 `preStop` 훅을 사용하는 것을 고려해야 한다.

3. kubelet이 파드의 정상적으로 종료를 시작하는 것과 동시에, 컨트롤 플레인은 종료 중인 파드를 EndpointSlice(와 Endpoints) 객체에서 제거할지 여부를 평가하며, 여기서 해당 객체는 구성된 selector가 있는 Service를 나타낸다. ReplicaSets과 다른 워크로드 리소스는 더 이상 종료되는 Pod를 유효한 서비스 내 복제로 취급하지 않는다.
천천히 종료되는 파드는 일반 트래픽을 계속 처리해서는 안되며, 종료되기 시작하여 열려있는 연결에 대한 처리를 완료해야 한다. 일부 어플리케이션은 열린 연결을 끝내는 것 이상으로 세션 배출(session draining)와 완료 같이 더 우아하게 종료해야 한다.
종료되는 파드를 나타내는 모든 엔드포인트는 EndpointSlices에서 즉시 제거되지 않으며, [terminating state](https://kubernetes.io/docs/concepts/services-networking/endpoint-slices/#conditions)를 나타내는 상태가 EndpointSlices API(와 레거시 엔드포인트 API)로 부터 노출된다. 종료되는 엔드포인트는 항상 `ready` 상태가 `false`(1.26 이전 버전과의 하위 호환성을 위해)이므로 로드 밸런서는 일반 트래픽에 사용하지 않는다.
종료하는 Pod에서 트래픽 배출이 필요한 경우, 활성 준비는 조건 `serving`으로 확인할 수 있다. 연결 배출을 구현하는 방법에 대한 자세한 내용은 [Pod Anf Endpoints Termination Flow](https://kubernetes.io/docs/tutorials/services/pods-and-endpoint-termination-flow/) 튜토리얼에서 확인할 수 있다.

> **Note**: 클러스터에서 `EndpointSliceTerminationCondition` 기능 게이트를 활성화하지 않은 경우(이 게이트는 쿠버네티스 1.22부터 기본적으로 켜져 있고 1.26에서는 기본값으로 잠겨 있다), Kubernetes 컨트롤 플레인은 파드의 종료 유예 기간이 *시작*되는 즉시 모든 관련 EndpointSLices에서 파드를 제거한다. 위의 동작은 기능 게이트인 `EndpointSliceTerminationCondition`이 활성화된 경우에 설명되어 있다.

1. 유예 기간이 만료되면, kubelet은 강제 종료를 트리거한다. 컨테이너 런타임은 Pod의 모든 컨테이너에서 여전히 실행 중인 모든 프로세스에 `SIGKILL`을 보낸다. 또한 컨테이너 런타임이 숨겨진 `pause` 컨테이너를 사용하는 경우, kubelet은 숨겨진 이들을 정리한다.
2. kubelet은 파드를 터미널 단계로 전환한다(컨테이너의 최종 상태에 따라 `Failed` 또는 `Succeeded`). 이 단계는 버전 1.27부터 보장된다.
3. kubelet은 유예 기간을 0(즉시 삭제)으로 설정하여 API 서버에서 Pod 객체의 강제 제거를 트리거한다.
4. API 서버는 Pod의 API 객체를 삭제하고, 그 후 모든 클라이언트에서 더 이상 볼 수 없다.

### 강제 종료된 Pod
> **Caution**: 강제 삭제는 일부 워크로드와 해당 Pod에 잠재적으로 중단을 일으킬 수 있다.

기본적으로 모든 삭제는 30초 내에서 유예된다. `kubectl delete` 명령은 기본값을 재정의하고 고유한 값을 지정할 수 있는 `--grace-period=<seconds>` 옵션을 지원한다.

유예 기간을 `0`으로 설정하면 API 서버에서 Pod가 강제로 즉시 삭제된다. Pod가 여전히 node에서 실행 중이면, 이 강제 삭제는 kubelet이 즉각적인 정리를 시작하도록 트리거한다.

> **Note**: 강제 삭제를 수행하려면 -`-grace-period=0`과 함께 추가 플래그 `--force`를 지정해야 한다.

강제 삭제가 수행될 때, API 서버는 실행 중이던 node에서 파드가 종료되었다는 kubelet의 확인을 기다리지 않는다. 동일한 이름으로 새 Pod를 생성할 수 있도록 API에서 즉시 Pod를 제거한다. node에서 즉시 종료되도록 설정된 Pod는 강제 종료되기 전에 약간의 유예 기간이 주어진다.

> **Caution**: 즉시 삭제는 실행 중인 리소스가 종료되었다는 확인을 기다리지 않는다. 리소스는 클러스터에서 무기한 계속 실행될 수 있다.

StatefulSet가 일부분인 Pod를 강제로 삭제해야 하는 경우, [deleting Pods from a StatefulSet](https://kubernetes.io/docs/tasks/run-application/force-delete-stateful-set-pod/)에 대한 작업 설명서를 참조한다.

### Pod 가베지 콜렉션
실패한 Pod의 경우, API 객체는 사람이나 컨트롤러 프로세스가 명시적으로 제거할 때까지 클러스터의 API에 남아 있다.

컨트롤 플레인의 컨트롤러인 Pod 가비지 컬렉터(PodGC)는 Pod 수가 구성된 임계값(kube-controller-manager의 `terminated-pod-gc-threshold`)을 초과하면 종료된 파드(`Succeeded` 또는 `Failure` 단계로)를 정리한다. 이렇게 하면 시간이 지남에 따라 Pod를 생성과 종료할 때 리소스 누수를 방지할 수 있다.

또한, PodGC는 다음 조건 중 하나를 만족하는 모든 Pod를 정리한다.

1. 고아 Pod - 더 이상 존재하지 않는 노드에 바인딩된 경우,
2. 스케줄되지 않은 종료 파드,
3. 종료 Pod, `NodeOutOfServiceVolumeDetach` 기능 게이트가 활성화된 경우, `node.kubernetes.io/out-of-service`로 오염된 준비되지 않은 노드에 바인딩된 경우.

`PodDisruptionConditions` 기능 게이트가 활성화되면, Pod 정리와 함께 Pod가 종료되지 않은 단계에 있는 경우 PodGC는 Pod를 실패로 표시한다. 또한, PodGC는 고아 Pod를 정리할 때 Pod 중단 조건을 추가한다. 자세한 내용은 [Pod disruption conditions](https://kubernetes.io/docs/concepts/workloads/pods/disruptions#pod-disruption-conditions)을 참고한다.

## 다음
- [컨테이너 수명주기 이벤트에 핸들러를 연결](https://kubernetes.io/docs/tasks/configure-pod-container/attach-handler-lifecycle-event/)해보는 실습을 해보세요.
- [활성, 준비와 시작 프로브를 직접 구성](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)해 보세요.
- [컨테이너 수명주지 훅](https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/)에 대해 자세히 알아본다.
- API에서 `Pod`와 컨테이너 상태에 대한 자세한 정보는 Pod `status`에 대한 API 참조 문서를 참고한다.
