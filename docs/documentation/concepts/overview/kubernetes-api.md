Kubernetes 컨트롤 플레인의 핵심은 API 서버이다. API 서버는 최종 사용자, 클러스터의 다른 부분과 외부 컴포넌트가 서로 통신할 수 있는 HTTP API를 노출한다.

Kubernetes API를 사용하면 Kubernetes의 API 객체(예: pod, 네임스페이스, ConfigMaps, 이벤트)의 상태를 쿼리하고 조작할 수 있다.

대부분의 작업은 [kubectl](https://kubernetes.io/docs/reference/kubectl/) 커맨드-라인 인터페이스 또는 API를 사용하는 [kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/)같은 다른 커맨드-라인 도구를 통해 수행할 수 있다. 그러나 REST 호출을 사용하여 API에 직접 액세스할 수도 있다.

Kubernetes API를 사용하여 애플리케이션을 작성하는 경우 [클라이언트 라이브러리](https://kubernetes.io/docs/reference/using-api/client-libraries/) 중 하나를 사용하는 것을 고려해보자.

## OpenAPI 사양
전체 API 세부 정보는 [OpenAPI](https://www.openapis.org/)를 사용하여 문서화되어 있다.

### OpenAPI V2
Kubernetes API 서버는 `/openapi/v2` 엔드포인트를 통해 집계된 OpenAPI v2 사양을 제공한다. 다음과 같이 요청 헤더를 사용하여 응답 형식을 요청할 수 있다.

| **Header** | **Posiible values** | **Notes** |
|------------|---------------------|-----------|
| Accept-Encoding | gzip | *이 Header를 제공하지 않아도 된다* |
| Accept | application/com.github.proto-openapi.spec.v2@v1.0+protobuf | *클러스터 내 주로 사용* |
| | application/json | *default* |
| | * | *serves* application/json |

Kubernetes는 주로 클러스터 내 통신을 위한 대체 ProtoBuf 기반 직렬화 형식을 구현한다. 이 형식에 대한 자세한 내용은 [Kubernetes Protobuf 직렬화](https://git.k8s.io/design-proposals-archive/api-machinery/protobuf.md) 디자인 제안서와 API 객체를 정의하는 Go 패키지에 있는 각 스키마에 대한 인터페이스 정의 언어(IDL) 파일을 참조한다.

### OpenAPI V3
**FEATURE STATE:** Kubernetes v1.27 [stable]
Kubernetes는 API에 대한 설명을 OpenAPI v3로 게시하는 것을 지원한다.

사용 가능한 모든 그룹/버전의 목록을 볼 수 있는 디스커버리 엔드포인트 `/openapi/v3`가 제공된다. 이 엔드포인트는 JSON만 반환한다. 이러한 그룹/버전은 다음 형식으로 제공된다.

```json
{
    "paths": {
        ...,
        "api/v1": {
            "serverRelativeURL": "/openapi/v3/api/v1?hash=CC0E9BFD992D8C59AEC98A1E2336F899E8318D3CF4C68944C3DEC640AF5AB52D864AC50DAA8D145B3494F75FA3CFF939FCBDDA431DAD3CA79738B297795818CF"
        },
        "apis/admissionregistration.k8s.io/v1": {
            "serverRelativeURL": "/openapi/v3/apis/admissionregistration.k8s.io/v1?hash=E19CC93A116982CE5422FC42B590A8AFAD92CDE9AE4D59B5CAAD568F083AD07946E6CB5817531680BCE6E215C16973CD39003B0425F3477CFD854E89A9DB6597"
        },
        ....
    }
}
```

상대 URL은 클라이언트 측 캐싱을 개선하기 위해 변경 불가능한 OpenAPI 설명을 가리키고 있다. API 서버에서 이를 위해 적절한 HTTP 캐싱 헤더도 설정한다(Expires는 향후 1년으로, `Cache-Control`은 `immutable`로 설정). 더 이상 사용되지 않는 URL이 사용되면 API 서버는 최신 URL로 리디렉션을 반환한다.

Kubernetes API 서버는 Kubernetes 그룹 버전에 따라 OpenAPI v3 사양을 `/openapi/v3/apis/<group>/<version>?hash=<hash> 엔드포인트에 게시한다.

허용되는 요청 헤더는 아래 표를 참조하세요.

| **Header** | **Posiible values** | **Notes** |
|------------|---------------------|-----------|
| Accept-Encoding | gzip | *이 Header를 제공하지 않아도 된다* |
| Accept | application/com.github.proto-openapi.spec.v3@v1.0+protobuf | *클러스터 내 주로 사용* |
| | application/json | *default* |
| | * | *serves* application/json |

OpenAPI V3를 가져오기 위한 Golang 구현은 `k8s.io/client-go/openapi3` 패키지에서 제공된다.

## 지속성(Persistence)
Kubernetes는 객체의 직렬화된 상태를 etcd에 기록하여 저장한다.

## API 탐색
클러스터에서 지원되는 모든 그룹 버전 목록은 `/api`와 `/apis` 엔드포인트에 게시된다. 각 그룹 버전은 또한 `/apis/<group>/<version>`을 통해 지원되는 리소스 목록을 알린다(예: `/apis/rbac.authorization.k8s.io/v1alpha1`). 이러한 엔드포인트는 클러스터에서 지원되는 리소스 목록을 가져오는 데 kubectl에서 사용된다.

### 통합 검색
**FEATURE STATE:** Kubernetes v1.27 [beta]
Kubernetes는 그룹 버전마다 하나씩이던 것을 두 엔드포인트(`/api`와 `/apis`)를 통해 클러스터에서 지원되는 모든 리소스를 게시하는 통합 검색에 대한 베타 지원을 제공한다. 이 엔드포인트를 요청하면 평균적인 Kubernetes 클러스터에 대한 검색을 가져오기 위해 전송되는 요청의 수가 크게 줄어든다. 이 엔드포인트는 통합 검색 리소스를 나타내는 Accept 헤더를 사용하여 각 엔드포인트를 요청하여 액세스할 수 있다. `Accept: application/json;v=v2beta1;g=apidiscovery.k8s.io;as=APIGroupDiscoveryList`

엔드포인트는 ETag와 protobuf 인코딩도 지원한다.

## API 그룹과 버저닝
필드를 제거하거나 리소스 표현을 쉽게 재구성할 수 있도록 Kubernetes는 `/api/v1` 또는 `/apis/rbac.authorization.k8s.io/v1alpha1`과 같이 각각 다른 API 경로에 있는 여러 API 버전을 지원한다.

버전 관리가 리소스나 필드 수준이 아닌 API 수준에서 이루어지므로 API가 시스템 리소스와 동작에 대한 명확하고 일관된 보기를 제공하고 수명이 다했거나 실험적인 API에 대한 액세스를 제어할 수 있다.

더 쉽게 발전하고 API를 확장할 수 있도록 Kubernetes는 [활성화 또는 비활성화](https://kubernetes.io/docs/reference/using-api/#enabling-or-disabling)할 수 있는 [API 그룹](https://kubernetes.io/docs/reference/using-api/#api-groups)을 구현한다.

API 리소스는 API 그룹, 리소스 유형, 네임스페이스(네임스페이스 리소스의 경우) 및 이름으로 구분된다. API 서버는 API 버전 간의 변환을 투명하게 처리한다. 모든 다른 버전은 실제로는 동일한 영구 데이터의 표현이다. API 서버는 여러 API 버전을 통해 동일한 기본 데이터를 제공할 수 있다.

예를 들어 동일한 리소스에 대해 `v1`과 `v1beta1`이라는 두 가지 API 버전이 있다고 가정해 보자. 원래 해당 API의 `v1beta1` 버전을 사용하여 객체를 만든 경우, 나중에 `v1beta1` 버전이 더 이상 사용되지 않고 제거될 때까지 `v1beta1` 또는 `v1` API 버전을 사용하여 해당 객체를 읽거나 업데이트하거나 삭제할 수 있다. 이 시점에서는 `v1` API를 사용하여 객체에 계속 액세스하고 수정할 수 있다.

### API 변경
성공적인 시스템은 새로운 사용 사례가 등장하거나 기존 사용 사례가 변경됨에 따라 성장하고 변화해야 gks다. 따라서 Kubernetes는 지속적으로 변화하고 성장할 수 있도록 Kubernetes API를 설계했다. Kubernetes 프로젝트는 기존 클라이언트와의 호환성을 깨지 않고 다른 프로젝트가 적응할 기회를 가질 수 있도록 오랜 기간 동안 호환성을 유지하는 것이 목표이다.

일반적으로 새로운 API 리소스와 새로운 리소스 필드는 자주 그리고 자주 추가될 수 있다. 리소스나 필드를 제거하려면 API 사용 중단 정책에 따라야 한다.

Kubernetes는 공식 Kubernetes API가 일반 가용성(GA)에 도달하면 일반적으로 API 버전 `v1`과 호환성을 유지하기 위해 강력한 노력을 기울이고 있다. 또한 Kubernetes는 공식 Kubernetes API의 *beta* API 버전을 통해 유지되는 데이터와의 호환성을 유지하고 기능이 안정되면 GA API 버전을 통해 데이터를 변환하고 액세스할 수 있도록 보장한다.

beta API 버전을 채택하는 경우, 그 API 테스트가 종료되면 후속 beta 또는 stable API 버전으로 전환해야 합니다. 두 API 버전 모두에서 동시에 객체에 액세스할 수 있으므로 beta API가 사용 중단 기간 중에 전환하는 것이 가장 좋다. beta API가 사용 기간이 완료되어 더 이상 제공되지 않으면 대체 API 버전을 사용해야 한다.

> **Note**: Kubernetes는 *alpha* API 버전에 대한 호환성을 유지하는 것을 목표로 하지만, 일부 상황에서는 이것이 불가능할 수 있다. alphs API 버전을 사용하는 경우, 업그레이드하기 전에 기존의 모든 alpha 객체를 삭제해야 하는 호환되지 않는 방식으로 API가 변경되었을 경우를 대비하여 클러스터를 업그레이드할 때 Kubernetes의 릴리스 노트를 확인해야 한다.

API 버전 수준 정의에 대한 자세한 내용은 [API versions reference](https://kubernetes.io/docs/reference/using-api/#api-versioning)를 참조하세요.

## API 확장
Kubernetes API는 아래의 둘 중 한 방법으로 확장할 수 있다.

1. [커스텀 리소스](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)를 사용하면 API 서버가 선택한 리소스 API를 제공하는 방법을 선언적으로 정의할 수 있다.
2. 또한, [aggregation layer](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/apiserver-aggregation/)를 구현하여 Kubernetes API를 확장할 수도 있다.

## 다음
- [CustomResourceDefintion](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/)을 추가하여 Kubernetes API를 확장하는 방법을 알아본다.
- [Kubernetes API에 대한 액세스 제어](https://kubernetes.io/docs/concepts/security/controlling-access/)에서는 클러스터가 API 액세스에 대한 인증과 권한 부여를 관리하는 방법을 설명한다.
- [API Reference](https://kubernetes.io/docs/reference/kubernetes-api/)를 읽고 API 엔드포인트, 리소스 유형과 샘플에 대해 알아본다.
- [API 변경](https://git.k8s.io/community/contributors/devel/sig-architecture/api_changes.md#readme)에서 호환 가능한 변경을 구성하는 요소와 API를 변경하는 방법에 대해 알아보세요.
