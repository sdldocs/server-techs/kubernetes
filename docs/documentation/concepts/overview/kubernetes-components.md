Kubernetes를 배포하면 클러스터를 갖게 된다.

Kubernetes 클러스터는 컨테이너화된 어플리케이션을 실행하는 node라고 하는 워커 머신의 집합으로 구성된다. 모든 클러스터에는 하나 이상의 워커 node가 있다.

워커 node는 어플리케이션 워크로드의 구성 요소인 Pod를 호스팅한다. 컨트롤 플레인은 클러스터의 워커 node와 Pod를 관리한다. 프로덕션 환경에서 컨트롤 플레인은 일반적으로 여러 대의 컴퓨터에서 실행되고 클러스터는 일반적으로 여러 node를 실행하여 내결함성과 고가용성을 제공한다.

이 페이지에서는 완전하고 작동하는 Kubernetes 클러스터를 위해 필요한 다양한 구성 요소를 간략하게 설명할 것이다.

![](../../images/concepts/components-of-kubernetes.svg)

Kubernetes 클러스터의 구성 요소

## 컨트롤 플레인 구성 요소
컨트롤 플레인의 구성 요소는 클러스터에 대한 전역 결정(예: 스케줄링)을 내리고 클러스터 이벤트를 감지 및 대응(예: 배포의 `replicas` 필드가 충족되지 않을 때 새 pod를 시작)한다.

컨트롤 플레인 구성 요소를 클러스터의 모든 머신에서 실행할 수 있다. 그러나 간소화를 위해 설정 스크립트는 일반적으로 동일한 머신에서 모든 컨트롤 플레인 구성 요소를 시작하고 이 머신에서 사용자 컨테이너를 실행하지 않는다. 여러 머신에서 실행되는 컨트롤 플레인 설정의 예는 [kubeadm으로 고가용성 클러스터 만들기](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/)를 참조한다.

### kube-apiserver
API 서버는 Kubernetes API를 노출하는 Kubernetes 컨트롤 플레인의 구성 요소이다. API 서버는 Kubernetes 컨트롤 플레인의 프론트엔드이다.

Kubernetes API 서버의 주요 구현은 [kube-apiserver](https://kubernetes.io/docs/reference/generated/kube-apiserver/)이다. kube-apiserver는 수평 확장, 즉 더 많은 인스턴스를 배포하여 확장하도록 설계되었다. 여러 개의 kube-apiserver 인스턴스를 실행하고 해당 인스턴스 간의 트래픽 균형을 맞출 수 있다.

### etcd
모든 클러스터 데이터에 대한 Kubernetes의 백업 저장소로 사용되는 일관되고 가용성이 높은 키 값 저장소이다.

Kubernetes 클러스터에서 etcd를 백업 저장소로 사용하는 경우, 데이터에 대한 [백업](https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster) 플랜이 있는지 확인하세요.

etcd에 대한 자세한 정보는 [공식 문서](https://etcd.io/docs/)에서 확인할 수 있다.

### kube-scheduler
아직 node에 할당되지 않은 새로 생성된 pod를 감시하고 실행할 노드를 선택하는 컨트롤 플레인 컴포넌트이다.

스케줄링 결정에 고려되는 요소로는 개별과 집단 리소스 요구사항, 하드웨어/소프트웨어/정책 제약, 선호도와  비선호도 사양, 데이터 로컬리티, 워크로드 간 간섭, 데드라인 등이 있다.

### Kube-controller-manager
컨트롤러 프로세스를 실행하는 컨트롤 플레인 컴포넌트이다.

논리적으로 각 컨트롤러는 별도 프로세스지만 복잡성을 줄이기 위해 모두 단일 바이너리로 컴파일되어 단일 프로세스에서 실행된다.

컨트롤러에는 다양한 타입이 있다. 다음은 몇 가지 예이다.

- node 컨트롤러: node가 다운될 때 이를 감지하고 대응하는 역할을 담당한다.
- 작업 컨트롤러: 일회성 작업을 나타내는 Job 객체를 감시한 다음, 해당 작업을 완료할 때까지 실행하기 위해 pod를 생성한다.
- EndpointSlice 컨트롤러:  (서비스와 pod 간의 링크를 제공하기 위해) EndpointSlice 객체를 채운다.
- ServiceAccount 컨트롤러: 새 네임스페이스에 대한 기본 ServiceAccount를 생성한다.

위는 전체 목록이 아니다.

### cloud-controller-manager
클라우드 전용 제어 로직을 내장하는 Kubernetes 컨트롤 플레인 구성 요소이다. 클라우드 컨트롤러 관리자를 사용하면 클러스터를 클라우드 제공자의 API에 연결하고 해당 클라우드 플랫폼과 상호 작용하는 구성 요소와 클러스터와만 상호 작용하는 구성 요소를 분리할 수 있다.
클라우드 컨트롤러 관리자는 클라우드 공급자에 특정한 컨트롤러만 실행한다. 자체 온프레미스 또는 자체 PC 내부의 학습 환경에서 Kubernetes를 실행하는 경우 클러스터에는 클라우드 컨트롤러 매니저가 없다.

kube-controller-manager와 마찬가지로, 클라우드 컨트롤러 매니저는 논리적으로 독립적인 여러 제어 루프를 단일 프로세스로 실행하는 단일 바이너리로 결합한다. 성능을 개선하거나 장애를 견딜 수 있도록 수평적으로 확장(둘 이상의 복사본 실행)할 수 있다.

다음 컨트롤러는 클라우드 제공업체 종속성이 있을 수 있다.

- node 컨트롤러: 노드가 응답을 중지한 후 클라우드에서 노드가 삭제되었는지 확인하기 위해 클라우드 공급자를 확인한다.
- 라우트 컨트롤러: 기본 클라우드 인프라에서 경로를 설정하는 데 사용된다.
- 서비스 컨트롤러: 클라우드 공급자 로드 밸런서를 생성, 업데이트와 삭제한다.

## Node 구성 요소
모든 node에서 node 구성 요소가 실행되고, 실행 중인 pod를 유지 관리하고 Kubernetes 런타임 환경을 제공한다.

### kubelet
클러스터의 각 node에서 실행되는 에이전트이다. 컨테이너가 pod에서 실행되고 있는지 확인한다.

kubelet은 다양한 메커니즘을 통해 제공되는 파드스펙 세트를 가져와서 해당 PodSpecs에 기술된 컨테이너가 실행 중이고 건강한지 확인한다. kubelet은 Kubernetes가 생성하지 않은 컨테이너는 관리하지 않는다.

### kube-proxy
kube-proxy는 클러스터의 각 node에서 실행되는 네트워크 프록시로, Kubernetes 서비스 개념의 일부를 구현한다.

[kube-proxy](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-proxy/)는 node에서 네트워크 규칙을 유지 관리한다. 이러한 네트워크 규칙은 클러스터 내부 또는 외부의 네트워크 세션에서 pod로의 네트워크 통신을 허용한다.

운영 체제 패킷 필터링 계층이 있고 사용 가능한 경우, kube-proxy는 이를 사용한다. 그렇지 않으면, kube-proxy는 트래픽 자체를 전달한다.

### 컨테이너 런타임
컨테이너 런타임은 컨테이너 실행을 담당하는 소프트웨어이다.

Kubernetes는 containerd, CRI-O와 기타 [Kubernetes CRI(Container Runtime Interface)](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-node/container-runtime-interface.md)의 모든 구현같은 컨테이너 런타임을 지원한다.

## 애드온(Addons)
애드온은 클러스터 기능을 구현하기 위해 Kubernetes 리소스(데몬셋, 디플로이먼트 등)를 사용한다. 이들은 클러스터 수준의 기능을 제공하기 때문에, 애드온을 위한 네임스페이스 리소스는 `kube-system` 네임스페이스 내에 속한다.

선택된 애드온은 아래에 설명되어 있으며, 사용 가능한 애드온의 확장 목록은 [애드온](https://kubernetes.io/docs/concepts/cluster-administration/addons/)을 참조한다.

### DNS
다른 애드온은 반드시 필요한 것은 아니지만, 많은 예에서 클러스터 DNS를 사용하므로 모든 Kubernetes 클러스터에는 클러스터 DNS가 있어야 한다.

클러스터 DNS는 사용자 환경의 다른 DNS 서버에 추가되는 DNS 서버로, Kubernetes 서비스에 대한 DNS 레코드를 제공합니다.

Kubernetes가 시작한 컨테이너는 자동으로 이 DNS 서버를 DNS 검색에 포함한다.

### 웹 UI(Dashboard)
[대시보드](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)는 Kubernetes 클러스터를 위한 범용 웹 기반 UI이다. 사용자는 대시보드를 통해 클러스터 자체뿐만 아니라 클러스터에서 실행 중인 어플리케이션을 관리하고 문제를 해결할 수 있다.

### 컨테이너 리소스 모니터링
[컨테이너 리소스 모니터링](https://kubernetes.io/docs/tasks/debug/debug-cluster/resource-usage-monitoring/)은 컨테이너에 대한 일반적인 시계열 메트릭을 중앙 데이터베이스에 기록하고 해당 데이터를 검색할 수 있는 UI를 제공한다.

### 클러스터 수준 로깅
[클러스터 수준 로깅](https://kubernetes.io/docs/concepts/cluster-administration/logging/) 메커니즘이 컨테이너 로그를 검색/브라우징 인터페이스가 있는 중앙 로그 저장소에 저장하는 역할을 한다.

### 네트워크 플러그인
[네트워크 플러그인](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins)은 컨테이너 네트워크 인터페이스(CNI) 사양을 구현한 소프트웨어 구성 요소이다. 이들은 pod에 IP 주소를 할당하고 클러스터 내에서 서로 통신할 수 있도록 하는 역할을 한다.

## 다음
다음 단계로 아래에 대해 자세히 알아보세요.

- [Node](https://kubernetes.io/docs/concepts/architecture/nodes/)와 컨트롤 플레인과의 [통신](https://kubernetes.io/docs/concepts/architecture/control-plane-node-communication/).
- Kubernetes [컨트롤러](https://kubernetes.io/docs/concepts/architecture/controller/).
- Kubernetes의 기본 스케줄러인 [kube-scheduler](https://kubernetes.io/docs/concepts/scheduling-eviction/kube-scheduler/).
- etcd의 공식 [문서](https://etcd.io/docs/).
- Kubernetes의 여러 [컨테이너 런타임](https://kubernetes.io/docs/setup/production-environment/container-runtimes/).
- [cloud-contrller-manager](https://kubernetes.io/docs/concepts/architecture/cloud-controller/)를 사용하여 클라우드 제공자와 통합.
- [kubectl](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands) 명령어.
