Kubernetes에서는 일부 객체가 다른 객체의 *소유자(owner)*일 수 있다. 예를 들어, ReplicaSet은 Pod 집합의 소유자이다. 이러한 소유된 객체는 해당 소유자의 *디펜던트(dependents)*이다.

소유권은 일부 리소스에서 사용하는 [레이블과 선택자](./labels.md) 메커니즘과는 다르다. 예를 들어, `EndpointSlice` 객체를 생성하는 서비스를 생각해 보자. 이 서비스는 레이블을 사용하여 컨트롤 플레인이 해당 서비스에 사용되는 `EndpointSlice` 객체를 결정할 수 있도록 한다. 레이블 외에도 서비스를 대신하여 관리되는 각 `EndpointSlice`에는 소유자 참조가 있다. 소유자 참조는 Kubernetes의 다른 부분이 제어하지 않는 객체를 간섭하지 않도록 도와준다.

## 객체 사양의 소유자 참조
디펜던트 객체에는 해당 소유자 객체를 참조하는 `metadata.ownerReferences` 필드가 있다. 유효한 오너 레퍼런스는 객체 이름과 디펜던트 객체와 동일한 네임스페이스 내의 UID로 구성된다. Kubernetes는 ReplicaSets, DaemonSets, Deployments, Jobs와 CronJobs 그리고 ReplicationControllers 같은 다른 객체의 디펜던트 객체에 대해 이 필드의 값을 자동으로 설정한다. 이 필드의 값을 변경하여 이러한 관계를 수동으로 구성할 수도 있다. 그러나 일반적으로는 그럴 필요가 없으며 Kubernetes가 자동으로 관계를 관리하도록 허용할 수 있다.

또한 디펜던트 객체에는 부울 값을 취하고 특정 디펜던트 객체가 가비지 컬렉션에서 소유자 객체를 삭제하지 못하도록 차단할 수 있는지 여부를 제어하는 `ownerReferences.blockOwnerDeletion` 필드가 있다. 컨트롤러(예: Deployment 컨트롤러)가 `metadata.ownerReferences` 필드 값을 설정하면 Kubernetes는 이 필드를 자동으로 `true`로 설정한다. `blockOwnerDeletion` 필드 값을 수동으로 설정하여 가비지 컬렉션을 차단하는 디펜던트를 제어할 수 있다.

Kubernetes admission 컨트롤러는 소유자의 삭제 권한에 따라 디펜던트 리소스에 대해 이 필드를 변경할 수 있는 사용자 액세스를 제어한다. 이 제어는 권한이 없는 사용자가 소유자 객체 삭제를 지연시키는 것을 방지한다.

> **Note**: 
>
> 교차 네임스페이스(cross-namespace) 소유자 참조는 설계상 허용되지 않는다. 네임스페이스 디펜던트는 클러스터 범위 또는 네임스페이스 소유자를 지정할 수 있다. 네임스페이스 소유자는 디펜던트와 동일한 네임스페이스에 **존재해야** 한다. 그렇지 않은 경우 소유자 참조는 없는 것으로 간주되며 모든 소유자가 없는 것으로 확인되면 디펜던트 항목은 삭제 대상이다.
> 
> 클러스터 범위(cluster-scoped)의 디펜던트는 클러스터 범위의 소유자만 지정할 수 있다. v1.20+에서 클러스터 범위 디팬던트가 네임스페이스 종류를 소유자로 지정하면 확인할 수 없는 소유자 참조가 있는 것으로 간주되어 가비지 컬렉션 대상이 안될 수 있다.
> 
> v1.20+에서 가비지 컬렉터가 잘못된 교차 네임스페이스 `ownerReference` 또는 네임스페이스 종류를 참조하는 `ownerReference`가 있는 클러스터 범위 디펜던트을 감지하면 소유자 참조가 잘못된 디펜던트의 `involveObject`와 `OwnerRefInvalidNamespace`의 사유가 포함된 경고 이벤트가 보고된다. 이러한 종류의 이벤트는 `kubectl get events -A --field-selector=reason=OwnerRefInvalidNamespace`를 실행하여 확인할 수 있다.

## 소유권(ownership)과 파이널라이저
Kubernetes에 리소스를 삭제하도록 지시하면, API 서버는 관리 컨트롤러가 리소스에 대한 [파이널라이저 규칙](./finalizers.md)을 처리할 수 있도록 허용한다. 파이널라이저는 클러스터가 여전히 올바르게 작동하는 데 필요할 수 있는 리소스를 실수로 삭제하는 것을 방지한다. 예를 들어, Pod에서 여전히 사용 중인 [PersistentVolume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)을 삭제하려고 하면, `PersistentVolume`에 `kubernetes.io/pv-protection` 파이널라이저가 있기 때문에 삭제가 즉시 수행되지 않는다. 대신, Kubernetes가 파이널라이저를 지울 때까지 [볼륨](https://kubernetes.io/docs/concepts/storage/volumes/)은 `Terminating` 상태로 유지되며, 이는 `PersistentVolume`이 더 이상 Pod에 바인딩되지 않은 후에만 발생한다.

또한 Kubernetes는 [foreground or orphan cascading deletion](https://kubernetes.io/docs/concepts/architecture/garbage-collection/#cascading-deletion)를 사용할 때 소유자 리소스에 파이널라이저를 추가한다. 포어그라운드(foreground) 삭제에서는 `foreground` 파이널라이저를 추가하여 컨트롤러가 소유자를 삭제하기 전에 `ownerReferences.blockOwnerDeletion=true`인 디펜던트 리소스도 삭제해야 한다. 고아(orphan) 삭제 정책을 지정하면, Kubernetes는 컨트롤러가 소유자 객체를 삭제한 후 디펜던트 리소스를 무시하도록 `orphan` 파이널라이저를 추가한다.

## 다음
- [Kunernetes 파이널라이저](./finalizers.md)에 대해 자세히 알아본다.
- [가비지 컬렉션](https://kubernetes.io/docs/concepts/architecture/garbage-collection)에 대해 알아보세요.
- [객체 메타데이터](https://kubernetes.io/docs/reference/kubernetes-api/common-definitions/object-meta/#System)에 대한 API 참조를 읽어보세요.
