*필드 선택자(Field selectors)*를 사용하면 하나 이상의 리소스 필드 값을 기반으로 Kubernetes 객체를 선택할 수 있다. 다음은 필드 선택자 쿼리의 예들이다.

- `metadata.name=my-service`
- `metadata.namespace!=default`
- `status.phase=Pending`

이 `kubectl` 명령은 `status.phase` 필드의 값이 `Running`인 모든 Pod를 선택한다.

```bash
$ kubectl get pods --field-selector status.phase=Running
```

> **Note**: 필드 선택자는 본질적으로 리소스 *필터*이다. 기본적으로 선택자/필터는 적용되지 않으며, 이는 지정된 타입의 모든 리소스가 선택된다는 것을 의미한다. 따라서 `kubectl` 쿼리인 `kubectl get pods`와 `kubectl get pods --field-selector ""`는 동등하게 된다.

## 지원되는 필드
지원되는 필드 선택자는 Kubernetes 리소스 타입에 따라 다르다. 모든 리소스 타입은 `metadata.name`과 `metadata.namespace` 필드를 지원한다. 지원되지 않는 필드 선택자를 사용하면 오류가 발생한다. 예를 들어

```bash
$ kubectl get ingress --field-selector foo.bar=baz
Error from server (BadRequest): Unable to find "ingresses" that match label selector "", field selector "foo.bar=baz": "foo.bar" is not a known field selector: only "metadata.name", "metadata.namespace"
```

## 지원되는 연산자
필드 선택자와 함께 `=`, `==`와 `!=` 연산자를 사용할 수 있다(`=`와 `==`는 같은 의미이다). 예를 들어, 이 `kubectl` 명령은 `default` 네임스페이스에 없는 모든 Kubernetes 서비스를 선택한다.

```bash
$ kubectl get services  --all-namespaces --field-selector metadata.namespace!=default
```

## 연결된 선택자
레이블과 다른 선택자와 마찬가지로, 필드 선택자는 쉼표로 구분된 목록으로 함께 연결될 수 있다. 아래 `kubectl` 명령은 `status.phase`가 `Running`과 같지 않고 `spec.restartPolicy` 필드가 `Always`인 모든 Pod를 선택한다.

```bash
$ kubectl get pods --field-selector=status.phase!=Running,spec.restartPolicy=Always
```

## 리소스 타입
여러 리소스 타입에 걸쳐 필드 선택자를 사용할 수 있다. 아래 `kubectl` 명령은 `default` 네임스페이스에 없는 모든 StatefulSet과 서비스를 선택한다.

```bash
$ kubectl get statefulsets,services --all-namespaces --field-selector metadata.namespace!=default
```
