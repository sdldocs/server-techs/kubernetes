클러스터의 각 객체에는 해당 리소스 타입내에서 고유한 [*이름*](#이름nmaes)을 갖는다. 모든 Kubernetes 객체에는 전체 클러스터에서 고유한 [*UID*](#uids)도 있다.

예를 들어, 동일한 [네임스페이스](./namespaces.md) 내에 `myapp-1234`라는 이름의 Pod는 하나만 가질 수 있지만, 각각 이름이 `myapp-1234`인 Pod와 Deployment를 하나씩 가질 수 있다.

사용자가 제공한 고유하지 않은 어트리뷰트의 경우, Kubernetes는 [레이블](./labels.md)과 [어노테이션](./annotations.md)을 제공한다.

## 이름(Nmaes)
클라이언트가 제공한 문자열로 `/api/v1/pods/some-name`처럼 리소스 URL의 객체를 나타낸다.

특정 종류의 객체는 한 번에 하나의 이름만 가질 수 있다. 그러나 객체를 삭제하면 같은 이름으로 새 객체를 만들 수 있다.

**이름은 동일한 리소스의 모든 [API 버전](../kubernetes-api.md/#api-그룹과-버저닝)에서 고유해야 한다. API 리소스는 API 그룹, 리소스 타입, 네임스페이스(네임스페이스 리소스의 경우)와 이름으로 구별된다. 즉, 이 맥락에서 API 버전은 관련이 없다.**

> **Note**: 객체가 물리적 호스트를 나타내는 Node 같이 물리적 엔티티를 나타내는 경우, Node를 삭제하고 다시 생성하지 않고 동일한 이름으로 호스트를 다시 생성하면 Kubernetes는 새 호스트를 이전 호스트로 취급하여 불일치가 발생할 수 있다.

다음은 리소스에 일반적으로 사용되는 네 가지 타입의 이름 제약 조건이다.

### DNS 서브도메인 이름
대부분의 리소스 타입은 [RFC 1123](https://tools.ietf.org/html/rfc1123)에 정의된 대로 DNS 하위 도메인 이름으로 사용할 수 있는 이름이 필요하다. 즉, 이름은

- 253자를 넘지 않아야 한다.
- 소문자 알파벳 문자와 숫자, '-' 또는 '.'만 포함해야 한다.
- 알파벳 문자와 숫자로 시작해야 한다.
- 알파벳 문자와 숫자로 끝나야 한다.

### RFC 1123 레이블 이름
일부 리소스 타입은 [RFC 1123](https://tools.ietf.org/html/rfc1123)에 정의된 DNS 레이블 표준을 따르는 이름을 사용해야 한다. 즉, 이름은

- 최대 63자를 넘지 않아야 한다.
- 소문자 알파벳 문자와 숫자, '-' 또는 '.'만 포함해야 한다.
- 알파벳 문자와 숫자로 시작해야 한다.
- 알파벳 문자와 숫자로 끝나야 한다.

### RFC 1035 레이블 
일부 리소스 유형은 RFC [1035](https://tools.ietf.org/html/rfc1035)에 정의된 DNS 레이블 표준을 따르는 이름을 사용해야 한다. 즉, 이름은

- 최대 63자를 넘지 않아야 한다.
- 소문자 알파벳 문자와 숫자, '-' 또는 '.'만 포함해야 한다.
- 알파벳 문자와 숫자로 시작해야 한다.
- 알파벳 문자와 숫자로 끝나야 한다.

### 경로 세그먼트 이름
일부 리소스 타입은 이름이 경로 세그먼트로 안전하게 인코딩될 수 있어야 한다. 즉, 이름이 "." 또는 "."일 수 없으며 이름에 "/" 또는 "%"가 포함되지 않아야 다.

다음은 `nginx-demo`라는 이름의 Pod에 대한 매니페스트 예이다.

```yml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-demo
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
```

> **Note**: 일부 리소스 타입에는 이름에 추가 제한이 있다.

## UIDs
객체를 고유하게 식별하기 위해 Kubernetes 시스템에서 생성된 문자열.

Kubernetes 클러스터의 전체 수명 기간 동안 생성된 모든 객체에는 고유한 UID를 갖는다. 이는 과거에 생성한 유사한 엔티티와 구별하기 위한 것이다.

Kubernetes UID는 범용 고유 식별자(UUID라고도 함)이다. UUID는 ISO/IEC 9834-8와 ITU-T X.667로 표준화되어 있다.

# 다음
- Kubernetes의 [레이블](labels.md)과 [어노테이션](./annotations.md)에 대해 읽어본다.
- [Kubernetes의 식별자와 이름](https://git.k8s.io/design-proposals-archive/architecture/identifiers.md) 디자인 문서를 참조하세요.
