*레이블(label)*은 Pod와 같은 객체에 첨부되는 키/값 쌍이다. 레이블은 사용자에게 의미 있고 관련성이 있는 객체의 식별 애트리뷰트을 지정하는 데 사용되지만 코어 시스템에 대한 의미를 직접적으로 의미하지는 않는다. 레이블은 객체의 부분집합을 구성하고 선택하는 데 사용할 수 있다. 레이블은 객체를 생성할 때 객체에 첨부할 수 있으며 이후 언제든지 추가 및 수정할 수 있다. 각 객체에는 키/값 레이블 세트를 정의할 수 있습니다. 각 키는 주어진 개체에 대해 고유해야 한다.

```json
"metadata": {
  "labels": {
    "key1" : "value1",
    "key2" : "value2"
  }
}
```

레이블은 효율적인 쿼리와 감시를 가능하게 하며 UI 및 CLI에서 사용하기에 이상적이다. 비식별 정보는 [어노테이션](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/)을 사용하여 기록해야 한다.

## 동기
레이블을 사용하면 클라이언트가 이러한 매핑을 저장할 필요 없이 사용자가 자신의 조직 구조를 느슨하게 결합된 방식으로 시스템 객체에 매핑할 수 있다.

서비스 배포와 배치 처리 파이프라인은 종종 다차원 엔티티(예: 여러 파티션 또는 배포, 여러 릴리스 트랙, 여러 티어, 티어당 여러 마이크로 서비스 등)이다. 관리에는 종종 교차 절단(cross-cutting) 작업이 필요하며, 이는 엄격한 계층적 표현, 특히 사용자가 아닌 인프라에 의해 결정되는 경직된 계층의 캡슐화를 깨뜨리게 된다.

다음은 레이블 예이다.

- "`release`" : "`stable`", "`release`" : "`canary`"
- "`environment`" : "`dev`", "`environment`" : "`qa`", "`environment`" : "`production`"
- "`tier`" : "`frontend`", "`tier`" : "`backend`", "`tier`" : "`cache`"
- "`partition`" : "`customerA`", "`partition`" : `"customerB`"
- "`track`" : "`daily`", "`track`" : "`weekly`"

이는 일반적으로 사용되는 레이블의 예이며, 자신만의 규칙을 자유롭게 개발할 수 있다. 레이블 키는 주어진 객체에 대해 고유해야 한다는 점에 유의하세요.

## 구문과 문자 집합
*레이블*은 키/값 쌍이다. 유효한 레이블 키에는 슬래시(`/`)로 구분된 접두사와 이름(선택 사항)의 두 세그먼트가 있다. 이름 세그먼트는 필수이며 63자 이하여야 하며, 영어 뮨자와 숫자(`[a-z0-9A-Z]`)로 시작하여야 하고, 영어 뮨자와 숫자 사이에 대시(`-`), 밑줄(`_`), 점(`.`)가 올 수 있다. 접두사는 선택 사항이다. 접두사를 지정하는 경우 접두사는 총 253자를 넘지 않는 점(`.`)으로 구분된 일련의 DNS 레이블과 슬래시(`/`)로 구분된 DNS 서브도메인이어야 한다.

접두사를 생략하면 키 레이블은 사용자에게 비공개(private)인 것으로 간주된다. 최종 사용자 객체에 레이블을 추가하는 자동화된 시스템 컴포넌트(예: k`ube-scheduler`, `kube-controller-manager`, `kube-apiserver`, `kubectl` 또는 기타 서드파티 자동화)는 접두사를 지정해야 한다.

`kubernetes.io/`와 `k8s.io/` 접두사는 Kubernetes 코어 컴포넌트를 위해 예약되어 있다.

유효한 레이블 값은 다음과 같아야 한다.

- 63자 이하여야 한다(비워도 됨),
- 비어 있지 않은 경우 영문자와 숫자(`[a-z0-9A-Z]`)로 시작하고 끝나야 한다.
- 사이에 대시(`-`), 밑줄(`_`), 점(`.`)와 영문자와 숫자를 포함할 수 있다.

예를 들어, 다음은 `environment`: `production`과 `app`: `nginx` 두 레이블이 있는 Pod에 대한 매니페스트이다.

```yml
piVersion: v1
kind: Pod
metadata:
  name: label-demo
  labels:
    environment: production
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
```

## 레이블 선택자(Selector)
[이름 및 UID](./names.md)와 달리 레이블은 고유성을 제공하지 않는다. 일반적으로 많은 객체가 동일한 레이블을 가질 것으로 예상한다.

클라이언트 또는 사용자는 *레이블 선택자*를 통해 객체 집합을 식별할 수 있다. 레이블 선택자는 Kubernetes의 핵심 그룹화 프리미티브(primitive)이다.

API는 현재 *동등 기반(equality-based)*과 *집합 기반(set-based)* 두 타입의 선택자를 지원한다. 레이블 선택자는 쉼표로 구분된 여러 *요구사항*으로 구성될 수 있다. 여러 요구사항이 있는 경우, 모든 요구사항이 충족되어야 하므로 쉼표 구분자는 논리 `AND`(`&&`) 연산자 역할을 한다.

비어 있거나 지정되지 않은 선택자의 의미는 컨텍스트에 따라 다르며, 선택자를 사용하는 API 타입은 선택자의 유효성과 의미를 문서화해야 한다.

> **Note**: ReplicaSets와 같은 일부 API 타입의 경우 두 인스턴스의 레이블 선택자가 네임스페이스 내에서 겹치지 않아야 하며, 그렇지 않으면 컨트롤러가 이를 충돌하는 명령으로 인식하여 얼마나 많은 복제본이 있어야 하는지 결정하지 못할 수 있다.

> **Caution**: 동등 기반 조건과 집합 기반 조건은 모두 논리 *OR*(`||`) 연산자를 지원하지 않는다. 필터 문이 적절하게 구조화되어 있는지 확인하세요.

### *동등 기반(equality-based)* 요구사항
*동등(equality-) 또는 비동등(inequality-)*기반 요구 사항으로 레이블 키와 값을 기준으로 필터링할 수 있다. 일치하는 객체는 지정된 모든 레이블 제약 조건을 충족해야 하지만, 추가 레이블을 가질 수도 있다. 세 가지 종류의 연산자 `=`, `==`과 `!=`가 허용된다. 앞의 두 연산자는 같음을 나타내며(동의어), 후자의 연산자는 다름을 나타낸다. 예를 들어

```
environment = production
tier != frontend
```

전자는 키는 `environment`이고 값이 `production`인 모든 리소스를 선택한다. 후자는 키가 `tier`이고 값이 `frontend`가 아닌 모든 리소스와 `tier` 키가 없는 레이블을 갖는 모든 리소스를 선택한다. 쉼표 연산자를 사용하여 `environment=production,tier!=frontend`는 `frontend`를 제외한 `production` 리소스를 필터링할 수 있다.

동등 기반 레이블 요구사항의 한 가지 사용 시나리오는 Pod가 Node 선택 기준을 지정하는 것이다. 예를 들어, 아래의 샘플 파드는 "`accelator=nvidia-tesla-p100`" 레이블을 가진 노드를 선택한다.

```yml
apiVersion: v1
kind: Pod
metadata:
  name: cuda-test
spec:
  containers:
    - name: cuda-test
      image: "registry.k8s.io/cuda-vector-add:v0.1"
      resources:
        limits:
          nvidia.com/gpu: 1
  nodeSelector:
    accelerator: nvidia-tesla-p100
```

### *집합 기반(set-based)* 요구사항
집합 기반 레이블 요구 사항을 사용하면 값 집합에 따라 키를 필터링할 수 있다. `in`, `notin`과 `exists`(키 식별자만) 세 종류의 연산자가 지원된다. 예를 들어,

```
environment in (production, qa)
tier notin (frontend, backend)
partition
!partition
```

- 첫 번째 예는 `environment` 키가 같고 값이 `production` 또는 `qa`인 모든 리소스를 선택한다.
- 두 번째 예는 키가 `tier`이고 값이 `frontend`와 `backend` 이외의 값인 모든 리소스를 선택하며, `tier` 키가 없는 레이블을 갖는 모든 리소스를 선택한다.
- 세 번째 예는 키 `partition`이 있는 레이블을 포함한 모든 리소스를 선택하며, 값은 확인하지 않는다.
- 네 번째 예는 키 `partition`이 없는 레이블을 갖는 모든 리소스를 선택하며, 값은 확인하지 않는다(?).

마찬가지로 쉼표 구분 기호는 *AND* 연산자 역할을 한다. 따라서 `partition` 키가 있고(값에 관계없이) 환경이 `qa`와 다른 리소스를 필터링하는 것은 `partition,environment notin(qa)`를 사용하여 수행할 수 있다. *집합 기반* 레이블 선택자는 `environment=production`이 `environment in (production)`과 값이 같으므로 동등의 일반적인 형태이며, `!=`와 `notin`도 마찬가지이다.

*집합 기반* 요구 사항은 *동등 기반* 요구 사항과 혼합할 수 있다. 예를 들면 `partition in (customerA, customerB),environment!=qa`이다.

## API

### LIST와 WATCH 필터링
LIST와 WATCH 작업은 쿼리 매개변수를 사용하여 반환되는 객체 집합을 필터링하기 위해 레이블 선택자를 지정할 수 있다. 두 요구 사항에서 모두 허용된다(여기서는 URL 쿼리 문자열에 나타나는 대로 표시됨).

- *동등 기반* 요구 사항: `?labelSelector=environment%3Dproduction,tier%3Dfrontend`
- *집합 기반* 요구 사항: `?labelSelector=environment+in+%28production%2Cqa%29%2Ctier+in+%28frontend%29`
두 레이블 선택기 스타일 모두 REST 클라이언트를 통해 리소스를 list하거나 watch하는 데 사용할 수 있다. 예를 들어, `kubectl`로 `apiserver`를 타깃팅하고 *동등 기반* 레이블을 사용하면 다음과 같이 작성할 수 있으며

```bash
$ kubectl get pods -l environment=production,tier=frontend
```

또는 *집합 기반* 레이블을 사용할 수도 있다.

```bash
$ kubectl get pods -l 'environment in (production),tier in (frontend)'
```

앞서 언급했듯이 *집합 기반* 요구 사항은 더 표현력이 좋다. 예를 들어 값에 *OR* 연산자를 구현할 수 있다.

```bash
$ kubectl get pods -l 'environment in (production, qa)'
```

또는 `notin` 연산자를 이용하여 부정적 일치를 제한할 수 있다.

```bash
$ kubectl get pods -l 'environment,environment notin (frontend)'
```

### API 객체에서 집합 참조
`services`와 `replicationcontroller` 같은 일부 Kubernetes 객체는 레이블 선택자를 사용하여 Pod와 같은 다른 리소스 집합을 지정할 수도 있다.

#### `Services`와 `ReplicationController`
`service`가 타겟팅하는 Pod 집합을 레이블 선택자로 정의할 수 있다. 마찬가지로, `replicationcontroller`가 관리해야 하는 Pod의 모집단도 레이블 선택자로 정의된다.

두 객체에 대한 레이블 선택자는 맵을 사용하여 `json` 또는 `yaml` 파일에 정의되며, *동등 기반* 요구사항 선택자만 지원된다.

```jason
"selector": {
    "component" : "redis",
}
```

또는

```yml
selector:
  component: redis
```

이 선택자(각각 `json` 또는 `yaml` 형식)는 `component=redis` 또는 `component in(redis)`와 같다.

#### 집합 기반 요구사항을 지원하는 리소스
`Job`, `Deployment`, `ReplicaSet`와 `DaemonSet` 같은 최신 리소스는 *집합 기반* 요구 사항도 지원한다.

```yml
selector:
  matchLabels:
    component: redis
  matchExpressions:
    - { key: tier, operator: In, values: [cache] }
    - { key: environment, operator: NotIn, values: [dev] }
```

`matchLabels`은 `{key,value}` 쌍의 맵이다. `matchLabels` 맵의 단일 `{key,value}`는 `key` 필드가 "key", `operator`가 "In", `vlaues` 배열이 "value"만 포함하는 `matchExpressions`의 요소와 동일하다. `matchExpressions`는 Pod 선택자 요구 사항의 목록이다. 유효한 연산자로는 `In`, `NotIn`, `Exists`와 `DoesNotExist`가 있다. `In`과 `NotIn`의 경우 값 집합이 공집합이 아니어야 한다. `matchLabels`과 `matchExpressions`의 모든 요구사항은 함께 AND이며, 일치하려면 모두 충족되어야 한다.

#### node 집합의 선택
레이블을 통한 선택의 한 가지 사용 사례로 Pod가 스케줄링할 수 있는 node 집합을 제한하는 것이다. 자세한 내용은 [노드 선택](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/)에 대한 문서를 참조하세요.

## 다음
- [add a label to a node](https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes/#add-a-label-to-a-node)하는 방법 알아보기
- [Well-known labels, Annotations and Taints](https://kubernetes.io/docs/reference/labels-annotations-taints/) 찾기
- [Recommended labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/) 보기
- [Enforce Pod Security Standards with Namespace Labels](https://kubernetes.io/docs/tasks/configure-pod-container/enforce-standards-namespace-labels/)
- [Use Labels effectively](https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/#using-labels-effectively)하여 배포를 관리한다.
- [Writing a Controller for Pod Labels](https://kubernetes.io/blog/2021/06/21/writing-a-controller-for-pod-labels/)에 대한 블로그 읽기
