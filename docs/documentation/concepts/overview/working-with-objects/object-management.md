`kubectl` 명령 도구는 Kubernetes 객체를 생성하고 관리하는 여러 방법을 지원한다. 이 문서는 다양한 접근 방식에 대한 개요를 제공한다. Kubectl로 객체를 관리하는 방법에 대한 자세한 내용은 [Kubectl book](https://kubectl.docs.kubernetes.io/)을 읽어보길 바란다.

## 관리 테크닉

> **Warning**: Kubernetes 객체는 하나의 기술만 사용하여 관리해야 한다. 동일한 객체에 대해 기술을 혼합하고 일치시키면 정의되지 않은 동작이 초래될 수 있다.

| Management technique | Operates on | Recommended environment | Supported writers | Learning curve |
|----------------------|-------------|-------------------------|-------------------|----------------|
| Imperative commands | Live objects | Development projects | 1+ | Lowest |
| Imperative object configuration | Individual files | Production projects | 1 | Moderate |
| Declarative object configuration | Directories of files | Production projects | 1+ | Highest |

## Imperative(명령형) 명령
명령형 명령을 사용할 때, 사용자는 클러스터의 라이브 객체를 직접 작동시킨다. 사용자는 인자 또는 플래그를 사용하여 `kubectl` 명령으로 작동을 제공한다.

이 방법은 클러스터에서 일회성 작업을 시작하거나 실행하는 데 권장되는 방법이다. 이 방법은 라이브 객체에서 직접 동작하기 때문에 이전 구성에 대한 기록을 제공하지 않는다.

### 예
Deployment 객체를 생성하여 nginx 컨테이너 인스턴스를 실행한다.

```bash
$ kubectl create deployment nginx --image nginx
```

### 장단점
객체 구성에 비해 장점

- 명령은 단일 동작 단어로 표현된다.
- 명령은 클러스터를 변경하는 데 단 한 단계만 필요하다.

객체 구성에 비해 단점

- 명령은 변경 검토 프로세스와 통합되지 않는다.
- 명령은 변경 사항과 관련된 감사 추적을 제공하지 않는다.
- 명령은 라이브 레코드를 제외한 레코드 소스를 제공하지 않는다.
- 명령은 새 객체를 만들기 위한 템플릿을 제공하지 않는다.

## Imperative 객체 구성
명령형 객체 구성에서 `kubectl` 명령은 작업(생성, 대체 등), 선택적 플래그와 하나 이상의 파일 이름을 지정한다. 지정한 파일에는 YAML 또는 JSON 형식의 객체의 전체 정의를 포함하여야 한다.

객체 정의에 대한 자세한 내용은 [API reference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.28/)를 참조한다.

> **Warning**: 명령형 `replace` 명령은 기존 사양을 새로 제공된 사양으로 대체하여 구성 파일에서 누락된 개체에 대한 모든 변경 사항을 삭제한다. 이 접근 방식은 사양이 구성 파일과 독립적으로 업데이트되는 리소스 타입에는 사용하지 않아야 한다. 예를 들어 `LoadBalancer` 타입의 서비스는 클러스터에 의해 구성과 독립적으로 `externalIPs` 필드가 업데이트된다.

### 예
구성 파일에 정의된 객체를 생성한다.

```bash
$ kubectl create -f nginx.yaml
```

두 구성 파일에서 정의한 객체를 삭제한다.

```bash
$ kubectl delete -f nginx.yaml -f redis.yaml
```

라이브 구성을 덮어쓰는 방식으로 구성 파일에 정의된 객체를 업데이트한다.

```bash
$ kubectl replace -f nginx.yaml
```

### 장단점
명령형 명령레 비해 장점.

- 객체 구성을 Git과 같은 소스 제어 시스템에 저장할 수 있다.
- 객체 구성은 푸시 전 변경 사항 검토와 감사 추적같은 프로세스와 통합할 수 있다.
- 개체 구성은 새 객체를 만들기 위한 템플릿을 제공한다.

- 명령형 명령에 비해 단점.

- 객체 구성에는 객체 스키마에 대한 기본적인 이해가 필요한다.
- 객체 구성에는 YAML 파일을 작성하는 추가 단계가 필요하다.

선언적 개체 구성에 비해 장점.

- 명령형 객체 구성 동작은 더 간단하고 이해하기 쉽다.
- Kubernetes 버전 1.5부터 명령형 오브젝트 구성이 더 성숙해졌다.

선언적 객체 구성에 비해 단점

- 명령형 객체 구성은 디렉터리가 아닌 파일에서 가장 잘 작동한다.
- 라이브 객체에 대한 업데이트는 구성 파일에 반영되어야 하며, 그렇지 않으면 다음 교체 시 손실된다.

## Declarative 객체 구성
선언적 객체 구성을 사용하는 경우, 사용자는 로컬에 저장된 객체 구성 파일에서 작업을 수행하지만 파일에서 수행할 작업을 정의하지는 않는다. 객체 생성, 업데이트와 삭제 작업은 `kubectl`에 의해 객체별로 자동으로 감지된다. 따라서 객체마다 다른 작업이 필요할 수 있는 디렉터리에서 작업할 수 있다.

> **Note**: 선언적 객체 구성은 변경 사항이 객체 구성 파일에 다시 병합되지 않더라도 다른 작성자가 변경한 내용을 유지한다. 이는 전체 객체 구성을 교체하는 `replace API` 작업 대신 `patch API` 작업을 사용하여 관찰된 차이점만 작성하면 가능하다.

### 예
configs 디렉터리에 있는 모든 객체 구성 파일을 처리하고 라이브 객체를 생성하거나 패치한다. 먼저 차이점을 확인하여 어떤 변경 사항이 적용될지 확인한 다음 적용할 수 있다.

```bash
$ kubectl diff -f configs/
$ kubectl apply -f configs/
```

디렉터리를 재귀적으로 처리한다.

```bash
$ kubectl diff -R -f configs/
$ kubectl apply -R -f configs/
```

### 장단점
명령형 객체 구성에 비해 장점.

- 라이브 객체에 직접 적용한 변경 사항은 구성 파일에 다시 병합하지 않더라도 유지된다.
- 선언적 객체 구성은 디렉터리에서 작동하고 객체별 작업 타입(생성, 패치, 삭제)을 자동으로 감지하는 기능이 더 잘 지원된다.

명령형 객체 구성에 비해 단점.

- 선언적 객체 구성은 예기치 않은 결과가 발생했을 때 디버깅하고 이해하기 어렵다.
- diff를 사용한 부분 업데이트는 복잡한 병합과 패치 작업을 생성한다.

## 다음
- [Managing Kubernetes Objects Using Imperative Commands](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/imperative-command/)
- [Imperative Management of Kubernetes Objects Using Configuration Files](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/imperative-config/)
- [Declarative Management of Kubernetes Objects Using Configuration Files](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/)
- [Declarative Management of Kubernetes Objects Using Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/)
- [Kubectl Command Reference](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands/)
- [Kubectl Book](https://kubectl.docs.kubernetes.io/)
- [Kubernetes API Reference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.28/)
