Kubernetes에서 네임스페이스는 단일 클러스터 내에서 리소스 그룹을 격리하기 위한 메커니즘을 제공한다. 리소스 이름은 네임스페이스 내에서 고유해야 하지만 네임스페이스 간에는 중복될 수 있다. 네임스페이스 기반 스코핑은 네임스페이스 객체(예: Deployment, 서비스 등)에만 적용되며 클러스터 전체 객체(예: StorageClass, Nodes, PersistentVolumes 등)에는 적용되지 않는다.

## 여러 네임스페이스를 사용해야 하는 경우
네임스페이스는 여러 팀 또는 프로젝트에 분산되어 있는 많은 사용자가 있는 환경에서 사용하기 위한 것dl다. 사용자가 몇 명에서 수십 명인 클러스터의 경우 네임스페이스를 만들거나 네임스페이스에 대해 전혀 생각할 필요가 없다. 네임스페이스가 제공하는 기능이 필요할 때 네임스페이스를 사용하기 시작하세요.

네임스페이스는 이름에 대한 범위를 제공합니다. 리소스 이름은 네임스페이스 내에서 고유해야 하지만 네임스페이스 간에는 중복될 수 있다. 네임스페이스는 서로 중첩될 수 없으며, 각 Kubernetes 리소스는 하나의 네임스페이스에만 존재할 수 있다.

네임스페이스는[리소스 할당량(resource quota)](https://kubernetes.io/docs/concepts/policy/resource-quotas/)을 통해 클러스터 리소스를 여러 사용자 간에 분할하는 방법이다.

동일한 소프트웨어의 다른 버전과 같이 약간 다른 리소스를 분리하기 위해 여러 네임스페이스를 사용할 필요는 없다. 레이블을 사용하여 동일한 네임스페이스 내에서 리소스를 구분하면 된다.

> **Note**: 프로덕션 클러스터의 경우 `default` 네임스페이스를 사용하지 않는 것이 좋다. 대신 다른 네임스페이스를 만들어서 사용하세요.

## 초기 네임스페이스
Kubernetes는 네 개의 초기 네임스페이스로 시작한다.

**`default`**

Kubernetes에는 이 네임스페이스가 포함되어 있으므로 먼저 네임스페이스를 생성하지 않고도 새 클러스터를 사용할 수 있다.

**`kube-node-lease`**

이 네임스페이스는 각 노드와 연결된 [Lease](https://kubernetes.io/docs/concepts/architecture/leases/) 객체를 보유한다. Node 리스(lease)를 사용하면 kubelet이 [heartbeats](https://kubernetes.io/docs/concepts/architecture/nodes/#heartbeats)를 전송하여 컨트롤 플레인이 노드 장애를 감지할 수 있다.

**`kube-public`**

이 네임스페이스는 *모든* 클라이언트(인증되지 않은 클라이언트 포함)가 읽을 수 있다. 이 네임스페이스는 대부분 전체 클러스터에서 일부 리소스를 공개적으로 보고 읽을 수 있어야 하는 경우에 대비하여 클러스터 사용을 위해 예약되어 있다. 이 네임스페이스의 공개 측면은 관례일 뿐 필수 사항은 아니다.

**`kube-system`**

Kubernetes 시스템에서 생성된 객체의 네임스페이스이다.

## 네임스페이스 작업
네임스페이스의 생성과 삭제는 [네임스페이스에 대한 관리 가이드 문서](https://kubernetes.io/docs/tasks/administer-cluster/namespaces)에 설명되어 있다.

> **Note**: 접두사 `kube-`는 Kubernetes 시스템 네임스페이스용으로 예약되어 있으므로, 접두사가 붙은 네임스페이스는 생성하지 않도록 한다.

### 네임스페이스 보기
다음과 같이 클러스터의 현재 네임스페이스를 나열할 수 있다.

```bash
$ kubectl get namespace
NAME              STATUS   AGE
default           Active   1d
kube-node-lease   Active   1d
kube-public       Active   1d
kube-system       Active   1d
```

### 요청에 대한 네임스페이스 설정
현재 요청의 네임스페이스를 설정하려면 `--namespace` 플래그를 사용합니다.

예를 들어

```bash
$ kubectl run nginx --image=nginx --namespace=<insert-namespace-name-here>
kubectl get pods --namespace=<insert-namespace-name-here>
```

### 선호 네임스페이스 설정
해당 컨텍스트에서 모든 후속 `kubectl` 명령의 네임스페이스를 영구적으로 저장할 수 있다.

```bash
$ kubectl config set-context --current --namespace=<insert-namespace-name-here>
# Validate it
$ kubectl config view --minify | grep namespace:
```

## 네임스페이스와 DNS
[서비스](https://kubernetes.io/docs/concepts/services-networking/service/)를 생성하면 해당 [DNS 엔트리](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/)가 생성된다. 이 항목은 `<service-name>.<namespace-name>.svc.cluster.local` 형식이며, 컨테이너가 `<service-name>`만 사용하는 경우 네임스페이스에 로컬인 서비스로 리졸브된다는 의미이다. 이는 개발, 스테이징과 프로덕션같은 여러 네임스페이스에서 동일한 구성을 사용할 때 유용하다. 네임스페이스들을 연결하려면 FQDN(정규화된 도메인 이름)을 사용해야 한다.

따라서 모든 네임스페이스 이름은 유효한 [RFC 1123 DNS 레이블](./names.md/#rfc-1123-레이블-이름)이어야 한다.

> **Warning**
>
> [공개 최상위 도메인](https://data.iana.org/TLD/tlds-alpha-by-domain.txt)과 동일한 이름의 네임스페이스를 만들면 이러한 네임스페이스의 서비스는 공개 DNS 레코드와 겹치는 짧은 DNS 이름을 가질 수 있다. [후행 점](https://datatracker.ietf.org/doc/html/rfc1034#page-8)없이 DNS 조회를 수행하는 네임스페이스의 워크로드는 공용 DNS보다 우선하여 해당 서비스로 리디렉션된다.

이를 완화하려면 네임스페이스 생성 권한을 신뢰할 수 있는 사용자로 제한하세요. 필요한 경우 추가적으로 [admission webhook](https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/) 같은 타사 보안 제어를 구성하여 [공개 TLD](https://data.iana.org/TLD/tlds-alpha-by-domain.txt) 이름으로 네임스페이스를 만드는 것을 차단할 수 있다.

## 모든 개체가 네임스페이스에 있는 것은 아니다
대부분의 Kubernetes 리소스(예: pod, 서비스, replication 컨트롤러 등)는 일부 네임스페이스에 있다. 그러나 네임스페이스 리소스는 그 자체로 네임스페이스에 있지 않다. 그리고 [node](https://kubernetes.io/docs/concepts/architecture/nodes/)와 [persistentVolumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)같은 로우레벨 리소스는 네임스페이스에 있지 않다.

네임스페이스에 있는 Kubernetes 리소스와 없는 리소스를 확인하려면,

```bash
# In a namespace
$ kubectl api-resources --namespaced=true

# Not in a namespace
$ kubectl api-resources --namespaced=false
```

## 자동 레이블링
**FEATURE STATE**: Kubernetes 1.22 [stable]

Kubernetes 컨트롤 플레인은 모든 네임스페이스에 불변 레이블인 `kubernetes.io/metadata.name`을 설정한다. 레이블의 값은 네임스페이스 이름이다.

## 다음
- [새 네임스페이스 만들기](https://kubernetes.io/docs/tasks/administer-cluster/namespaces/#creating-a-new-namespace)에 대해 자세히 알아보세요.
- [네임스페이스 삭제에](https://kubernetes.io/docs/tasks/administer-cluster/namespaces/#deleting-a-namespace) 대해 자세히 알아보세요.
