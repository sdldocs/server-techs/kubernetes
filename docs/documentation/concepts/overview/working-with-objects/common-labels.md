kubectl과 대시보드보다 더 많은 도구로 Kubernetes 객체를 시각화하고 관리할 수 있다. 공통 레이블 세트를 사용하면 모든 도구가 이해할 수 있는 공통된 방식으로 객체를 설명하여 도구가 상호 운용적으로 작동할 수 있다.

권장 레이블은 툴을 지원하는 것 외에도 쿼리할 수 있는 방식으로 어플리케이션을 설명한다.

메타데이터는 *어플리케이션*의 개념을 중심으로 구성된다. Kubernetes는 서비스형 플랫폼(PaaS)이 아니며 어플리케이션에 대한 공식적인 개념이 없거나 강제하지 않는다. 대신 비공식적이며 메타데이터로 어플리케이션을 설명한다. 어플리케이션에 포함된 항목에 대한 정의는 느슨하다.

> **Note**: 이는 권장하는 레이블이다. 어플리케이션을 더 쉽게 관리할 수 있도록 도와주지만 핵심 도구에 반드시 필요한 것은 아니다.

공유 레이블과 어노테이션은 공통 접두사 `app.kubernetes.io`를 공유한다. 접두사가 없는 레이블은 사용자에게 비공개된다. 공유 접두사는 공유 레이블이 커스텀 사용자 레이블을 방해하지 않도록 보장한다.

## 레이블
이러한 레이블을 최대한 활용하려면 모든 리소스 객체에 레이블을 적용해야 한다.

| **키** | **설명** | **예** | **타입** |
|--------|--------|--------|---------|
| app.kubernetes.io/name | 어플리케이션 이름 | mysql | string |
| app.kubernetes.io/instance | 어플리케이션 인스턴스를 식별하는 고유한 이름 | mysql-abcxzy | string |
| app.kubernetes.io/version | 어플리케이션의 현재 버전 (e.g., a [SemVer 1.0](https://semver.org/spec/v1.0.0.html), revision hash, etc.) | 5.7.21 | string |
| app.kubernetes.io/component | 아키텍처내의 구성요소 | database | string |
| app.kubernetes.io/part-of | 이 어플리케이션이 속해 있는 상위 어플리케이션의 이름 | wordpress | string |
| app.kubernetes.io/managed-by | 애플리케이션의 운영을 관리하는 데 사용되는 도구 | helm | string |

이러한 레이블이 실제로 작동하는 것을 설명하기 위해 다음 StatefulSet 객체를 고려해 보겠다.

```yml
# This is an excerpt
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app.kubernetes.io/name: mysql
    app.kubernetes.io/instance: mysql-abcxzy
    app.kubernetes.io/version: "5.7.21"
    app.kubernetes.io/component: database
    app.kubernetes.io/part-of: wordpress
    app.kubernetes.io/managed-by: helm
```

## 어플리케이션과 어플리케이션 인스턴스
어플리케이션은 Kubernetes 클러스터에 한 번 이상 설치될 수 있으며, 경우에 따라 동일한 네임스페이스에 설치될 수도 있다. 예를 들어, 서로 다른 웹사이트가 서로 다른 WordPress를 설치하고자 하는 경우 WordPress를 두 번 이상 설치할 수 있다.

어플리케이션 이름과 인스턴스 이름은 별도로 기록된다. 예를 들어, WordPress는 `wordpress`의 `app.kubernetes.io/name`를 가지며, 인스턴스 이름은 `app.kubernetes.io/instance`의 값 `wordpress-abcxzy`으로  표시된다. 이렇게 하면 어플리케이션과 애플리케이션의 인스턴스를 식별할 수 있다. 어플리케이션의 모든 인스턴스는 고유한 이름을 가져야 한다.

## 예
이러한 레이블을 사용하는 다양한 방법을 설명하기 위해 다음 예는 다양하고 복잡하다.

### 간단한 상태를 저장하지 않는 서비스
`Deployment`와 `Service` 객체를 사용하여 배포된 간단한 상태를 저장하지 않는 서비스의 경우를 생각해 보자. 다음 두 코드는 레이블을 가장 간단한 형태로 사용하는 방법을 보이고 있다.

`Deployment`는 어플리케이션 자체를 실행하는 pod를 감독하는 데 사용된다.

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/name: myservice
    app.kubernetes.io/instance: myservice-abcxzy
...
```

`Service`는 어플리케이션을 노출하는 데 사용된다.

```yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: myservice
    app.kubernetes.io/instance: myservice-abcxzy
...
```

### 데이터베이스를 운영하는 웹 어플리케이션
조금 더 복잡한 어플리케이션, 즉 헬름을 사용하여 설치한 데이터베이스(MySQL)를 사용하는 웹 애플리케이션(WordPress)을 예로 들어보자. 다음 코드는 이 어플리케이션을 배포하는 데 사용되는 객체의 시작을 보여준다.

WordPress의 `Deployment`로 시작한다.

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/name: wordpress
    app.kubernetes.io/instance: wordpress-abcxzy
    app.kubernetes.io/version: "4.9.4"
    app.kubernetes.io/managed-by: helm
    app.kubernetes.io/component: server
    app.kubernetes.io/part-of: wordpress
...
```

WordPress를 노출하는 데 `Service`를 사용한다.

```yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: wordpress
    app.kubernetes.io/instance: wordpress-abcxzy
    app.kubernetes.io/version: "4.9.4"
    app.kubernetes.io/managed-by: helm
    app.kubernetes.io/component: server
    app.kubernetes.io/part-of: wordpress
...
```

해당 어플리케이션과 해당 어플리케이션이 속한 더 큰 어플리케이션 모두에 대한 메타데이터가 포함된 `StatefulSet`으로 MySQL을 노출한다.

```yml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    app.kubernetes.io/name: mysql
    app.kubernetes.io/instance: mysql-abcxzy
    app.kubernetes.io/version: "5.7.21"
    app.kubernetes.io/managed-by: helm
    app.kubernetes.io/component: database
    app.kubernetes.io/part-of: wordpress
...
```

WordPress의 구성요소로 MySQL을 노출하기 위하여 `Service`를 사용한다.

```yml
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/name: mysql
    app.kubernetes.io/instance: mysql-abcxzy
    app.kubernetes.io/version: "5.7.21"
    app.kubernetes.io/managed-by: helm
    app.kubernetes.io/component: database
    app.kubernetes.io/part-of: wordpress
...
```

MySQL `StatefulSet`과 `Service`를 사용하면 MySQL과 더 광범위한 어플리케이션인 WordPress에 대한 정보가 모두 포함되어 있음을 알 수 있다.
