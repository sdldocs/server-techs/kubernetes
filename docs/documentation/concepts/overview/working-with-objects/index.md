# Kubernetes의 객체
이 페이지는 Kubernetes 객체가 Kubernetes API에서 어떻게 표현되는지, 그리고 `.yaml` 형식으로 어떻게 표현할 수 있는지 설명한다.

## Kubernetes 객체의 이해
Kubernetes 객체는 Kubernetes 시스템의 영구적인 엔티티이다. Kubernetes는 이러한 엔티티를 사용하여 클러스터의 상태를 나타낸다. 구체적으로는 다음과 같다.

- 실행 중인 컨테이너화된 어플리케이션(그리고 어떤 node상에서)
- 해당 어플리케이션에 사용할 수 있는 리소스
- 재시작 정책, 업그레이드, 내결함성 등 해당 어플리케이션의 동작 방식에 관한 정책

Kubernetes 객체는 "의도에 대한 기록"으로, 일단 객체를 생성하면 Kubernetes 시스템은 해당 객체가 존재하는지 확인하기 위해 지속적으로 작업한다. 객체를 생성하면 클러스터의 워크로드가 어떤 모습이어야 하는지, 즉 클러스터의 원하는 상태를 Kubernetes 시스템에 효과적으로 전달할 수 있다.

객체를 생성, 수정 또는 삭제하는 등 Kubernetes 객체에 대하여 작업하려면 [Kubernetes API](../kubernetes-api.md)를 사용해야 한다. 예를 들어, `kubectl` 커맨드-라인 인터페이스를 사용하면 CLI가 필요한 Kubernetes API 호출을 대신 수행한다. [클라이언트 라이브러리](https://kubernetes.io/docs/reference/using-api/client-libraries/) 중 하나를 사용하여 자체 프로그램에서 직접 Kubernetes API를 사용할 수도 있다.

### 객체 사양과 상태
거의 모든 Kubernetes 객체에는 객체의 구성을 관리하는 두 개의 중첩된 객체 필드, 즉 객체 `spec`과 객체 `status`가 포함되어 있다. `spec`이 있는 객체의 경우, 객체를 생성할 때 이를 설정하여 리소스에 원하는 특성, 즉 *원하는 상태*에 대한 기술을 제공해야 한다.

`status`는 Kubernetes 시스템과 그 구성 요소에 의해 제공되고 업데이트되는 객체의 *현재 상태*를 기술한다. 쿠버네티스 컨트롤 플레인은 모든 오브젝트의 실제 상태가 사용자가 제공한 원하는 상태와 일치하도록 지속적이고 능동적으로 관리합니다.

예를 들어, Kubernetes에서 Deployment는 클러스터에서 실행 중인 어플리케이션을 나타낼 수 있는 객체이다. Deployment를 생성할 때, Deployment `spec`을 설정하여 어플리케이션의 복제본 세 개를 실행하도록 지정할 수 있다. Kubernetes 시스템은 Deployment spec을 읽고 원하는 어플리케이션의 인스턴스 3개를 시작하여 spec과 일치하도록 status를 업데이트한다. 이러한 인스턴스 중 하나라도 실패(status 변경)하면, Kubernetes 시스템은 spec과 status의 차이에 대응하여 수정(이 경우 대체 인스턴스 시작)을 수행한다.

객체 spec, status와 metadata에 대한 자세한 내용은 [Kubernetes API Conventions](https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md)을 참조하세요.

### Kubenetes 객체의 기술
Kubenetes에서 객체를 생성할 때, 원하는 state를 기술하는 객체 `spec`과 오브젝트에 대한 몇 가지 기본 정보(예: 이름)를 제공해야 한다. Kubernetes API를 사용하여 객체를 생성하는 경우(직접 또는 `kubectl`을 통해), 해당 API 요청은 요청 본문에 JSON 형식의 정보를 포함해야 한다. **대부분의 경우, 사용자는 .yaml 파일로 정보를 `kubectl`에 제공하며**, `kubectl`은 API 요청을 할 때 정보를 JSON으로 변환한다.

다음은 Kubenetes Deployment에 필수 필드와 객체 spec을 보여주는 `.yaml` 파일이다.

```yaml
# application/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

위와 같은 `.yaml` 파일을 사용하여 Deployment를 생성하는 한 가지 방법은 `kubectl` 커맨드-라인 인터페이스에서 `.yaml` 파일을 인수로 전달하여 `kubectl apply` 명령을 사용하는 것이다. 다음이 그 예이다.

```bash
$ kubectl apply -f https://k8s.io/examples/application/deployment.yaml
```

출력은 아래와 유사할 것이다.

```
deployment.apps/nginx-deployment created
```

### 필수 필드
생성하려는 Kubernetes 객체에 대한 `.yaml` 파일에서 다음 필드에 대한 값을 설정해야 한다.

- `apiVersion` - 이 객체를 생성하는 데 사용하는 Kubernetes API 버전
- `kind` - 생성하려는 객체의 종류
- `metadata` - `name` 문자열, `UID`과 선택적인 `namespace`를 포함하여 객체를 고유하게 식별하는 데 도움이 되는 데이터
- `spec` - 객체에 대해 원하는 상태

객체 `spec`의 정확한 형식은 모든 Kubernetes 객체마다 다르며, 해당 객체와 관련된 중첩된 필드를 포함한다. [Kubernetes API Reference](https://kubernetes.io/docs/reference/kubernetes-api/)는 Kubernetes를 사용하여 생성할 수 있는 모든 객체에 대한 spec 형식을 찾는 데 도움을 줄 수 있다.

예를 들어, Pod API Reference는 [`spec` field](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#PodSpec)를 참조한다. 각 Pod에 대해, `.spec` 필드는 pod와 원하는 상태(예: 해당 pod 내의 각 컨테이너에 대한 컨테이너 이미지 이름)를 지정한다. 객체 사양의 또 다른 예는 StatefulSet API에 대한 [`spec` field](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#PodSpec)이다. StatefulSet의 경우, `.spec` 필드는 StatefulSet과 원하는 그 상태를 지정한다. StatefulSet의 `.spec` 안에는 Pod 객체를 위한 [템플릿](https://kubernetes.io/docs/concepts/workloads/pods/#pod-templates)이 있다. 이 템플릿은 StatefulSet 컨트롤러가 StatefulSet 사양을 충족하기 위해 생성할 Pod를 기술한다. 다른 종류의 오브젝트도 다른 `.status`를 가질 수 있으며, API reference 페이지에서는 해당 `.status` 필드의 구조와 각 다른 타입의 객체에 대한 내용을 자세히 설명한다.

> **Note**: YAML 구성 파일 작성에 대한 자세한 내용은 [Configuration Best Practices](https://kubernetes.io/docs/concepts/configuration/overview/)를 참조하세요.

## 서버 측 필드 유효성 검사
Kubernetes v1.25부터 API 서버는 객체에서 인식할 수 없거나 중복된 필드를 감지하는 서버 측 [필드 유효성 검사](https://kubernetes.io/docs/reference/using-api/api-concepts/#field-validation)을 제공한다. 이는 서버 측에서 `kubectl --validate`의 모든 기능인 것이다.

`kubectl` 도구는 `--validate` 플래그를 사용하여 필드 유효성 검사 수준을 설정한다. 이 플래그는 `ignore`, `warn`과 `strict` 값을 허용하는 동시에 `true`(`strict`와 동일)와 `false`(`ignore`와 동일) 값도 허용한다. `kubectl`의 기본 유효성 검사 설정은 `--validate=true`이다.

**Strict**
엄격한 필드 유효성 검사, 유효성 검사 실패 시 오류 발생

**Warn**
필드 유효성 검사가 수행되지만 오류는 요청 실패가 아닌 경고로 노출된다.

**Ignore**
서버 측 필드 유효성 검사를 수행하지 않는다.

 `kubectl`이 필드 유효성 검사를 지원하는 API 서버에 연결할 수 없는 경우, 클라이언트 측 유효성 검사를 사용하게 된다. Kubernetes 1.27과 이상 버전은 항상 필드 유효성 검사를 제공하지만, 이전 Kubernetes 릴리스에서는 그렇지 않을 수 있다. 클러스터가 v1.27보다 이전 버전인 경우, 사용 중인 Kubernetes 버전에 대한 설명서를 확인하세요.

## 다음
Kubernetes를 처음 사용하는 경우 다음에 대해 자세히 읽어보자.

- 가장 중요한 기본 Kubernetes 객체인 [Pods](../../workloads/pods/index.md).
- [Deployement]() 객체.
- Kubernetes의 [컨트롤러]().
- [kubectl]()과 [kubectl 명령어]().

Kubernetes API에 대해 전반적으로 알아보려면 다음을 방문하세요.

- [Kubernetes API Overview](https://kubernetes.io/docs/reference/using-api/)

Kubernetes의 객체에 대해 더 자세히 알아보려면 [개요](../../overview/) 섹션의 다른 페이지를 읽어 본다.

- [Kubernetes Object 관리](../../overview/working-with-objects/object-management.md)
- [객체 이름과 ID]()
- [레이블과 셀렉터]()
- [네임스페이스]()
- [어노테이션]()
- [필드 셀렉터]()
- [파이널라이저]()
- [소유자와 종속 요소]()
- [추천 레이블]()
