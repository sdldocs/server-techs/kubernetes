# Kubernetes
Kubernetes에 대한 기본적인 지식을 위한 블로그이다. 

첫 번째는 https://medium.com/ 에 실린 Kubernetes for Developers 시리즈를 저작권에 대한 허가 없이 개인적인 용도로 편역한 것이다.

- [Part I: Why learn Kubernetes at all?](https://medium.com/@yitaek/why-learn-kubernetes-at-all-1508e404c130)
- [Part II: Docker basics](https://yitaek.medium.com/docker-basics-29e306ec6ade)
- [Part III: Kubernetes 101 — Pods & Controllers](https://yitaek.medium.com/kubernetes-101-pods-controllers-dc574aa5b359)
- [Part IV: Kubernetes 101 — Architecture & Networking](https://yitaek.medium.com/kubernetes-101-architecture-networking-a5940743671e)
- [Part V: Kubernetes 101 — Resource Management & Scheduling](https://medium.com/@yitaek/kubernetes-101-resource-management-scheduling-b390da71c2b5)
- [Part VI: Popular Kubernetes Tools](https://yitaek.medium.com/popular-kubernetes-tools-62165020e839)
- [Part VII: State of Kubernetes 2023](https://medium.com/@yitaek/state-of-kubernetes-2023-2732a4cba77d)

두번째는 Kubernetes의 공식 사이트의 문서 페이지(https://kubernetes.io/docs/)를 편역한 것이다.

- [Home](https://kubernetes.io/docs/home/)
- [Getting Started](https://kubernetes.io/docs/setup/)
- [Concepts](https://kubernetes.io/docs/concepts/)
- [Tasks](https://kubernetes.io/docs/tasks/)
- [Tutorials](https://kubernetes.io/docs/tutorials/)
- [Reference](https://kubernetes.io/docs/reference/)
